package ru.nsu.fit.g16202.korshunov.game.presenter;

import ru.nsu.fit.g16202.korshunov.data.OptionsStorage;
import ru.nsu.fit.g16202.korshunov.game.model.GameModel;
import ru.nsu.fit.g16202.korshunov.game.model.Point;
import ru.nsu.fit.g16202.korshunov.game.view.FieldDrawer;
import ru.nsu.fit.g16202.korshunov.game.view.GamePanel;

import java.awt.*;
import java.util.Timer;
import java.util.TimerTask;

public class LifeGamePresenter {

    private Point lastClicked = new Point(-1, -1);
    private boolean replaceMode = false;
    private boolean isRunning = false;
    private Timer timer;
    private ModelFileHandler fileHandler = new ModelFileHandler();

    public Component getViewForDrawing() {
        return view.getViewForDrawing();
    }

    private GamePanel view;
    private GameModel model;

    public LifeGamePresenter(GamePanel view, GameModel model) {
        this.view = view;
        this.view.setPresenter(this);
        this.model = model;
    }

    public void hexagonHovered(int x, int y) {
//        if (!isClicked) return;
        if (view.isBorder(x, y)) {
            return;
        }
        Point hex = findHexagon(x, y);
        //System.out.println(hex.getX() + " " + lastClicked.getX() + " | " + hex.getY() + " " + lastClicked.getY());
        if (hex.getX() == lastClicked.getX() && hex.getY() == lastClicked.getY()) {
            return;
        }
        lastClicked.setX(hex.getX());
        lastClicked.setY(hex.getY());
        int maxWidthInThisRow = hex.getY() % 2 == 1 ? model.getWidth() - 1 : model.getWidth();
        if (hex.getX() > -1 && hex.getX() < maxWidthInThisRow && hex.getY() > -1 && hex.getY() < model.getHeight()) {
            if (!replaceMode) {
                model.setAlive(hex.getX(), hex.getY(), true);
            } else {
                model.flipAlive(hex.getX(), hex.getY());
            }
            OptionsStorage.getInstance().setSaved(false);
            view.fillAreaAt(x, y, model.isAlive(hex.getX(), hex.getY()) ? GamePanel.COLOR_ALIVE : GamePanel.COLOR_DEAD);
            view.updateImpactImage(model.getImpactMatrix());
        }
    }

    public void mouseReleased() {
        lastClicked.setX(-1);
        lastClicked.setY(-1);
//        isClicked = false;
    }

    public void forceRedrawView() {
        view.redraw(model.getStateMatrix());
        view.updateImpactImage(model.getImpactMatrix());
    }

    public void nextStep() {
        model.nextStep();
        view.updateImpactImage(model.getImpactMatrix());
        view.redraw(model.getStateMatrix());
        OptionsStorage.getInstance().setSaved(false);
    }

    private Point findHexagon(int x, int y) {
        x -= FieldDrawer.FIELD_MARGIN;
        y -= FieldDrawer.FIELD_MARGIN;
        int length = OptionsStorage.getInstance().getCellSize();
        int width = (int) (length * Math.sqrt(3));
        if (width % 2 == 1) {
            width++;
        }
        int row = (int) (y / (length * 1.5));
        int column;

        boolean rowIsOdd = row % 2 == 1;

        if (rowIsOdd) {// Yes: Offset x to match the indent of the row
            column = ((x - (width / 2)) / width);
            if ((x - (width / 2) < 0)) {
                column = -1;
            }
        } else {// No: Calculate normally
            column = (x / width);
        }
        int relY = (int) (y - (row * length * 1.5));
        int relX;

        if (rowIsOdd)
            relX = (x - (column * width)) - (width / 2);
        else
            relX = x - (column * width);
        float c = (float) length / 2;
        double m = c / (width / 2); // y = mx + c
        if (relY < (-m * relX) + c) // LEFT edge
        {
            row--;
            if (!rowIsOdd)
                column--;
        } else if (relY < (m * relX) - c) // RIGHT edge
        {
            row--;
            if (rowIsOdd)
                column++;
        }
        return new Point(column, row);
    }

    public void startNewGame(int width, int height) {
        model.startNewGame(width, height);
        view.setFieldSize(width, height);
        view.updateImpactImage(model.getImpactMatrix());
        OptionsStorage.getInstance().setSaved(true);
    }


    public boolean isReplaceMode() {
        return replaceMode;
    }

    public void setReplaceMode(boolean replaceMode) {
        this.replaceMode = replaceMode;
    }

    public void clearField() {
        model.clearField();
        view.updateImpactImage(model.getImpactMatrix());
        view.redraw(model.getStateMatrix());
        OptionsStorage.getInstance().setSaved(true);
    }

    public void flipTimer() {
        if (isRunning) {
            stopTimer();
            isRunning = false;
        } else {
            isRunning = true;
            startTimer();
        }
    }

    private void startTimer() {
        timer = new Timer();
        timer.schedule(new AutoStepTask(), 100, 500);
    }

    private void stopTimer() {
        timer.cancel();
    }

    public void openModelFromFile() {
        GameOptionsSet optionsSet = fileHandler.readDataFromFile(view);
        if (optionsSet == null) {
            return;
        }
        OptionsStorage.getInstance().setSaved(true);
        OptionsStorage.getInstance().setCellSize(optionsSet.getCellSize());
        OptionsStorage.getInstance().setLineThickness(optionsSet.getLineThickness());
        model.startNewGame(optionsSet.getM(), optionsSet.getN());
        view.setFieldSize(optionsSet.getM(), optionsSet.getN());
        model.setCellState(optionsSet.getAlivePoints());
        view.updateImpactImage(model.getImpactMatrix());
        view.redraw(model.getStateMatrix());
    }

    public void saveModelToFile() {
        fileHandler.saveModelToFile(view, model.getWidth(), model.getHeight(), model.getStateMatrix());
        OptionsStorage.getInstance().setSaved(true);
    }

    public void drawImpacts() {
        view.makeImpactsImage(model.getImpactMatrix());
    }

    public void clearImpacts() {
        view.clearImpactImage();
    }

    public void changeSize(int x, int y) {
        if (x == model.getWidth() && y == model.getHeight()) {
            forceRedrawView();
            return;
        }
        model.changeSize(x, y);
        view.setFieldSize(x, y);
        forceRedrawView();
        OptionsStorage.getInstance().setSaved(false);
    }

    public GameModel getModel() {
        return model;
    }

    private class AutoStepTask extends TimerTask {

        @Override
        public void run() {
            nextStep();
        }
    }
}