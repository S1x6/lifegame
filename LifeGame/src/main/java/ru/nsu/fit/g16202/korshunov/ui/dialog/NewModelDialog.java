package ru.nsu.fit.g16202.korshunov.ui.dialog;

import ru.nsu.fit.g16202.korshunov.data.OptionsStorage;

import javax.swing.*;
import java.awt.event.*;

public class NewModelDialog extends JDialog {
    private JPanel contentPane;
    private JButton buttonOK;
    private JButton buttonCancel;
    private JTextField etWidth;
    private JSlider slWidth;
    private JTextField etHeight;
    private JSlider slHeight;
    private JRadioButton replaceRadioButton;
    private JRadioButton XORRadioButton;
    private OnNewModelCreateListener listener;

    public NewModelDialog(OnNewModelCreateListener listener) {
        setContentPane(contentPane);
        this.listener = listener;
        setModal(true);
        getRootPane().setDefaultButton(buttonOK);
        setTitle("Создать новую модель");
        buttonOK.addActionListener(e -> onOK());

        buttonCancel.addActionListener(e -> onCancel());

        ButtonGroup btnGroup = new ButtonGroup();
        btnGroup.add(replaceRadioButton);
        btnGroup.add(XORRadioButton);
        replaceRadioButton.setSelected(true);

        etWidth.setText("10");
        slWidth.setValue(10);
        slWidth.addChangeListener(e -> etWidth.setText(String.valueOf(slWidth.getValue())));
        etWidth.addKeyListener(new KeyListener() {
            @Override
            public void keyTyped(KeyEvent e) {
            }

            @Override
            public void keyPressed(KeyEvent e) {

            }

            @Override
            public void keyReleased(KeyEvent e) {
                try {
                    int a = Integer.valueOf(etWidth.getText());
                    if (a > 100) {
                        etWidth.setText("100");
                        a = 100;
                    } else if (a < 1) {
                        etWidth.setText("1");
                        a = 1;
                    }
                    slWidth.setValue(a);

                } catch (NumberFormatException ex) {
                }
            }
        });

        etHeight.setText("10");
        slHeight.setValue(10);
        slHeight.addChangeListener(e -> etHeight.setText(String.valueOf(slHeight.getValue())));
        etHeight.addKeyListener(new KeyListener() {
            @Override
            public void keyTyped(KeyEvent e) {
            }

            @Override
            public void keyPressed(KeyEvent e) {

            }

            @Override
            public void keyReleased(KeyEvent e) {
                try {
                    int a = Integer.valueOf(etHeight.getText());
                    if (a > 100) {
                        etHeight.setText("100");
                        a = 100;
                    } else if (a < 1) {
                        etHeight.setText("1");
                        a = 1;
                    }
                    slHeight.setValue(a);

                } catch (NumberFormatException ex) {
                }
            }
        });
        // call onCancel() when cross is clicked
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onCancel();
            }
        });

        // call onCancel() on ESCAPE
        contentPane.registerKeyboardAction(e -> onCancel(), KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
    }

    private void onOK() {
        try {
            int w = Integer.valueOf(etWidth.getText());
            int h = Integer.valueOf(etHeight.getText());
            listener.onNewModelCreate(w,h, XORRadioButton.isSelected());
            dispose();
        } catch (NumberFormatException ex) {
            JOptionPane.showMessageDialog(this, "Введены неподходящие значения ширины или высоты");
        }
    }

    private void onCancel() {
        // add your code here if necessary
        dispose();
    }

    public interface OnNewModelCreateListener {
        void onNewModelCreate(int w, int h, boolean setXorMode);
    }

}
