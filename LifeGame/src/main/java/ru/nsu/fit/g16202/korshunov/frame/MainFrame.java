package ru.nsu.fit.g16202.korshunov.frame;

import javafx.application.Platform;
import org.omg.CORBA.Environment;
import ru.nsu.fit.g16202.korshunov.data.OptionsStorage;
import ru.nsu.fit.g16202.korshunov.game.model.GameModel;
import ru.nsu.fit.g16202.korshunov.game.presenter.LifeGamePresenter;
import ru.nsu.fit.g16202.korshunov.game.view.GamePanel;
import ru.nsu.fit.g16202.korshunov.ui.StatusBar;
import ru.nsu.fit.g16202.korshunov.ui.dialog.AboutDialog;
import ru.nsu.fit.g16202.korshunov.ui.dialog.NewModelDialog;
import ru.nsu.fit.g16202.korshunov.ui.dialog.OptionsDialog;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.lang.management.PlatformLoggingMXBean;

public class MainFrame extends JFrame {

    private JMenuBar jMenuBar;
    private JToolBar jToolBar;
    private StatusBar statusBar;
    private LifeGamePresenter presenter;

    //toolbar
    private JButton newButton;
    private JButton openButton;
    private JButton saveButton;
    private JButton optionsButton;
    private JToggleButton flipToggleButton;
    private JToggleButton impactToggleButton;
    private JButton clearButton;
    private JButton nextButton;
    private JToggleButton timerToggleButton;
    private JButton aboutButton;

    //menu
    private JMenuItem newJMenuButton;
    private JMenuItem openJMenuItem;
    private JMenuItem saveJMenuItem;
    private JMenuItem optionsJMenuItem;
    private JCheckBoxMenuItem flipJCheckboxMenuItem;
    private JMenuItem clearJMenuItem;
    private JMenuItem nextStepJMenuItem;
    private JCheckBoxMenuItem impactJCheckboxMenuItem;
    private JCheckBoxMenuItem timerJCheckboxMenuItem;
    private JMenuItem aboutJMenuItem;
    private JMenuItem exitJMenuItem;


    public static void main(String[] args) {
        new MainFrame();
    }

    private MainFrame() {
        super();
        Dimension minDim = new Dimension(800, 600);
        setSize(minDim);
        setMinimumSize(minDim);
        setTitle("Игра \"Жизнь\"");
        setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
        Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
        this.setLocation(dim.width / 2 - this.getSize().width / 2, dim.height / 2 - this.getSize().height / 2);
        this.setLayout(new BorderLayout());
        GamePanel view = new GamePanel();
        GameModel model = new GameModel();
        presenter = new LifeGamePresenter(view, model);
        addWindowListener(new java.awt.event.WindowAdapter() {
            @Override
            public void windowClosing(java.awt.event.WindowEvent windowEvent) {
                if (OptionsStorage.getInstance().isSaved()) {
                    System.exit(0);
                }
                if (JOptionPane.showConfirmDialog(MainFrame.this,
                        "Хотите сохранить модель перед выходом?", "Сохранение",
                        JOptionPane.YES_NO_OPTION,
                        JOptionPane.QUESTION_MESSAGE) == JOptionPane.NO_OPTION){
                    System.exit(0);
                } else {
                    presenter.saveModelToFile();
                    System.exit(0);
                }
            }
        });
        initMenu();
        initStatusBar();
        initToolbar();
        initCenterPanel();
        presenter.startNewGame(10, 7);
        pack();
        setVisible(true);
    }

    private void initCenterPanel() {
        this.add(presenter.getViewForDrawing(), BorderLayout.CENTER);
    }

    private void initStatusBar() {
        statusBar = new StatusBar(this);
        this.add(statusBar, BorderLayout.SOUTH);
    }

    private void initToolbar() {
        jToolBar = new JToolBar();

        newButton = new JButton(new ImageIcon(ClassLoader.getSystemResource("NewDoc.gif")));
        newButton.setToolTipText("Новая модель");
        newButton.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
                openNewModelDialog();
            }

            @Override
            public void mousePressed(MouseEvent e) {

            }

            @Override
            public void mouseReleased(MouseEvent e) {

            }

            @Override
            public void mouseEntered(MouseEvent e) {
                statusBar.setStatus("Новая модель");
            }

            @Override
            public void mouseExited(MouseEvent e) {
                statusBar.setStatus("");
            }
        });
        jToolBar.add(newButton);

        openButton = new JButton(new ImageIcon(ClassLoader.getSystemResource("folder.png")));
        openButton.setToolTipText("Открыть модель из файла");
        openButton.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
                presenter.openModelFromFile();
            }

            @Override
            public void mousePressed(MouseEvent e) {

            }

            @Override
            public void mouseReleased(MouseEvent e) {

            }

            @Override
            public void mouseEntered(MouseEvent e) {
                statusBar.setStatus("Открыть модель из файла");
            }

            @Override
            public void mouseExited(MouseEvent e) {
                statusBar.setStatus("");
            }
        });
        jToolBar.add(openButton);

        saveButton = new JButton(new ImageIcon(ClassLoader.getSystemResource("save.png")));
        saveButton.setToolTipText("Сохранить модель");
        saveButton.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
                presenter.saveModelToFile();
            }

            @Override
            public void mousePressed(MouseEvent e) {

            }

            @Override
            public void mouseReleased(MouseEvent e) {

            }

            @Override
            public void mouseEntered(MouseEvent e) {
                statusBar.setStatus("Сохранить модель");
            }

            @Override
            public void mouseExited(MouseEvent e) {
                statusBar.setStatus("");
            }
        });
        jToolBar.add(saveButton);

        optionsButton = new JButton(new ImageIcon(ClassLoader.getSystemResource("Settings.png")));
        optionsButton.setToolTipText("Опции");
        optionsButton.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
                openOptions();
            }

            @Override
            public void mousePressed(MouseEvent e) {

            }

            @Override
            public void mouseReleased(MouseEvent e) {

            }

            @Override
            public void mouseEntered(MouseEvent e) {
                statusBar.setStatus("Опции");
            }

            @Override
            public void mouseExited(MouseEvent e) {
                statusBar.setStatus("");
            }
        });
        jToolBar.add(optionsButton);

        flipToggleButton = new JToggleButton(new ImageIcon(ClassLoader.getSystemResource("bucket.png")));
        flipToggleButton.setToolTipText("Режим XOR/Режим Replace");
        flipToggleButton.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
                flipMode();
                if (flipJCheckboxMenuItem.isSelected()) {
                    flipJCheckboxMenuItem.setSelected(false);
                } else {
                    flipJCheckboxMenuItem.setSelected(true);
                }
            }

            @Override
            public void mousePressed(MouseEvent e) {

            }

            @Override
            public void mouseReleased(MouseEvent e) {

            }

            @Override
            public void mouseEntered(MouseEvent e) {
                statusBar.setStatus("Режим XOR/Режим Replace");
            }

            @Override
            public void mouseExited(MouseEvent e) {
                statusBar.setStatus("");
            }
        });
        flipToggleButton.setSelected(true);
        jToolBar.add(flipToggleButton);

        clearButton = new JButton(new ImageIcon(ClassLoader.getSystemResource("cross.png")));
        clearButton.setToolTipText("Очистить поле");
        clearButton.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
                clearField();
            }

            @Override
            public void mousePressed(MouseEvent e) {

            }

            @Override
            public void mouseReleased(MouseEvent e) {

            }

            @Override
            public void mouseEntered(MouseEvent e) {
                statusBar.setStatus("Очистить поле");
            }

            @Override
            public void mouseExited(MouseEvent e) {
                statusBar.setStatus("");
            }
        });
        jToolBar.add(clearButton);

        impactToggleButton = new JToggleButton(new ImageIcon(ClassLoader.getSystemResource("number.png")));
        impactToggleButton.setToolTipText("Показывать влияние");
        impactToggleButton.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
                if (impactToggleButton.isSelected()) {
                    impactJCheckboxMenuItem.setSelected(true);
                    presenter.drawImpacts();
                } else {
                    impactJCheckboxMenuItem.setSelected(false);
                    presenter.clearImpacts();
                }
            }

            @Override
            public void mousePressed(MouseEvent e) {

            }

            @Override
            public void mouseReleased(MouseEvent e) {

            }

            @Override
            public void mouseEntered(MouseEvent e) {
                if (timerToggleButton.getModel().isPressed()) {
                    statusBar.setStatus("Скрыть значения влияния");
                } else {
                    statusBar.setStatus("Отображать значения влияние");
                }
            }

            @Override
            public void mouseExited(MouseEvent e) {
                statusBar.setStatus("");
            }
        });
        jToolBar.add(impactToggleButton);

        nextButton = new JButton(new ImageIcon(ClassLoader.getSystemResource("timer.png")));
        nextButton.setToolTipText("Следующий шаг");
        nextButton.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
                presenter.nextStep();
            }

            @Override
            public void mousePressed(MouseEvent e) {

            }

            @Override
            public void mouseReleased(MouseEvent e) {
            }

            @Override
            public void mouseEntered(MouseEvent e) {
                statusBar.setStatus("Следующий шаг");
            }

            @Override
            public void mouseExited(MouseEvent e) {
                statusBar.setStatus("");
            }
        });
        jToolBar.add(nextButton);

        timerToggleButton = new JToggleButton(new ImageIcon(ClassLoader.getSystemResource("Play24.gif")));
        timerToggleButton.setToolTipText("Автоматический шаг");
        timerToggleButton.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
                actionPerformed(false);
            }

            @Override
            public void mousePressed(MouseEvent e) {

            }

            @Override
            public void mouseReleased(MouseEvent e) {

            }

            @Override
            public void mouseEntered(MouseEvent e) {
                if (timerToggleButton.getModel().isPressed()) {
                    statusBar.setStatus("Остановить автоматический шаг");
                } else {
                    statusBar.setStatus("Автоматический шаг");
                }
            }

            @Override
            public void mouseExited(MouseEvent e) {
                statusBar.setStatus("");
            }
        });
        jToolBar.add(timerToggleButton);

        aboutButton = new JButton(new ImageIcon(ClassLoader.getSystemResource("About.gif")));
        aboutButton.setToolTipText("О программе");
        aboutButton.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
                openAbout();
            }

            @Override
            public void mousePressed(MouseEvent e) {

            }

            @Override
            public void mouseReleased(MouseEvent e) {

            }

            @Override
            public void mouseEntered(MouseEvent e) {
                statusBar.setStatus("О программе");
            }

            @Override
            public void mouseExited(MouseEvent e) {
                statusBar.setStatus("");
            }
        });
        jToolBar.add(aboutButton);

        this.add(jToolBar, BorderLayout.NORTH);
    }

    private void clearField() {
        presenter.clearField();
    }

    private void flipMode() {
        presenter.setReplaceMode(!presenter.isReplaceMode());
    }

    private void openNewModelDialog() {
        NewModelDialog dialog = new NewModelDialog((w, h, isXor) -> {
            presenter.startNewGame(w, h);
            presenter.setReplaceMode(isXor);
            flipToggleButton.setSelected(!isXor);
            flipJCheckboxMenuItem.setSelected(!isXor);
        });
        dialog.setModal(true);
        dialog.pack();
        dialog.setLocationRelativeTo(this);
        dialog.setVisible(true);
    }

    private void initMenu() {
        jMenuBar = new JMenuBar();
        JMenu fileJMenu = new JMenu("Файл");
        JMenu gameJMenu = new JMenu("Игра");
        JMenu infoJMenu = new JMenu("Справка");

        newJMenuButton = new JMenuItem("Новая модель");
        newJMenuButton.addActionListener(e -> openNewModelDialog());
        fileJMenu.add(newJMenuButton);

        openJMenuItem = new JMenuItem("Открыть");
        openJMenuItem.addActionListener(e -> presenter.openModelFromFile());
        fileJMenu.add(openJMenuItem);

        saveJMenuItem = new JMenuItem("Сохранить");
        saveJMenuItem.addActionListener(e -> presenter.saveModelToFile());
        fileJMenu.add(saveJMenuItem);

        optionsJMenuItem = new JMenuItem("Опции");
        optionsJMenuItem.addActionListener(e -> openOptions());
        fileJMenu.add(optionsJMenuItem);

        flipJCheckboxMenuItem = new JCheckBoxMenuItem("Сплошная заливка");
        flipJCheckboxMenuItem.setSelected(true);
        flipJCheckboxMenuItem.addActionListener(e -> {
            flipMode();
            if (flipToggleButton.isSelected()) {
                flipToggleButton.setSelected(false);
            } else {
                flipToggleButton.setSelected(true);
            }
        });
        gameJMenu.add(flipJCheckboxMenuItem);

        clearJMenuItem = new JMenuItem("Очистить поле");
        clearJMenuItem.addActionListener(e -> presenter.clearField());
        gameJMenu.add(clearJMenuItem);

        exitJMenuItem = new JMenuItem("Выход");
        exitJMenuItem.addActionListener(e -> System.exit(0));
        fileJMenu.add(exitJMenuItem);

        timerJCheckboxMenuItem = new JCheckBoxMenuItem("Автоматический шаг");
        timerJCheckboxMenuItem.addActionListener(e -> actionPerformed(true));
        gameJMenu.add(timerJCheckboxMenuItem);

        aboutJMenuItem = new JMenuItem("О программе");
        aboutJMenuItem.addActionListener(e -> openAbout());
        infoJMenu.add(aboutJMenuItem);


        impactJCheckboxMenuItem = new JCheckBoxMenuItem("Показывать влияние");
        impactJCheckboxMenuItem.addActionListener(e -> {
            if (impactJCheckboxMenuItem.isSelected()) {
                impactToggleButton.setSelected(true);
                presenter.drawImpacts();
            } else {
                impactToggleButton.setSelected(false);
                presenter.clearImpacts();
            }
        });
        gameJMenu.add(impactJCheckboxMenuItem);

        nextStepJMenuItem = new JMenuItem("Следующий шаг");
        nextStepJMenuItem.addActionListener(e -> presenter.nextStep());
        gameJMenu.add(nextStepJMenuItem);

        jMenuBar.add(fileJMenu);
        jMenuBar.add(gameJMenu);
        jMenuBar.add(infoJMenu);
        this.setJMenuBar(jMenuBar);
    }

    private void openAbout() {
        AboutDialog dialog = new AboutDialog();
        dialog.setModal(true);
        dialog.pack();
        dialog.setLocationRelativeTo(this);
        dialog.setVisible(true);
    }

    private void openOptions() {
        OptionsDialog dialog = new OptionsDialog((x, y) -> presenter.changeSize(x, y),
                presenter.getModel().getWidth(), presenter.getModel().getHeight());
        dialog.setModal(true);
        dialog.pack();
        dialog.setLocationRelativeTo(this);
        dialog.setVisible(true);
    }

    private void actionPerformed(boolean isMenu) {
        presenter.flipTimer();
        if (isMenu) {
            if (timerJCheckboxMenuItem.isSelected()) {
                timerToggleButton.setSelected(true);
                nextStepJMenuItem.setEnabled(false);
                nextButton.setEnabled(false);
            } else {
                timerToggleButton.setSelected(false);
                nextButton.setEnabled(true);
                nextStepJMenuItem.setEnabled(true);
            }
        } else {
            if (timerToggleButton.isSelected()) {
                timerJCheckboxMenuItem.setSelected(true);
                nextStepJMenuItem.setEnabled(false);
                nextButton.setEnabled(false);
            } else {
                timerJCheckboxMenuItem.setSelected(false);
                nextButton.setEnabled(true);
                nextStepJMenuItem.setEnabled(true);
            }
        }
    }
}
