package ru.nsu.fit.g16202.korshunov.data;

public class OptionsStorage {

    private volatile static OptionsStorage instance;

    public static final int MAX_THICKNESS = 5;
    public static final int MAX_CELLSIZE = 50;
    public static final int MIN_CELLSIZE = 3;

    private int lineThickness = 1;
    private int cellSize = 25;
    private int modelWidth = 10;
    private int modelHeight = 10;
    private double liveBegin = 2.0;
    private double LiveEnd = 3.3;
    private double birthBegin = 2.3;
    private double birthEnd = 2.9;
    private double firstImpact = 1.0;
    private double secondImpact = 0.3;
    private boolean isSaved = true;

    private OptionsStorage() {
    }

    public boolean isSaved() {
        return isSaved;
    }

    public void setSaved(boolean saved) {
        isSaved = saved;
    }


    public int getCellSize() {
        return cellSize;
    }

    public void setCellSize(int cellSize) {
        this.cellSize = cellSize;
    }

    public int getModelWidth() {
        return modelWidth;
    }

    public void setModelWidth(int modelWidth) {
        this.modelWidth = modelWidth;
    }

    public int getModelHeight() {
        return modelHeight;
    }

    public void setModelHeight(int modelHeight) {
        this.modelHeight = modelHeight;
    }

    public double getLiveBegin() {
        return liveBegin;
    }

    public void setLiveBegin(double liveBegin) {
        this.liveBegin = liveBegin;
    }

    public double getLiveEnd() {
        return LiveEnd;
    }

    public void setLiveEnd(double liveEnd) {
        LiveEnd = liveEnd;
    }

    public double getBirthBegin() {
        return birthBegin;
    }

    public void setBirthBegin(double birthBegin) {
        this.birthBegin = birthBegin;
    }

    public double getBirthEnd() {
        return birthEnd;
    }

    public void setBirthEnd(double birthEnd) {
        this.birthEnd = birthEnd;
    }

    public double getFirstImpact() {
        return firstImpact;
    }

    public void setFirstImpact(double firstImpact) {
        this.firstImpact = firstImpact;
    }

    public double getSecondImpact() {
        return secondImpact;
    }

    public void setSecondImpact(double secondImpact) {
        this.secondImpact = secondImpact;
    }

    public static OptionsStorage getInstance() {
        if (instance == null) {
            synchronized (OptionsStorage.class) {
                if (instance == null) {
                    instance = new OptionsStorage();
                }
            }
        }
        return instance;
    }

    public int getLineThickness() {
        return lineThickness;
    }

    public void setLineThickness(int lineThickness) {
        this.lineThickness = lineThickness;
    }
}
