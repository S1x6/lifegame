package ru.nsu.fit.g16202.korshunov.ui.dialog;

import ru.nsu.fit.g16202.korshunov.data.OptionsStorage;

import javax.swing.*;
import javax.swing.border.BevelBorder;
import java.awt.event.*;

public class OptionsDialog extends JDialog {
    private JPanel contentPane;
    private JButton buttonOK;
    private JButton buttonCancel;
    private JTextField etLB;
    private JTextField etLE;
    private JTextField etBB;
    private JTextField etBE;
    private JTextField etFI;
    private JTextField etSI;
    private JTextField etThickness;
    private JTextField etCellSize;
    private JSlider slThickness;
    private JSlider slCellsize;
    private JTextField etWidth;
    private JTextField etHeight;
    private JSlider slWidth;
    private JSlider slHeight;
    private OnOptionsApplyListener listener;

    public OptionsDialog(OnOptionsApplyListener listener, int x, int y) {
        setContentPane(contentPane);
        setModal(true);
        getRootPane().setDefaultButton(buttonOK);
        OptionsStorage storage = OptionsStorage.getInstance();
        this.listener = listener;
        setTitle("Опции");
        setResizable(false);
        etLB.setText(String.valueOf(storage.getLiveBegin()));
        etLE.setText(String.valueOf(storage.getLiveEnd()));
        etBB.setText(String.valueOf(storage.getBirthBegin()));
        etBE.setText(String.valueOf(storage.getBirthEnd()));
        etFI.setText(String.valueOf(storage.getFirstImpact()));
        etSI.setText(String.valueOf(storage.getSecondImpact()));
        etThickness.setText(String.valueOf(storage.getLineThickness()));
        slThickness.setValue(storage.getLineThickness());
        slCellsize.setValue(storage.getCellSize());
        etCellSize.setText(String.valueOf(storage.getCellSize()));

        buttonOK.addActionListener(e -> onOK());
        buttonCancel.addActionListener(e -> onCancel());

        etWidth.setText(String.valueOf(x));
        slWidth.setValue(x);
        slWidth.addChangeListener(e -> etWidth.setText(String.valueOf(slWidth.getValue())));
        etWidth.addKeyListener(new KeyListener() {
            @Override
            public void keyTyped(KeyEvent e) {
            }

            @Override
            public void keyPressed(KeyEvent e) {

            }

            @Override
            public void keyReleased(KeyEvent e) {
                try {
                    int a = Integer.valueOf(etWidth.getText());
                    if (a > 100) {
                        etWidth.setText("100");
                        a = 100;
                    } else if (a < 1) {
                        etWidth.setText("1");
                        a = 1;
                    }
                    slWidth.setValue(a);

                } catch (NumberFormatException ex) {
                }
            }
        });

        etHeight.setText(String.valueOf(y));
        slHeight.setValue(y);
        slHeight.addChangeListener(e -> etHeight.setText(String.valueOf(slHeight.getValue())));
        etHeight.addKeyListener(new KeyListener() {
            @Override
            public void keyTyped(KeyEvent e) {
            }

            @Override
            public void keyPressed(KeyEvent e) {

            }

            @Override
            public void keyReleased(KeyEvent e) {
                try {
                    int a = Integer.valueOf(etHeight.getText());
                    if (a > 100) {
                        etHeight.setText("100");
                        a = 100;
                    } else if (a < 1) {
                        etHeight.setText("1");
                        a = 1;
                    }
                    slHeight.setValue(a);

                } catch (NumberFormatException ex) {
                }
            }
        });

        slThickness.addChangeListener(e -> etThickness.setText(String.valueOf(slThickness.getValue())));
        etThickness.addKeyListener(new KeyListener() {
            @Override
            public void keyTyped(KeyEvent e) {
            }

            @Override
            public void keyPressed(KeyEvent e) {

            }

            @Override
            public void keyReleased(KeyEvent e) {
                try {
                    int a = Integer.valueOf(etThickness.getText());
                    if (a > 5) {
                        etThickness.setText("100");
                        a = 5;
                    } else if (a < 1) {
                        etThickness.setText("1");
                        a = 1;
                    }
                    slThickness.setValue(a);

                } catch (NumberFormatException ex) {
                }
            }
        });

        slCellsize.addChangeListener(e -> etCellSize.setText(String.valueOf(slCellsize.getValue())));
        etCellSize.addKeyListener(new KeyListener() {
            @Override
            public void keyTyped(KeyEvent e) {
            }

            @Override
            public void keyPressed(KeyEvent e) {

            }

            @Override
            public void keyReleased(KeyEvent e) {
                try {
                    int a = Integer.valueOf(etCellSize.getText());
                    if (a > 50) {
                        etCellSize.setText("100");
                        a = 50;
                    } else if (a < 1) {
                        etCellSize.setText("1");
                        a = 1;
                    }
                    slCellsize.setValue(a);

                } catch (NumberFormatException ex) {
                }
            }
        });

        // call onCancel() when cross is clicked
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onCancel();
            }
        });
        contentPane.registerKeyboardAction(e -> onCancel(), KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
    }

    private void onOK() {
        OptionsStorage storage = OptionsStorage.getInstance();
        try {
            if (Double.valueOf(etLB.getText()) < 0 ||
                    Double.valueOf(etLE.getText()) < 0 ||
                    Double.valueOf(etBB.getText()) < 0 ||
                    Double.valueOf(etBE.getText()) < 0 ||
                    Double.valueOf(etFI.getText()) < 0 ||
                    Double.valueOf(etSI.getText()) < 0) {
                JOptionPane.showMessageDialog(this,
                        "Все введенные значения должны быть неотрицательными",
                        "Ошибка",
                        JOptionPane.ERROR_MESSAGE);
            }
            if (Double.valueOf(etLB.getText()) > Double.valueOf(etBB.getText())) {
                JOptionPane.showMessageDialog(this,
                        "LIVE_BEGIN должно быть меньше либо равно BIRTH_BEGIN",
                        "Ошибка",
                        JOptionPane.ERROR_MESSAGE);
                return;
            }
            if (Double.valueOf(etBB.getText()) > Double.valueOf(etBE.getText())) {
                JOptionPane.showMessageDialog(this,
                        "BIRTH_BEGIN должно быть меньше либо равно BIRTH_END",
                        "Ошибка",
                        JOptionPane.ERROR_MESSAGE);
                return;
            }
            if (Double.valueOf(etBE.getText()) > Double.valueOf(etLE.getText())) {
                JOptionPane.showMessageDialog(this,
                        "BIRTH_END должно быть меньше либо равно LIVE_END",
                        "Ошибка",
                        JOptionPane.ERROR_MESSAGE);
                return;
            }
            if (Double.valueOf(etThickness.getText()) < 1 || Double.valueOf(etThickness.getText()) > OptionsStorage.MAX_THICKNESS) {
                JOptionPane.showMessageDialog(this,
                        "Толщина линии должна выть в диапазоне от 1 до " + OptionsStorage.MAX_THICKNESS + " включительно",
                        "Ошибка",
                        JOptionPane.ERROR_MESSAGE);
                return;
            }
            if (Double.valueOf(etCellSize.getText()) < OptionsStorage.MIN_CELLSIZE
                    || Double.valueOf(etCellSize.getText()) > OptionsStorage.MAX_CELLSIZE) {
                JOptionPane.showMessageDialog(this,
                        "Размер клетки должен выть в диапазоне от " + OptionsStorage.MIN_CELLSIZE +
                                " до " + OptionsStorage.MAX_CELLSIZE + " включительно",
                        "Ошибка",
                        JOptionPane.ERROR_MESSAGE);
                return;
            }
            storage.setLiveBegin(Double.valueOf(etLB.getText()));
            storage.setLiveEnd(Double.valueOf(etLE.getText()));
            storage.setBirthBegin(Double.valueOf(etBB.getText()));
            storage.setBirthEnd(Double.valueOf(etBE.getText()));
            storage.setFirstImpact(Double.valueOf(etFI.getText()));
            storage.setSecondImpact(Double.valueOf(etSI.getText()));
            storage.setLineThickness(Integer.valueOf(etThickness.getText()));
            storage.setCellSize(Integer.valueOf(etCellSize.getText()));
            listener.onOptionsApplyListener(Integer.valueOf(etWidth.getText()), Integer.valueOf(etHeight.getText()));
        } catch (NumberFormatException ex) {
            JOptionPane.showMessageDialog(this,
                    "Некоторые значения не являются числами",
                    "Ошибка",
                    JOptionPane.ERROR_MESSAGE);
            return;
        }
        dispose();
    }

    private void onCancel() {
        dispose();
    }

    public interface OnOptionsApplyListener {
        void onOptionsApplyListener(int newX, int newY);
    }
}
