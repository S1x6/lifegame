package ru.nsu.fit.g16202.korshunov.game.view;

import ru.nsu.fit.g16202.korshunov.game.model.Point;
import ru.nsu.fit.g16202.korshunov.game.presenter.LifeGamePresenter;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.image.BufferedImage;
import java.util.ArrayList;

public class GamePanel extends JPanel {

    public static final int COLOR_ALIVE = Color.green.getRGB();
    public static final int COLOR_DEAD = Color.white.getRGB();

    private JScrollPane scrollPane;
    private FieldDrawer drawer = new FieldDrawer();
    private LifeGamePresenter presenter;
    private int width = 0;
    private int height = 0;
    private BufferedImage image;
    private BufferedImage impactImage;

    public GamePanel() {
        scrollPane = new JScrollPane(this);
        scrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
        scrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        this.addMouseMotionListener(new MouseMotionListener() {
            @Override
            public void mouseDragged(MouseEvent e) {
                presenter.hexagonHovered(e.getX(), e.getY());
            }

            @Override
            public void mouseMoved(MouseEvent e) {
            }
        });
        this.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
                presenter.hexagonHovered(e.getX(), e.getY());
            }

            @Override
            public void mousePressed(MouseEvent e) {

            }

            @Override
            public void mouseReleased(MouseEvent e) {
                presenter.mouseReleased();
            }

            @Override
            public void mouseEntered(MouseEvent e) {

            }

            @Override
            public void mouseExited(MouseEvent e) {

            }
        });
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        g.drawImage(image, 0, 0, image.getWidth(), image.getHeight(), this);
        if (impactImage != null) {
            g.drawImage(impactImage, 0, 0, impactImage.getWidth(), impactImage.getHeight(), this);
        }
    }

    public void redraw(ArrayList<ArrayList<Boolean>> stateMatrix) {
        image = drawer.drawField(width, height, stateMatrix);
        this.setPreferredSize(new Dimension(image.getWidth(), image.getHeight()));
        revalidate();
        repaint();
    }

    public void updateImpactImage(ArrayList<ArrayList<Double>> impactMatrix) {
        if (impactImage != null) {
            makeImpactsImage(impactMatrix);
        }
    }

    public void makeImpactsImage(ArrayList<ArrayList<Double>> impactMatrix) {
        impactImage = drawer.drawImpacts(image.getWidth(), image.getHeight(), impactMatrix);
        repaint();
    }

    public void clearImpactImage() {
        impactImage = null;
        repaint();
    }

    public Component getViewForDrawing() {
        return scrollPane;
    }

    public void setFieldSize(int width, int height) {
        this.width = width;
        this.height = height;
        redraw(null);
    }

    public void setPresenter(LifeGamePresenter presenter) {
        this.presenter = presenter;
    }

    public void fillAreaAt(int x, int y, int color) {
        BufferedImage im = drawer.fillArea(image, x, y, color);
        if (im != null) {
            repaint();
        }
    }
    public boolean isBorder(int x, int y) {
        if (x >= image.getWidth() || y >= image.getHeight())
             return true;
        return image.getRGB(x, y) == Color.black.getRGB();
    }
}
