package ru.nsu.fit.g16202.korshunov.game.presenter;

import ru.nsu.fit.g16202.korshunov.data.OptionsStorage;
import ru.nsu.fit.g16202.korshunov.game.model.Point;

import javax.swing.*;
import javax.swing.filechooser.FileFilter;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.*;
import java.io.*;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.NoSuchElementException;
import java.util.Scanner;

public class ModelFileHandler {

    public ModelFileHandler() {
    }

    public GameOptionsSet readDataFromFile(Component parent) {
        final JFileChooser fc = new JFileChooser();
        FileFilter filter = new FileNameExtensionFilter("Life Game format (.txt)", "txt");
        fc.setFileFilter(filter);
        File dataDir = new File(".\\FIT_16202_Korshunov_Life_Data");
        if (!Files.exists(dataDir.toPath())) {
            try {
                Files.createDirectory(dataDir.toPath());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        fc.setCurrentDirectory(dataDir);
        int ret = fc.showDialog(parent, "Открыть модель");

        if (ret == JFileChooser.APPROVE_OPTION) {
            File file = fc.getSelectedFile();
            try {
                GameOptionsSet optionsSet = scanFile(file);
                if (optionsSet == null) {
                    JOptionPane.showMessageDialog(parent, "Файл неверного формата");
                }
                return optionsSet;
            } catch (FileNotFoundException ex) {
                ex.printStackTrace();
                return null;
            }
        }
        return null;
    }

    private GameOptionsSet scanFile(File file) throws FileNotFoundException {
        Scanner scanner = new Scanner(file);
        try {
            String firstLine = scanner.nextLine();
            firstLine = getLineWithoutComment(firstLine);
            String[] nums = firstLine.split(" ");
            if (nums.length != 2) {
                return null;
            }
            int m = Integer.valueOf(nums[0]);
            int n = Integer.valueOf(nums[1]);
            if (m < 1 || m > 100 || n < 1 || n > 100) {
                return null;
            }
            String secondLine = scanner.nextLine();
            int lineThickness = Integer.valueOf(getLineWithoutComment(secondLine));
            if (lineThickness < 1 || lineThickness > 5) {
                return null;
            }
            String thirdLine = scanner.nextLine();
            int cellSize = Integer.valueOf(getLineWithoutComment(thirdLine));
            if (cellSize < 5 || cellSize > 50) {
                return null;
            }
            String fourthLine = scanner.nextLine();
            int numOfAlive = Integer.valueOf(getLineWithoutComment(fourthLine));
            ArrayList<Point> aliveList = new ArrayList<>();
            for (int i = 0; i < numOfAlive; ++i) {
                String line = scanner.nextLine();
                line = getLineWithoutComment(line);
                String[] coords = line.split(" ");
                if (coords.length != 2) {
                    return null;
                }
                int x = Integer.valueOf(coords[0]);
                int y = Integer.valueOf(coords[1]);
                boolean isOdd = y % 2 == 1;
                if (x < 0 || (x >= m && !isOdd) || (x >= m - 1 && isOdd) || y < 0 || y >= n) {
                    return null;
                }
                aliveList.add(new Point(x, y));
            }
            while (scanner.hasNextLine() && getLineWithoutComment(scanner.nextLine()).equals(""));
            if (scanner.hasNextLine()) {
                return null;
            }
            return new GameOptionsSet(m, n, cellSize, lineThickness, aliveList);
        } catch (NumberFormatException | NoSuchElementException ex) {
            return null;
        }
    }

    public void saveModelToFile(Component parent, int m, int n, ArrayList<ArrayList<Boolean>> stateMatrix) {
        final JFileChooser fc = new JFileChooser();
        FileFilter filter = new FileNameExtensionFilter("Life Game format (.txt)", "txt");
        fc.setFileFilter(filter);
        File dataDir = new File(".\\FIT_16202_Korshunov_Life_Data");
        if (!Files.exists(dataDir.toPath())) {
            try {
                Files.createDirectory(dataDir.toPath());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        fc.setCurrentDirectory(dataDir);
        int ret = fc.showDialog(parent, "Сохранить модель");

        if (ret == JFileChooser.APPROVE_OPTION) {
            File file = fc.getSelectedFile();
            try {
                if (!file.toPath().endsWith(".txt")) {
                    file = new File(file.toPath().toString() + ".txt");
                }
                System.out.println(file.toPath());
                Files.deleteIfExists(file.toPath());
                file.createNewFile();
                int aliveCells = 0;
                ArrayList<Point> alivePoints = new ArrayList<>();
                for (int j = 0; j < stateMatrix.size(); ++j) {
                    ArrayList<Boolean> list = stateMatrix.get(j);
                    for (int i = 0; i < list.size(); ++i) {
                        if (list.get(i)) {
                            aliveCells++;
                            alivePoints.add(new Point(i,j));
                        }
                    }
                }
                FileWriter writer = new FileWriter(file);
                writer.write(m + " " + n + System.lineSeparator());
                writer.write(OptionsStorage.getInstance().getLineThickness() + System.lineSeparator());
                writer.write(OptionsStorage.getInstance().getCellSize() + System.lineSeparator());
                writer.write(aliveCells + System.lineSeparator());
                for (Point alivePoint : alivePoints) {
                    writer.write(alivePoint.getX() + " " + alivePoint.getY() + System.lineSeparator());
                }
                writer.flush();
                writer.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private String getLineWithoutComment(String line) {
        int commentIndex = line.indexOf("//");
        if (commentIndex != -1) {
            line = line.substring(0, commentIndex);
        }
        line = line.trim();
        return line;
    }

}
