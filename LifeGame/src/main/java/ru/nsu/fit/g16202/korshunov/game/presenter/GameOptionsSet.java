package ru.nsu.fit.g16202.korshunov.game.presenter;

import ru.nsu.fit.g16202.korshunov.game.model.Point;

import java.util.ArrayList;

public class GameOptionsSet {
    private int m;
    private int n;
    private int cellSize;
    private int lineThickness;
    private ArrayList<Point> alivePoints;

    public GameOptionsSet(int m, int n, int cellSize, int lineThickness, ArrayList<Point> alivePoints) {
        this.m = m;
        this.n = n;
        this.cellSize = cellSize;
        this.lineThickness = lineThickness;
        this.alivePoints = alivePoints;
    }

    public int getM() {
        return m;
    }

    public void setM(int m) {
        this.m = m;
    }

    public int getN() {
        return n;
    }

    public void setN(int n) {
        this.n = n;
    }

    public int getCellSize() {
        return cellSize;
    }

    public void setCellSize(int cellSize) {
        this.cellSize = cellSize;
    }

    public int getLineThickness() {
        return lineThickness;
    }

    public void setLineThickness(int lineThickness) {
        this.lineThickness = lineThickness;
    }

    public ArrayList<Point> getAlivePoints() {
        return alivePoints;
    }

    public void setAlivePoints(ArrayList<Point> alivePoints) {
        this.alivePoints = alivePoints;
    }
}
