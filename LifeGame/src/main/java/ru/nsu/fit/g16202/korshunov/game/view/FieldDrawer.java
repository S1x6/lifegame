package ru.nsu.fit.g16202.korshunov.game.view;

import ru.nsu.fit.g16202.korshunov.data.OptionsStorage;
import ru.nsu.fit.g16202.korshunov.game.model.Point;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Stack;

public class FieldDrawer {

    public static final int FIELD_MARGIN = 10;

    private ArrayList<ArrayList<Point>> hexCenters = new ArrayList<>();

    FieldDrawer() {
    }

    private void drawLine(Graphics g, int fx, int fy, int tx, int ty) {
        int thickness = OptionsStorage.getInstance().getLineThickness();
        Graphics2D g2 = (Graphics2D) g;
        g2.setStroke(new BasicStroke(thickness));
        if (thickness != 1) {
            g.drawLine(fx, fy, tx, ty);
            return;
        }

        int dx = tx - fx;
        int dy = ty - fy;
        int xDir = Integer.compare(dx, 0); //dx < 0 ? -1 : (dx > 0 ? 1 : 0);
        int yDir = Integer.compare(dy, 0); //dx < 0 ? -1 : (dy > 0 ? 1 : 0);
        int aDx = Math.abs(dx);
        int aDy = Math.abs(dy);

        int length;
        int x = fx;
        int y = fy;
        int err = 0;
        g.drawLine(x, y, x, y);
        if (aDx > aDy) {
            length = aDx;
            for (int t = 0; t < length; t++) {
                err += 2 * aDy;
                if (err > aDx) {
                    err -= 2 * aDx;
                    y += yDir;
                }
                x += xDir;
                g.drawLine(x, y, x, y);
            }
        } else {
            length = aDy;
            for (int t = 0; t < length; t++) {
                err += 2 * aDx;
                if (err > aDy) {
                    err -= 2 * aDy;
                    x += xDir;
                }
                y += yDir;
                g.drawLine(x, y, x, y);
            }
        }
    }

    private void drawHexagon(Graphics g, int x, int y, int length) {
        int width = (int) (length * Math.sqrt(3));
        if (width % 2 == 1) {
            width++;
        }
        width /= 2;
        drawLine(g, x, y - length, (x + width), y - length / 2);
        drawLine(g, (x + width), y - length / 2, (x + width), y + length / 2);
        drawLine(g, x, y + length, (x + width), y + length / 2);
        drawLine(g, (x - width), y + length / 2, x, y + length);
        drawLine(g, (x - width), y - length / 2, (x - width), y + length / 2);
        drawLine(g, (x - width), y - length / 2, x, y - length);
    }

    BufferedImage drawField(int m, int n, ArrayList<ArrayList<Boolean>> stateMatrix) {
        int length = OptionsStorage.getInstance().getCellSize();
        hexCenters.clear();
        int width = (int) (length * Math.sqrt(3));
        if (width % 2 == 1) {
            width++;
        }
        int w = (m * (width)) + OptionsStorage.getInstance().getLineThickness() * 2 + FIELD_MARGIN * 2;
        int h = (n - 1) * (int) (length * 1.5) + 2 * length + 2 * FIELD_MARGIN;
        BufferedImage image = new BufferedImage(w, h, BufferedImage.TYPE_INT_RGB);
        Graphics2D g = image.createGraphics();
        g.setColor(Color.WHITE);
        g.fillRect(0, 0, w, h);
        g.setColor(Color.BLACK);

        int x;
        int y = length + FIELD_MARGIN;
        int elInRow;
        for (int i = 0; i < n; ++i) {
            hexCenters.add(new ArrayList<>());
            x = width / 2 + OptionsStorage.getInstance().getLineThickness() / 2 + FIELD_MARGIN;
            if (i % 2 == 1) {
                x += width / 2;
                elInRow = m - 1;
            } else {
                elInRow = m;
            }
            for (int j = 0; j < elInRow; ++j) {
                hexCenters.get(i).add(new Point(x, y));
                drawHexagon(g, x, y, length);
                if (stateMatrix != null && stateMatrix.get(i).get(j)) {
                    fillArea(image, x, y, Color.green.getRGB());
                }
                x += width;
            }
            y += length * 1.5;
        }
        g.dispose();
         fillArea(image, 1, 1, Color.decode("#EEEEEE").getRGB());
        return image;
    }

    BufferedImage fillArea(BufferedImage image, int x, int y, int color) {
        if (image.getRGB(x, y) == Color.BLACK.getRGB() ||
                image.getRGB(x, y) == color) {
            return null;
        }
        Stack<Span> spans = new Stack<>();
        int oc = image.getRGB(x, y);
        int xLeft = x;
        int xRight = x;
        while (xLeft > 0 && image.getRGB(xLeft, y) == oc) {
            xLeft--;
        }
        while (xRight < image.getWidth() && image.getRGB(xRight, y) == oc) {
            xRight++;
        }
        spans.push(new Span(xLeft, xRight, y));
        while (spans.size() != 0) {
            Span currentSpan = spans.pop();
            currentSpan.fill(image, color);
            xLeft = currentSpan.getFx() + 1; // spans above
            int y1 = currentSpan.getY() - 1;
            if (y1 < 0) {
                continue;
            }
            while (xLeft > 0 && image.getRGB(xLeft, y1) == oc) {
                xLeft--;
            }
            xRight = currentSpan.getFx() + 1;
            makeSpans(image, spans, oc, xLeft, xRight, currentSpan, y1);

            xLeft = currentSpan.getFx() + 1; // spans below
            y1 = currentSpan.getY() + 1;
            if (y1 == image.getHeight()) {
                continue;
            }
            while (xLeft > 0 && image.getRGB(xLeft, y1) == oc) {
                xLeft--;
            }
            xRight = currentSpan.getFx() + 1;
            makeSpans(image, spans, oc, xLeft, xRight, currentSpan, y1);
        }
        return image;
    }

    private void makeSpans(BufferedImage image, Stack<Span> spans, int oc, int xLeft, int xRight, Span currentSpan, int y1) {
        while (xRight < currentSpan.getTx()) {
            while (xRight < image.getWidth() && image.getRGB(xRight, y1) == oc) {
                xRight++;
            }
            if (xRight - xLeft > 1) {
                spans.push(new Span(xLeft, xRight, y1));
            }
            xLeft = xRight;
            xRight++;
        }
    }

    public Point getHexCenter(int x, int y) {
        return hexCenters.get(y).get(x);
    }

    public BufferedImage drawImpacts(
            int width,
            int height,
            ArrayList<ArrayList<Double>> impactMatrix
    ) {
        BufferedImage image = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
        Graphics2D g = image.createGraphics();
        g.setColor(new Color(0f, 0f, 0f, 0f));
        g.fillRect(0, 0, width, height);
        int length = OptionsStorage.getInstance().getCellSize();
        g.setFont(new Font("TimesRoman", Font.PLAIN, (int) (length / 1.5)));
        g.setColor(Color.decode("#444444"));
        DecimalFormat df = new DecimalFormat("#.##");
        df.setRoundingMode(RoundingMode.CEILING);
        int xShift = (int) (length * Math.sqrt(3) / 2);
        int yShift = length / 2;
        for (int i = 0; i < impactMatrix.size(); ++i) {
            for (int j = 0; j < impactMatrix.get(i).size(); ++j) {
                g.drawString(df.format(impactMatrix.get(i).get(j)),
                        hexCenters.get(i).get(j).getX() - xShift + 1,
                        hexCenters.get(i).get(j).getY() + yShift);
            }
        }
        return image;
    }
}
