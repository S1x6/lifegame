package ru.nsu.fit.g16202.korshunov.game.model;

import ru.nsu.fit.g16202.korshunov.data.OptionsStorage;

import java.util.ArrayList;

public class GameModel {

    private int width;
    private int height;
    private ArrayList<ArrayList<Cell>> field = new ArrayList<>();

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public GameModel() {
    }

    public void startNewGame(int width, int height) {
        this.width = width;
        this.height = height;
        makeFieldEmpty();
        for (int i = 0; i < height; ++i) {
            field.add(new ArrayList<>());
            if (i % 2 == 1) {
                width = this.width - 1;
            } else {
                width = this.width;
            }
            for (int j = 0; j < width; ++j) {
                field.get(i).add(new Cell());
            }
        }
        setNeighbours();
    }

    public void setAlive(int x, int y, boolean isAlive) {
        field.get(y).get(x).setAliveNow(isAlive);
        field.get(y).get(x).setWasAlive(isAlive);
    }

    public void flipAlive(int x, int y) {
        Cell cell = field.get(y).get(x);
        cell.setAliveNow(!cell.isAliveNow());
        cell.setWasAlive(cell.isAliveNow());
    }

    public void changeSize(int x, int y) {
        ArrayList<ArrayList<Cell>> oldField = new ArrayList<>();
        for (ArrayList list: field) {
            oldField.add(new ArrayList<Cell>(list));
        }
        startNewGame(x,y);
        int minY = Math.min(oldField.size(), field.size());
        for (int i = 0; i < minY; ++i) {
            int minX = Math.min(oldField.get(i).size(), field.get(i).size());
            for (int j = 0; j < minX; ++j) {
                if (oldField.get(i).get(j).isAliveNow()) {
                    field.get(i).get(j).setAliveNow(true);
                    field.get(i).get(j).setWasAlive(true);
                }
            }
        }
    }

    public boolean isAlive(int x, int y) {
        return field.get(y).get(x).isAliveNow();
    }

    public void nextStep() {
        for (ArrayList<Cell> aField1 : field) {
            for (Cell cell : aField1) {
                double impact = cell.getImpact();
                if (!cell.wasAlive() && impact >= OptionsStorage.getInstance().getBirthBegin() &&
                        impact <= OptionsStorage.getInstance().getBirthEnd()) {
                    cell.setAliveNow(true);
                }
                if (cell.wasAlive()) {
                    if (impact < OptionsStorage.getInstance().getLiveBegin()) {
                        cell.setAliveNow(false);
                    } else if (impact > OptionsStorage.getInstance().getLiveEnd()) {
                        cell.setAliveNow(false);
                    }
                }
            }
        }
        for (ArrayList<Cell> aField : field) {
            for (Cell cell : aField) {
                cell.setWasAlive(cell.isAliveNow());
            }
        }
    }

    private void setNeighbours() {
        int x, y;
        for (int i = 0; i < field.size(); ++i) {
            boolean isOdd = i % 2 == 1;
            for (int j = 0; j < field.get(i).size(); ++j) {
                Cell cell = field.get(i).get(j);
                if (isOdd) {
                    y = i - 1; //first neighbours
                    x = j;
                    if (checkIsInField(x, y, true)) {
                        cell.addFirstNeighbour(field.get(y).get(x));
                    }
                    y = i - 1;
                    x = j + 1;
                    if (checkIsInField(x, y, true)) {
                        cell.addFirstNeighbour(field.get(y).get(x));
                    }
                    y = i;
                    x = j + 1;
                    if (checkIsInField(x, y, true)) {
                        cell.addFirstNeighbour(field.get(y).get(x));
                    }
                    y = i;
                    x = j - 1;
                    if (checkIsInField(x, y, true)) {
                        cell.addFirstNeighbour(field.get(y).get(x));
                    }
                    y = i + 1;
                    x = j;
                    if (checkIsInField(x, y, true)) {
                        cell.addFirstNeighbour(field.get(y).get(x));
                    }
                    y = i + 1;
                    x = j + 1;
                    if (checkIsInField(x, y, true)) {
                        cell.addFirstNeighbour(field.get(y).get(x));
                    }

                    y = i - 2; // second neighbours
                    x = j;
                    if (checkIsInField(x, y, true)) {
                        cell.addSecondNeighbour(field.get(y).get(x));
                    }
                    y = i + 2;
                    x = j;
                    if (checkIsInField(x, y, true)) {
                        cell.addSecondNeighbour(field.get(y).get(x));
                    }
                    y = i - 1;
                    x = j - 1;
                    if (checkIsInField(x, y, true)) {
                        cell.addSecondNeighbour(field.get(y).get(x));
                    }
                    y = i - 1;
                    x = j + 2;
                    if (checkIsInField(x, y, true)) {
                        cell.addSecondNeighbour(field.get(y).get(x));
                    }
                    y = i + 1;
                    x = j - 1;
                    if (checkIsInField(x, y, true)) {
                        cell.addSecondNeighbour(field.get(y).get(x));
                    }
                    y = i + 1;
                    x = j + 2;
                    if (checkIsInField(x, y, true)) {
                        cell.addSecondNeighbour(field.get(y).get(x));
                    }
                } else {
                    y = i - 1; //first neighbours
                    x = j - 1;
                    if (checkIsInField(x, y, false)) {
                        cell.addFirstNeighbour(field.get(y).get(x));
                    }
                    y = i - 1;
                    x = j;
                    if (checkIsInField(x, y, false)) {
                        cell.addFirstNeighbour(field.get(y).get(x));
                    }
                    y = i;
                    x = j - 1;
                    if (checkIsInField(x, y, false)) {
                        cell.addFirstNeighbour(field.get(y).get(x));
                    }
                    y = i;
                    x = j + 1;
                    if (checkIsInField(x, y, false)) {
                        cell.addFirstNeighbour(field.get(y).get(x));
                    }
                    y = i + 1;
                    x = j - 1;
                    if (checkIsInField(x, y, false)) {
                        cell.addFirstNeighbour(field.get(y).get(x));
                    }
                    y = i + 1;
                    x = j;
                    if (checkIsInField(x, y, false)) {
                        cell.addFirstNeighbour(field.get(y).get(x));
                    }

                    y = i - 2; // second neighbours
                    x = j;
                    if (checkIsInField(x, y, false)) {
                        cell.addSecondNeighbour(field.get(y).get(x));
                    }
                    y = i + 2;
                    x = j;
                    if (checkIsInField(x, y, false)) {
                        cell.addSecondNeighbour(field.get(y).get(x));
                    }
                    y = i - 1;
                    x = j - 2;
                    if (checkIsInField(x, y, false)) {
                        cell.addSecondNeighbour(field.get(y).get(x));
                    }
                    y = i - 1;
                    x = j + 1;
                    if (checkIsInField(x, y, false)) {
                        cell.addSecondNeighbour(field.get(y).get(x));
                    }
                    y = i + 1;
                    x = j - 2;
                    if (checkIsInField(x, y, false)) {
                        cell.addSecondNeighbour(field.get(y).get(x));
                    }
                    y = i + 1;
                    x = j + 1;
                    if (checkIsInField(x, y, false)) {
                        cell.addSecondNeighbour(field.get(y).get(x));
                    }
                }
            }
        }
    }

    private boolean checkIsInField(int x, int y, boolean isOdd) {
        if (isOdd) {
            if (y % 2 == 1) {
                return x > -1 && y > -1 && x < width - 1 && y < height;
            } else {
                return x > -1 && y > -1 && x < width && y < height;
            }
        } else {
            if (y % 2 == 1) {
                return x > -1 && y > -1 && x < width - 1 && y < height;
            } else {
                return x > -1 && y > -1 && x < width && y < height;
            }
        }
    }

    public void makeFieldEmpty() {
        for (ArrayList<Cell> aField : field) {
            aField.clear();
        }
        field.clear();
    }

    public void clearField() {
        for (ArrayList<Cell> list : field) {
            for (Cell cell : list) {
                cell.setWasAlive(false);
                cell.setAliveNow(false);
            }
        }
    }

    public void setCellState(ArrayList<Point> alivePoints) {
        for (Point point : alivePoints) {
            setAlive(point.getX(), point.getY(), true);
        }
    }

    public ArrayList<ArrayList<Boolean>> getStateMatrix() {
        ArrayList<ArrayList<Boolean>> m = new ArrayList<>();
        for (int i = 0; i < field.size(); ++i) {
            m.add(new ArrayList<>());
            for (int j = 0; j < field.get(i).size(); ++j) {
                m.get(i).add(field.get(i).get(j).isAliveNow());
            }
        }
        return m;
    }

    public ArrayList<ArrayList<Double>> getImpactMatrix() {
        ArrayList<ArrayList<Double>> lists = new ArrayList<>();
        for (int i = 0; i < height; ++i) {
            ArrayList<Double> list = new ArrayList<>();
            lists.add(list);
            for (int j = 0; j < field.get(i).size(); ++j) {
                list.add(field.get(i).get(j).getImpact());
            }
        }
        return lists;
    }
}
