package ru.nsu.fit.g16202.korshunov.game.model;

import ru.nsu.fit.g16202.korshunov.data.OptionsStorage;

import java.util.ArrayList;

public class Cell {
    private boolean isAliveNow = false;
    private boolean wasAlive = false;
    private ArrayList<Cell> firstNeighbours = new ArrayList<>();
    private ArrayList<Cell> secondNeighbours = new ArrayList<>();

    public boolean isAliveNow() {
        return isAliveNow;
    }

    public void setAliveNow(boolean aliveNow) {
        isAliveNow = aliveNow;
    }

    public boolean wasAlive() {
        return wasAlive;
    }

    public void setWasAlive(boolean wasAlive) {
        this.wasAlive = wasAlive;
    }

    public void addFirstNeighbour(Cell cell) {
        firstNeighbours.add(cell);
    }

    public double getImpact() {
        int firstAlive = 0;
        int secondAlive = 0;
        for (Cell cell : firstNeighbours) {
            if (cell.wasAlive) {
                firstAlive++;
            }
        }
        for (Cell cell : secondNeighbours) {
            if (cell.wasAlive) {
                secondAlive++;
            }
        }
        return firstAlive * OptionsStorage.getInstance().getFirstImpact() + secondAlive * OptionsStorage.getInstance().getSecondImpact();
    }

    public void addSecondNeighbour(Cell cell) {
        secondNeighbours.add(cell);
    }
}
