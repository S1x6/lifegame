package ru.nsu.fit.g16202.korshunov.mvp.model;

import java.awt.*;
import java.awt.image.BufferedImage;

public class AffineFilter {

    void applyRotationFilter(BufferedImage srcImage, BufferedImage destImage, double angle) {
        int oldX, oldY;
        double cos = Math.cos(Math.toRadians(angle));
        double sin = Math.sin(Math.toRadians(angle));
        for (int j = 0; j < destImage.getHeight(); ++j) { //для каждого пикселя
            for (int i = 0; i < destImage.getWidth(); ++i) {
                oldX = (int) (srcImage.getWidth() / 2. + (i - srcImage.getWidth() / 2.) * cos + (j - srcImage.getHeight() / 2.) * sin);
                oldY = (int) (srcImage.getHeight() / 2. + (j - srcImage.getHeight() / 2.) * cos - (i - srcImage.getWidth() / 2.) * sin);
                if (oldX < 0 || oldX >= destImage.getWidth() || oldY < 0 || oldY >= destImage.getHeight()) {
                    destImage.setRGB(i, j, Color.WHITE.getRGB());
                } else {
                    destImage.setRGB(i, j, srcImage.getRGB(oldX, oldY));
                }

            }

        }

    }
}
