package ru.nsu.fit.g16202.korshunov.mvp.model;

import java.awt.image.BufferedImage;

import static ru.nsu.fit.g16202.korshunov.mvp.model.ModelUtils.leaveBlueChannel;
import static ru.nsu.fit.g16202.korshunov.mvp.model.ModelUtils.leaveGreenChannel;
import static ru.nsu.fit.g16202.korshunov.mvp.model.ModelUtils.leaveRedChannel;

public class MatrixFilter {

    void applyColoredMatrixFilter(BufferedImage srcImage, BufferedImage destImage, int[][] matrix) {
        BufferedImage redImage = leaveRedChannel(ModelUtils.copyImage(srcImage));
        BufferedImage greenImage = leaveGreenChannel(ModelUtils.copyImage(srcImage));
        BufferedImage blueImage = leaveBlueChannel(ModelUtils.copyImage(srcImage));

        for (int j = 0; j < srcImage.getHeight(); j++) {
            for (int i = 0; i < srcImage.getWidth(); i++) {
                int r = ModelUtils.applyMatrix(redImage, i, j, matrix, Model.ColorComponent.RED);
                int g = ModelUtils.applyMatrix(greenImage, i, j, matrix, Model.ColorComponent.GREEN);
                int b = ModelUtils.applyMatrix(blueImage, i, j, matrix, Model.ColorComponent.BLUE);
                r = Math.max(Math.min(r, 255),0);
                g = Math.max(Math.min(g, 255),0);
                b = Math.max(Math.min(b, 255),0);
                destImage.setRGB(i, j, r << 16 | g << 8 | b);
            }
        }
    }
}
