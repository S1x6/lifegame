package ru.nsu.fit.g16202.korshunov.mvp.model;

import java.awt.image.BufferedImage;

import static ru.nsu.fit.g16202.korshunov.mvp.model.ModelUtils.*;

public class EmbossFilter {

    void applyEmbossFilter(BufferedImage srcImage, BufferedImage destImage, int[][] matrix) {
        BufferedImage redImage = leaveRedChannel(ModelUtils.copyImage(srcImage));
        BufferedImage greenImage = leaveGreenChannel(ModelUtils.copyImage(srcImage));
        BufferedImage blueImage = leaveBlueChannel(ModelUtils.copyImage(srcImage));

        for (int j = 0; j < srcImage.getHeight(); j++) {
            for (int i = 0; i < srcImage.getWidth(); i++) {
                int r = applyEmbossMatrix(redImage, i, j, matrix, Model.ColorComponent.RED);
                int g = applyEmbossMatrix(greenImage, i, j, matrix, Model.ColorComponent.GREEN);
                int b = applyEmbossMatrix(blueImage, i, j, matrix, Model.ColorComponent.BLUE);
                r = Math.max(Math.min(r, 255),0);
                g = Math.max(Math.min(g, 255),0);
                b = Math.max(Math.min(b, 255),0);
                destImage.setRGB(i, j, r << 16 | g << 8 | b);
            }
        }
    }

    private int applyEmbossMatrix(BufferedImage srcImage, int i, int j, int[][] matrix, Model.ColorComponent component) {
            int lu = i - 1 < 0 || j - 1 < 0 ? 0 : getComponent(srcImage.getRGB(i - 1, j - 1), component);
            int u = j - 1 < 0 ? 0 : getComponent(srcImage.getRGB(i, j - 1), component);
            int ru = i + 1 >= srcImage.getWidth() || j - 1 < 0 ? 0 : getComponent(srcImage.getRGB(i + 1, j - 1), component);
            int r = i + 1 >= srcImage.getWidth() ? 0 : getComponent(srcImage.getRGB(i + 1, j), component);
            int rb = i + 1 >= srcImage.getWidth() || j + 1 >= srcImage.getHeight() ? 0 : getComponent(srcImage.getRGB(i + 1, j + 1), component);
            int b = j + 1 >= srcImage.getHeight() ? 0 : getComponent(srcImage.getRGB(i, j + 1), component);
            int lb = j + 1 >= srcImage.getHeight() || i - 1 < 0 ? 0 : getComponent(srcImage.getRGB(i - 1, j + 1), component);
            int l = i - 1 < 0 ? 0 : getComponent(srcImage.getRGB(i - 1, j), component);
            return (lu * matrix[0][0] + u * matrix[0][1] + ru * matrix[0][2] + r * matrix[1][2]
                    + rb * matrix[2][2] + b * matrix[2][1] + lb * matrix[2][0] + l * matrix[1][0]
                    + getComponent(srcImage.getRGB(i, j), component) * matrix[1][1]) + 128;
    }

}
