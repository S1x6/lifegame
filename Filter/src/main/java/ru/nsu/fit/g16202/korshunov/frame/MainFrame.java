package ru.nsu.fit.g16202.korshunov.frame;

import ru.nsu.fit.g16202.korshunov.mvp.model.Model;
import ru.nsu.fit.g16202.korshunov.mvp.presenter.Presenter;
import ru.nsu.fit.g16202.korshunov.mvp.view.View;
import ru.nsu.fit.g16202.korshunov.ui.StatusBar;
import ru.nsu.fit.g16202.korshunov.ui.dialog.DitheringDialog;
import ru.nsu.fit.g16202.korshunov.ui.dialog.GammaDialog;
import ru.nsu.fit.g16202.korshunov.ui.dialog.InfoDialog;
import ru.nsu.fit.g16202.korshunov.ui.dialog.OneValueDialog;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

public class MainFrame extends JFrame {

    private StatusBar statusBar;

    private Presenter presenter;

    private JToolBar jToolBar;
    private JCheckBoxMenuItem selectMenu;
    private JCheckBoxMenuItem emissionMenu;
    private JCheckBoxMenuItem absorptionMenu;
    private JToggleButton selectToolbar;
    private JToggleButton emissionToolbar;
    private JToggleButton absorptionToolbar;

    public static void main(String[] args) {
        new MainFrame();
    }

    private MainFrame() {
        super();
        Dimension minDim = new Dimension(1100, 700);
        setSize(minDim);
        setMinimumSize(minDim);
        setTitle("Filter NSU_g16202_Korshunov");
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
        presenter = new Presenter(new Model(), new View());
        this.setLocation(dim.width / 2 - this.getSize().width / 2, dim.height / 2 - this.getSize().height / 2);
        this.setLayout(new BorderLayout());
        initMenu();
        initStatusBar();
        initToolbar();
        initCenterPanel();
        pack();
        setVisible(true);
    }

    private void initCenterPanel() {
        this.add(new JScrollPane(presenter.getView()), BorderLayout.CENTER);
    }

    private void initToolbar() {
        jToolBar = new JToolBar();
        JButton newDocumentButton = new DefaultToolBarButton("file.png", "Новый документ",
                statusBar, e -> presenter.newFile());
        jToolBar.add(newDocumentButton);

        selectToolbar = new JToggleButton(new ImageIcon(ClassLoader.getSystemResource("select.png")));
        selectToolbar.setToolTipText("Выделение области");
        selectToolbar.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
                if (selectToolbar.isSelected()) {
                    presenter.enableSelection(true);
                    selectMenu.setSelected(true);
                } else {
                    presenter.enableSelection(false);
                    selectMenu.setSelected(false);
                }
            }

            @Override
            public void mousePressed(MouseEvent e) {

            }

            @Override
            public void mouseReleased(MouseEvent e) {

            }

            @Override
            public void mouseEntered(MouseEvent e) {
                statusBar.setStatus(selectToolbar.getToolTipText());
            }

            @Override
            public void mouseExited(MouseEvent e) {
                statusBar.setStatus("");
            }
        });
        JButton openButton = new DefaultToolBarButton("open.png", "Открыть файл",
                statusBar, e -> presenter.openFile());
        jToolBar.add(openButton);
        JButton save = new DefaultToolBarButton("save.png", "Сохранить файл",
                statusBar, e -> presenter.saveFile());
        jToolBar.add(save);
        jToolBar.addSeparator();
        jToolBar.add(selectToolbar);
        jToolBar.addSeparator();
        JButton grayScaleButton = new DefaultToolBarButton("black_white.png", "Черно-белое изображение",
                statusBar, e -> presenter.applyGrayScaleFilter());
        jToolBar.add(grayScaleButton);
        JButton negativeButton = new DefaultToolBarButton("negative.png", "Негатив",
                statusBar, e -> presenter.applyNegativeFilter());
        jToolBar.add(negativeButton);
        jToolBar.addSeparator();
        JButton bToCButton = new DefaultToolBarButton("BC.png", "Копировать B в C",
                statusBar, e -> presenter.copyBtoC());
        jToolBar.add(bToCButton);
        JButton cToBButton = new DefaultToolBarButton("CB.png", "Копировать C в B",
                statusBar, e -> presenter.copyCtoB());
        jToolBar.add(cToBButton);
        jToolBar.addSeparator();
        JButton floydDitheringButton = new DefaultToolBarButton("floydDither.png", "Дизеринг Флойда-Стейнберга",
                statusBar, e -> {
            DitheringDialog dialog = new DitheringDialog(this, true, "Флойд-Стейнберг");
            dialog.setVisible(true);
            if (!dialog.wasCanceled()) {
                presenter.applyFloydDitheringFilter(dialog.getNr(), dialog.getNg(), dialog.getNb());
            }
        });
        jToolBar.add(floydDitheringButton);
        JButton orderedDitheringButton = new DefaultToolBarButton("ordered.png", "Упорядоченный дизеринг", statusBar,
                e -> {
                    DitheringDialog dialog = new DitheringDialog(this, true, "Упорядоченный");
                    dialog.setVisible(true);
                    if (!dialog.wasCanceled()) {
                        presenter.applyOrderedDithering(dialog.getNr(), dialog.getNg(), dialog.getNb());
                    }
                });
        jToolBar.add(orderedDitheringButton);
        jToolBar.addSeparator();
        JButton blurButton = new DefaultToolBarButton("blur.png", "Размытие", statusBar,
                e -> {
                    int[][] table = new int[3][3];
                    table[0][0] = 1;
                    table[0][1] = 1;
                    table[0][2] = 1;
                    table[1][0] = 1;
                    table[1][1] = 1;
                    table[1][2] = 1;
                    table[2][0] = 1;
                    table[2][1] = 1;
                    table[2][2] = 1;
                    presenter.applyBlurFilter(table);
                });
        jToolBar.add(blurButton);
        JButton sharpnessButton = new DefaultToolBarButton("sharp.png", "Резкость", statusBar,
                e -> {
                    int[][] table = new int[3][3];
                    table[0][0] = 0;
                    table[0][1] = -1;
                    table[0][2] = 0;
                    table[1][0] = -1;
                    table[1][1] = 5;
                    table[1][2] = -1;
                    table[2][0] = 0;
                    table[2][1] = -1;
                    table[2][2] = 0;
                    presenter.applySharpnessFilter(table);
                });
        jToolBar.add(sharpnessButton);
        JButton embossButton = new DefaultToolBarButton("emboss.png", "Тиснение", statusBar,
                e -> {
                    int[][] table = new int[3][3];
                    table[0][0] = 0;
                    table[0][1] = 1;
                    table[0][2] = 0;
                    table[1][0] = -1;
                    table[1][1] = 0;
                    table[1][2] = 1;
                    table[2][0] = 0;
                    table[2][1] = -1;
                    table[2][2] = 0;
                    presenter.applyEmbossFilter(table);
                });
        jToolBar.add(embossButton);
        jToolBar.addSeparator();
        JButton robertButton = new DefaultToolBarButton("border1.png", "Фильтр Робертса", statusBar,
                e -> {
                    OneValueDialog dialog = new OneValueDialog(this, 1, 4000, 800, "Пороговое значение");
                    dialog.setVisible(true);
                    if (!dialog.isCanceled())
                        presenter.applyRobertsFilter((int) dialog.getValue());
                });
        jToolBar.add(robertButton);
        JButton sobelButton = new DefaultToolBarButton("sobel.png", "Фильтр Собеля", statusBar,
                e -> {
                    OneValueDialog dialog = new OneValueDialog(this, 1, 10200, 3000, "Пороговое значение");
                    dialog.setVisible(true);
                    if (!dialog.isCanceled())
                        presenter.applySobelFilter((int) dialog.getValue());
                });
        jToolBar.add(sobelButton);
        jToolBar.addSeparator();
        JButton watercolorButton = new DefaultToolBarButton("water.png", "Акварель", statusBar,
                e -> presenter.applyWaterColorFilter());
        jToolBar.add(watercolorButton);
        JButton zoomButton = new DefaultToolBarButton("zoom.png", "Увеличить изображение", statusBar,
                e -> presenter.applyZoomFilter());
        jToolBar.add(zoomButton);
        JButton gammaButton = new DefaultToolBarButton("gamma.png", "Коррекция гаммы", statusBar,
                e -> {
                    GammaDialog dialog = new GammaDialog(this, 0, 10, 1, "Значение гаммы");
                    dialog.setVisible(true);
                    if (!dialog.isCanceled())
                        presenter.applyGammaFilter(dialog.getValue());
                });
        jToolBar.add(gammaButton);
        JButton rotate = new DefaultToolBarButton("rotate.png", "Поворот", statusBar,
                e -> {
                    OneValueDialog dialog = new OneValueDialog(this, -1800, 1800, 0, "Угол поворота");
                    dialog.setVisible(true);
                    if (!dialog.isCanceled())
                        presenter.applyRotatironFilter(dialog.getValue());
                });
        jToolBar.add(rotate);
        jToolBar.addSeparator();
        JButton config = new DefaultToolBarButton("config.png", "Открыть конфигурацию", statusBar,
                e -> presenter.loadVolumeConfig());
        jToolBar.add(config);

        emissionToolbar = new JToggleButton(new ImageIcon(ClassLoader.getSystemResource("emission.png")));
        emissionToolbar.addActionListener(e -> {
            presenter.setEmission(emissionToolbar.isSelected());
            emissionMenu.setSelected(emissionToolbar.isSelected());
        });
        emissionToolbar.setToolTipText("Эмиссия");
        emissionToolbar.addMouseListener(
                new MouseListener() {
                    @Override
                    public void mouseClicked(MouseEvent e) {

                    }

                    @Override
                    public void mousePressed(MouseEvent e) {

                    }

                    @Override
                    public void mouseReleased(MouseEvent e) {

                    }

                    @Override
                    public void mouseEntered(MouseEvent e) {
                        statusBar.setStatus(emissionToolbar.getToolTipText());
                    }

                    @Override
                    public void mouseExited(MouseEvent e) {
                        statusBar.setStatus("");
                    }
                }
        );
        jToolBar.add(emissionToolbar);


        absorptionToolbar = new JToggleButton(new ImageIcon(ClassLoader.getSystemResource("absorption.png")));
        absorptionToolbar.addActionListener(e -> {
            presenter.setAbsorption(absorptionToolbar.isSelected());
            absorptionMenu.setSelected(absorptionToolbar.isSelected());
        });
        absorptionToolbar.setToolTipText("Абсорбция");
        absorptionToolbar.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {

            }

            @Override
            public void mousePressed(MouseEvent e) {

            }

            @Override
            public void mouseReleased(MouseEvent e) {

            }

            @Override
            public void mouseEntered(MouseEvent e) {
                statusBar.setStatus(absorptionToolbar.getToolTipText());
            }

            @Override
            public void mouseExited(MouseEvent e) {
                statusBar.setStatus("");
            }
        });
        jToolBar.add(absorptionToolbar);

        JButton run = new DefaultToolBarButton("run.png", "Запустить визуализацию", statusBar,
                e -> presenter.applyVolumeRender());
        jToolBar.add(run);
        jToolBar.addSeparator();
        JButton info = new DefaultToolBarButton("info.png", "О программе", statusBar,
                e -> new InfoDialog(this).setVisible(true));
        jToolBar.add(info);
        this.add(jToolBar, BorderLayout.NORTH);
    }

    private void initStatusBar() {
        statusBar = new StatusBar(this);
        this.add(statusBar, BorderLayout.SOUTH);
    }

    private void initMenu() {
        JMenuBar menuBar = new JMenuBar();
        JMenu fileJMenu = new JMenu("Файл");
        JMenu viewJMenu = new JMenu("Окно");
        JMenu filtersJMenu = new JMenu("Фильтры");
        JMenu vvJMenu = new JMenu("ОП");
        JMenu infoJMenu = new JMenu("Справка");

        JMenuItem newJMenuButton = new JMenuItem("Новый документ");
        newJMenuButton.addActionListener(e -> presenter.newFile());
        fileJMenu.add(newJMenuButton);
        JMenuItem openJMenuButton = new JMenuItem("Открыть изображение");
        openJMenuButton.addActionListener(e -> presenter.openFile());
        fileJMenu.add(openJMenuButton);
        JMenuItem save = new JMenuItem("Сохранить изображение");
        save.addActionListener(e -> presenter.saveFile());
        fileJMenu.add(save);
        JMenuItem grayMenu = new JMenuItem("Черно-белый");
        grayMenu.addActionListener(e -> presenter.applyGrayScaleFilter());
        filtersJMenu.add(grayMenu);
        JMenuItem negativeMenu = new JMenuItem("Негатив");
        negativeMenu.addActionListener(e -> presenter.applyNegativeFilter());
        filtersJMenu.add(negativeMenu);
        JMenuItem floyd = new JMenuItem("Дизеринг Флойда");
        floyd.addActionListener(e -> {
            DitheringDialog dialog = new DitheringDialog(this, true, "Флойд-Стейнберг");
            dialog.setVisible(true);
            if (!dialog.wasCanceled()) {
                presenter.applyFloydDitheringFilter(dialog.getNr(), dialog.getNg(), dialog.getNb());
            }
        });
        filtersJMenu.add(floyd);
        JMenuItem ordered = new JMenuItem("Упорядоченный дизеринг");
        ordered.addActionListener(e -> {
            DitheringDialog dialog = new DitheringDialog(this, true, "Упорядоченный");
            dialog.setVisible(true);
            if (!dialog.wasCanceled()) {
                presenter.applyOrderedDithering(dialog.getNr(), dialog.getNg(), dialog.getNb());
            }
        });
        filtersJMenu.add(ordered);
        JMenuItem colors = new JMenuItem("Акварель");
        colors.addActionListener(e -> presenter.applyWaterColorFilter());
        filtersJMenu.add(colors);
        JMenuItem zoom = new JMenuItem("Увеличение");
        zoom.addActionListener(e -> presenter.applyZoomFilter());
        filtersJMenu.add(zoom);
        JMenuItem blur = new JMenuItem("Размытие");
        blur.addActionListener(e -> {
            int[][] table = new int[3][3];
            table[0][0] = 1;
            table[0][1] = 1;
            table[0][2] = 1;
            table[1][0] = 1;
            table[1][1] = 1;
            table[1][2] = 1;
            table[2][0] = 1;
            table[2][1] = 1;
            table[2][2] = 1;
            presenter.applyBlurFilter(table);
        });
        filtersJMenu.add(blur);
        JMenuItem sharp = new JMenuItem("Резкость");
        sharp.addActionListener(e -> {
            int[][] table = new int[3][3];
            table[0][0] = 0;
            table[0][1] = -1;
            table[0][2] = 0;
            table[1][0] = -1;
            table[1][1] = 5;
            table[1][2] = -1;
            table[2][0] = 0;
            table[2][1] = -1;
            table[2][2] = 0;
            presenter.applySharpnessFilter(table);
        });
        filtersJMenu.add(sharp);
        JMenuItem emboss = new JMenuItem("Тиснение");
        emboss.addActionListener(e -> {
            int[][] table = new int[3][3];
            table[0][0] = 0;
            table[0][1] = 1;
            table[0][2] = 0;
            table[1][0] = -1;
            table[1][1] = 0;
            table[1][2] = 1;
            table[2][0] = 0;
            table[2][1] = -1;
            table[2][2] = 0;
            presenter.applyEmbossFilter(table);
        });
        filtersJMenu.add(emboss);
        JMenuItem roberts = new JMenuItem("Фильтр Робертса");
        roberts.addActionListener(e -> {
            OneValueDialog dialog = new OneValueDialog(this, 10, 150, 50, "Пороговое значение");
            dialog.setVisible(true);
            if (!dialog.isCanceled())
                presenter.applyRobertsFilter((int) dialog.getValue());
        });
        filtersJMenu.add(roberts);
        JMenuItem sobel = new JMenuItem("Фильтр Собеля");
        sobel.addActionListener(e -> {
            OneValueDialog dialog = new OneValueDialog(this, 10, 800, 300, "Пороговое значение");
            dialog.setVisible(true);
            if (!dialog.isCanceled())
                presenter.applySobelFilter((int) dialog.getValue());
        });
        filtersJMenu.add(sobel);
        JMenuItem gamma = new JMenuItem("Гамма коррекция");
        gamma.addActionListener(e -> {
            GammaDialog dialog = new GammaDialog(this, 0, 10, 1, "Значение гаммы");
            dialog.setVisible(true);
            if (!dialog.isCanceled())
                presenter.applyGammaFilter(dialog.getValue());
        });
        filtersJMenu.add(gamma);

        JMenuItem rotate = new JMenuItem("Поворот");
        rotate.addActionListener(e -> {

            OneValueDialog dialog = new OneValueDialog(this, -1800, 1800, 0, "Угол поворота");
            dialog.setVisible(true);
            if (!dialog.isCanceled())
                presenter.applyRotatironFilter(dialog.getValue());
        });
        filtersJMenu.add(rotate);

        selectMenu = new JCheckBoxMenuItem("Выбор области");
        selectMenu.addActionListener(e -> {
            if (selectMenu.isSelected()) {
                presenter.enableSelection(true);
                selectToolbar.setSelected(true);
            } else {
                presenter.enableSelection(false);
                selectToolbar.setSelected(false);
            }
        });
        viewJMenu.add(selectMenu);
        JMenuItem bc = new JMenuItem("B -> C");
        bc.addActionListener(e -> presenter.copyBtoC());
        viewJMenu.add(bc);
        JMenuItem cb = new JMenuItem("C -> B");
        cb.addActionListener(e -> presenter.copyCtoB());
        viewJMenu.add(cb);

        JMenuItem info = new JMenuItem("О программе");
        info.addActionListener(e -> new InfoDialog(this).setVisible(true));
        infoJMenu.add(info);

        JMenuItem config = new JMenuItem("Открыть конфигурацию");
        config.addActionListener(e -> presenter.loadVolumeConfig());
        vvJMenu.add(config);

        emissionMenu = new JCheckBoxMenuItem("Эмиссия");
        emissionMenu.addActionListener(e -> {
            presenter.setEmission(emissionMenu.isSelected());
            emissionToolbar.setSelected(emissionMenu.isSelected());
        });
        vvJMenu.add(emissionMenu);

        absorptionMenu = new JCheckBoxMenuItem("Абсорбция");
        absorptionMenu.addActionListener(e -> {
            presenter.setAbsorption(absorptionMenu.isSelected());
            absorptionToolbar.setSelected(absorptionMenu.isSelected());
        });
        vvJMenu.add(absorptionMenu);

        JMenuItem render = new JMenuItem("Рендер");
        render.addActionListener(e -> presenter.applyVolumeRender());
        vvJMenu.add(render);

        menuBar.add(fileJMenu);
        menuBar.add(viewJMenu);
        menuBar.add(filtersJMenu);
        menuBar.add(vvJMenu);
        menuBar.add(infoJMenu);
        this.setJMenuBar(menuBar);
    }

}
