package ru.nsu.fit.g16202.korshunov.ui.dialog;

import javax.swing.*;
import java.awt.*;

public class OneValueDialog extends Dialog {

    private boolean canceled = false;

    private JSlider slider;

    public OneValueDialog(JFrame owner, int min, int max, int init, String title) {
        super(owner, true);
        JPanel main = new JPanel();
        JPanel fields = new JPanel();
        JPanel buttons = new JPanel();
        setTitle(title);
        setLocationRelativeTo(owner);
        TextField tf = new TextField();
        tf.setText(String.valueOf(init));
        JButton okButton = new JButton("OK");
        okButton.addActionListener(e -> {
            double a;
            try {
                a = Double.valueOf(tf.getText());
            } catch (NumberFormatException ex) {
                showError();
                return;
            }
            if (a < min || a > max) {
                showError();
                return;
            }
            dispose();
        });
        JButton cancelButton = new JButton("Отмена");
        cancelButton.addActionListener(e -> {
            canceled = true;
            dispose();
        });
        slider = new JSlider();
        slider.setMinimum(min);
        slider.setMaximum(max);
        slider.addChangeListener(e -> tf.setText(String.valueOf(slider.getValue()/10.)));
        slider.setValue(init);
        tf.addTextListener(e -> {
            double a;
            try {
                a = Double.valueOf(tf.getText());

            } catch (NumberFormatException ex) {
                return;
            }
            if (a < min || a > max) {
                return;
            }
            slider.setValue((int)(a*10));
        });
        main.setLayout(new FlowLayout());
        fields.setLayout(new FlowLayout());
        buttons.setLayout(new FlowLayout());
        fields.add(tf);
        fields.add(slider);
        buttons.add(okButton);
        buttons.add(cancelButton);
        main.add(fields);
        main.add(buttons);
        add(main);
        pack();
    }

    private void showError() {
        JOptionPane.showMessageDialog(this, "Введено некорректное значение");
    }

    public double getValue() {
        return slider.getValue()/10.;
    }

    public boolean isCanceled() {
        return canceled;
    }
}
