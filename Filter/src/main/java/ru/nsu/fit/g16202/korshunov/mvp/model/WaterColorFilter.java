package ru.nsu.fit.g16202.korshunov.mvp.model;

import java.awt.image.BufferedImage;
import java.util.ArrayList;

public class WaterColorFilter {

    private Model model;

    WaterColorFilter(Model model) {
        this.model = model;
    }

    void applyWaterColorFilter(BufferedImage srcImage, BufferedImage destImage) {
        applyMedianFilter(srcImage, destImage, Model.ColorComponent.RED);
        applyMedianFilter(srcImage, destImage, Model.ColorComponent.GREEN);
        applyMedianFilter(srcImage, destImage, Model.ColorComponent.BLUE);
        int[][] table = new int[3][3];
        table[0][0] = 0;
        table[0][1] = -1;
        table[0][2] = 0;
        table[1][0] = -1;
        table[1][1] = 5;
        table[1][2] = -1;
        table[2][0] = 0;
        table[2][1] = -1;
        table[2][2] = 0;
        model.applySharpnessFilter(destImage, destImage, table);
    }

    private void applyMedianFilter(BufferedImage image, BufferedImage destImage, Model.ColorComponent component) {
        ArrayList<Integer> list = new ArrayList<>(25);
        for (int x = 0; x < image.getWidth(); x++) {
            for (int y = 0; y < image.getHeight(); y++) {
                list.clear();
                for (int j = -2; j < 3; j++) {
                    for (int i = -2; i < 3; i++) {
                        if ((x + i) < 0 || (x + i) >= image.getWidth() || (y + j) < 0 || (y + j) >= image.getHeight()) {
                            list.add(0);
                        } else {
                            list.add(ModelUtils.getComponent(image.getRGB(x + i, y + j), component));
                        }
                    }
                }
                list.sort((o1, o2) -> o2 - o1);
                ModelUtils.setComponent(destImage, x, y, list.get(13), component);
            }
        }

    }
}
