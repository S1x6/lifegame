package ru.nsu.fit.g16202.korshunov.mvp.model;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.awt.image.WritableRaster;

public class ModelUtils {

    static int getNearestColor(int color, int Nr, int Ng, int Nb) {
        int divR = 256 / Nr;
        int divG = 256 / Ng;
        int divB = 256 / Nb;
        return (getRedComponent(color) / divR * divR) << 16 |
                (getGreenComponent(color) / divG * divG) << 8 |
                (getBlueComponent(color) / divB * divB);
    }

    static int getNearestColorComponent(int colorComponent, int N) {
        double div = 255. / (N-1);
        return (int)(Math.round(colorComponent / div) * div);
    }

    static int getRedComponent(int color) {
        return color >> 16 & 0x0000ff;
    }

    static int getGreenComponent(int color) {
        return color >> 8 & 0x0000ff;
    }

    static int getBlueComponent(int color) {
        return color & 0x0000ff;
    }

    static BufferedImage copyImage(BufferedImage bi) {
        WritableRaster data = ((WritableRaster) bi.getData(new Rectangle(0, 0, bi.getWidth(), bi.getHeight()))).createWritableTranslatedChild(0, 0);
        return new BufferedImage(bi.getColorModel(), data, bi.isAlphaPremultiplied(), null);
    }

    static int applyMatrix(BufferedImage srcImage, int i, int j, int[][] matrix, Model.ColorComponent component) {
        int sum = matrix[0][0] + matrix[0][1] + matrix[0][2] + matrix[1][0] + matrix[1][1] + matrix[1][2] +
                matrix[2][0] + matrix[2][1] + matrix[2][2];
        int lu = i - 1 < 0 || j - 1 < 0 ? 0 : getComponent(srcImage.getRGB(i - 1, j - 1), component);
        int u = j - 1 < 0 ? 0 : getComponent(srcImage.getRGB(i, j - 1), component);
        int ru = i + 1 >= srcImage.getWidth() || j - 1 < 0 ? 0 : getComponent(srcImage.getRGB(i + 1, j - 1), component);
        int r = i + 1 >= srcImage.getWidth() ? 0 : getComponent(srcImage.getRGB(i + 1, j), component);
        int rb = i + 1 >= srcImage.getWidth() || j + 1 >= srcImage.getHeight() ? 0 : getComponent(srcImage.getRGB(i + 1, j + 1), component);
        int b = j + 1 >= srcImage.getHeight() ? 0 : getComponent(srcImage.getRGB(i, j + 1), component);
        int lb = j + 1 >= srcImage.getHeight() || i - 1 < 0 ? 0 : getComponent(srcImage.getRGB(i - 1, j + 1), component);
        int l = i - 1 < 0 ? 0 : getComponent(srcImage.getRGB(i - 1, j), component);
        int calc = (lu * matrix[2][2] + u * matrix[2][1] + ru * matrix[2][0] + r * matrix[1][0]
                + rb * matrix[0][0] + b * matrix[0][1] + lb * matrix[0][2] + l * matrix[1][2]
                + getComponent(srcImage.getRGB(i, j), component) * matrix[1][1]);
//        int calc = (lu * matrix[0][0] + u * matrix[0][1] + ru * matrix[0][2] + r * matrix[1][2]
//                + rb * matrix[2][2] + b * matrix[2][1] + lb * matrix[2][0] + l * matrix[1][0]
//                + getComponent(srcImage.getRGB(i, j), component) * matrix[1][1]);
        return sum == 0 ? calc : calc / sum;
    }

    public static int getComponent(int color, Model.ColorComponent component) {
        switch (component) {
            case RED:
                return getRedComponent(color);
            case GREEN:
                return getGreenComponent(color);
            case BLUE:
                return getBlueComponent(color);
            default:
                return -1;
        }
    }

    public static void setComponent(BufferedImage image, int x, int y, int color, Model.ColorComponent component) {
        int c = image.getRGB(x,y);
        switch (component) {
            case RED:
                c &= 0x00ffff;
                c |= color << 16;
                break;
            case GREEN:
                c &= 0xff00ff;
                c |= color << 8;
                break;
            case BLUE:
                c &= 0xffff00;
                c |= color;
                break;
        }
        image.setRGB(x,y,c);
    }

    static BufferedImage leaveRedChannel(BufferedImage image) {
        for (int j = 0; j < image.getHeight(); ++j) {
            for (int i = 0; i < image.getWidth(); ++i) {
                image.setRGB(i, j, ModelUtils.getRedComponent(image.getRGB(i, j)) << 16);
            }
        }
        return image;
    }

    static BufferedImage leaveGreenChannel(BufferedImage image) {
        for (int j = 0; j < image.getHeight(); ++j) {
            for (int i = 0; i < image.getWidth(); ++i) {
                image.setRGB(i, j, ModelUtils.getGreenComponent(image.getRGB(i, j)) << 8);
            }
        }
        return image;
    }

    static BufferedImage leaveBlueChannel(BufferedImage image) {
        for (int j = 0; j < image.getHeight(); ++j) {
            for (int i = 0; i < image.getWidth(); ++i) {
                image.setRGB(i, j, ModelUtils.getBlueComponent(image.getRGB(i, j)));
            }
        }
        return image;
    }

}
