package ru.nsu.fit.g16202.korshunov.mvp.model;

import java.awt.image.BufferedImage;

public class GammaFilter {

    void applyGammaFilter(BufferedImage srcImage, BufferedImage destImage, double parameter) {
        applyGammaFilterOnChannel(srcImage, destImage, parameter, Model.ColorComponent.RED);
        applyGammaFilterOnChannel(srcImage, destImage, parameter, Model.ColorComponent.GREEN);
        applyGammaFilterOnChannel(srcImage, destImage, parameter, Model.ColorComponent.BLUE);
    }

    private void applyGammaFilterOnChannel(BufferedImage srcImage, BufferedImage destImage,
                                           double parameter, Model.ColorComponent component) {
        for (int j = 0; j < srcImage.getHeight(); j++) {
            for (int i = 0; i < srcImage.getWidth(); i++) {
                int color = ModelUtils.getComponent(srcImage.getRGB(i, j), component);
                double norm = color / 255.;
                int newColor = (int) (255 * Math.pow(norm, parameter));
                ModelUtils.setComponent(destImage, i, j, newColor, component);
            }
        }
    }
}
