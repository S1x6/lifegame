package ru.nsu.fit.g16202.korshunov.mvp.presenter;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.filechooser.FileFilter;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

public class ImageReader {

    public BufferedImage getImageFromFile(Component parent) {
        final JFileChooser fc = new JFileChooser();
        FileFilter filter = new FileNameExtensionFilter("BMP/PNG/JPEG", "bmp", "png", "jpg");
        fc.setFileFilter(filter);
        File dataDir = new File(".\\FIT_16202_Korshunov_Life_Data");
        if (!Files.exists(dataDir.toPath())) {
            try {
                Files.createDirectory(dataDir.toPath());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        fc.setCurrentDirectory(dataDir);
        int ret = fc.showDialog(parent, "Открыть изображение");

        if (ret == JFileChooser.APPROVE_OPTION) {
            File file = fc.getSelectedFile();

            BufferedImage img = null;
            try {
                img = ImageIO.read(file);
            } catch (IOException e) {
            }
            return img;
        }
        return null;
    }
}
