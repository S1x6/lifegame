package ru.nsu.fit.g16202.korshunov.mvp.model;

import ru.nsu.fit.g16202.korshunov.mvp.model.volume.VolumeVisualization;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public class Model {

    public enum ColorComponent {
        RED, GREEN, BLUE
    }

    private ColorFilter colorFilter = new ColorFilter();
    private DitheringFilter ditheringFilter = new DitheringFilter(this);
    private MatrixFilter matrixFilter = new MatrixFilter();
    private EmbossFilter embossFilter = new EmbossFilter();
    private WaterColorFilter waterColorFilter = new WaterColorFilter(this);
    private BordersFilter bordersFilter = new BordersFilter(this);
    private ZoomFilter zoomFilter = new ZoomFilter();
    private GammaFilter gammaFilter = new GammaFilter();
    private AffineFilter affineFitler = new AffineFilter();
    private VolumeVisualization volumeVisualization= new VolumeVisualization();

    public void applyGrayScaleFilter(BufferedImage srcImage, BufferedImage destImg) {
        colorFilter.applyGrayScaleFilter(srcImage, destImg);
    }

    public void applyEqualGrayScaleFilter(BufferedImage srcImage, BufferedImage destImg) {
        colorFilter.applyEqualgrayScaleFilter(srcImage, destImg);
    }

    public void applyNegativeFilter(BufferedImage srcImage, BufferedImage destImage) {
        colorFilter.applyNegativeFilter(srcImage, destImage);
    }

    public void applyFloydDitheringFilter(BufferedImage srcImage, BufferedImage destImage, int Nr, int Ng, int Nb) {
        ditheringFilter.applyFloydDitheringFilter(srcImage, destImage, Nr, Ng, Nb);
    }

    public void applyBlurFilter(BufferedImage srcImage, BufferedImage destImage, int[][] matrix) {
        matrixFilter.applyColoredMatrixFilter(srcImage, destImage, matrix);
    }

    public void applySharpnessFilter(BufferedImage srcImage, BufferedImage destImage, int[][] matrix) {
        matrixFilter.applyColoredMatrixFilter(srcImage, destImage, matrix);
    }

    public void applyEmbossFilter(BufferedImage srcImage, BufferedImage destImage, int[][] matrix) {
        embossFilter.applyEmbossFilter(srcImage, destImage, matrix);
    }

    public void applyWaterColorFilter(BufferedImage srcImage, BufferedImage destImage) {
        waterColorFilter.applyWaterColorFilter(srcImage, destImage);
    }

    public void applyRobertsFilter(BufferedImage srcImage, BufferedImage destImage, int parameter) {
        bordersFilter.applyRobertsFilter(srcImage, destImage, parameter);
    }

    public void applyGammaFilter(BufferedImage srcImage, BufferedImage destImage, double parameter) {
        gammaFilter.applyGammaFilter(srcImage, destImage, parameter);
    }

    public void applySobelFilter(BufferedImage srcImage, BufferedImage destImage, int parameter) {
        bordersFilter.applySobelFilter(srcImage, destImage, parameter);
    }

    public void applyZoomFilter(BufferedImage srcImage, BufferedImage destImage) {
        zoomFilter.applyZoomFilter(srcImage, destImage);
    }

    public void applyOrderedDithering(BufferedImage srcImage, BufferedImage destImage, int Nr, int Ng, int Nb) {
        ditheringFilter.applyOrderedDithering(srcImage,destImage, Nr, Ng, Nb);
    }

    public void applyRotatironFilter(BufferedImage srcImage, BufferedImage destImage, double angle) {
        affineFitler.applyRotationFilter(srcImage, destImage, angle);
    }

    public void loadVolumeVisualizationConfig(File file) throws IOException {
        VolumeVisualization.loadConfig(file);
    }

    public void applyVolumeRender(BufferedImage srcImage, BufferedImage destImage, boolean emission, boolean absorption) {
        volumeVisualization.applyVisualization(srcImage, destImage, emission, absorption);
    }

}
