package ru.nsu.fit.g16202.korshunov.ui.dialog;

import javax.swing.*;
import java.awt.*;

public class InfoDialog extends Dialog {
    public InfoDialog(Frame owner) {
        super(owner, true);
        setLocationRelativeTo(owner);
        setLayout(new FlowLayout());
        setTitle("О программе");
        JLabel product = new JLabel("Product: FIT_16202_Filter_Task2");
        JLabel name = new JLabel("Author: Korshunov V.A.");
        JLabel version = new JLabel("Version: 0.9");
        JButton button = new JButton("Закрыть");
        button.addActionListener(e -> dispose());
        add(product);
        add(name);
        add(version);
        add(button);
        pack();
    }
}
