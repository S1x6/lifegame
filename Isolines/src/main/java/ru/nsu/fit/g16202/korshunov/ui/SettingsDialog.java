package ru.nsu.fit.g16202.korshunov.ui;

import javax.swing.*;
import java.awt.*;

public class SettingsDialog extends JDialog {

    public JTextField width = new JTextField(10);
    public JTextField height = new JTextField(10);
    public JTextField minX = new JTextField(10);
    public JTextField minY = new JTextField(10);
    public JTextField maxX = new JTextField(10);
    public JTextField maxY = new JTextField(10);

    private boolean isOk = false;

    public SettingsDialog(Frame owner) {
        super(owner, true);
        setTitle("Настройки");
        JPanel panel1 = new JPanel();
        panel1.setLayout(new FlowLayout());
        panel1.add(new JLabel("Ширина сетки:"));
        panel1.add(width);
        add(panel1);
        JPanel panel7 = new JPanel();
        panel7.setLayout(new FlowLayout());
        panel7.add(new JLabel("Высота сетки:"));
        panel7.add(height);
        add(panel7);
        JPanel panel2 = new JPanel();
        panel2.setLayout(new FlowLayout());
        panel2.add(new JLabel("MIN_X"));
        panel2.add(minX);
        add(panel2);
        JPanel panel3 = new JPanel();
        panel3.setLayout(new FlowLayout());
        panel3.add(new JLabel("MAX_X"));
        panel3.add(maxX);
        add(panel3);
        JPanel panel4 = new JPanel();
        panel4.setLayout(new FlowLayout());
        panel4.add(new JLabel("MIN_Y"));
        panel4.add(minY);
        add(panel4);
        JPanel panel5 = new JPanel();
        panel5.setLayout(new FlowLayout());
        panel5.add(new JLabel("MAX_Y"));
        panel5.add(maxY);
        add(panel5);
        JPanel panel6 = new JPanel();
        panel6.setLayout(new FlowLayout());
        JButton save = new JButton("Сохранить");
        save.addActionListener(e -> {
            try {
                if (
                        Double.valueOf(minX.getText()) < Double.valueOf(maxX.getText()) &&
                                Double.valueOf(minY.getText()) < Double.valueOf(maxY.getText()) &&
                                Integer.valueOf(width.getText()) > 1 &&
                                Integer.valueOf(height.getText()) > 1
                ) {
                    isOk = true;
                    dispose();
                } else {
                    JOptionPane.showMessageDialog(this, "Неверные параметры");
                }
            } catch (NumberFormatException ex) {
                JOptionPane.showMessageDialog(this, "Неверные параметры");
            }
        });
        panel6.add(save);
        JButton cancel = new JButton("Отмена");
        cancel.addActionListener(e -> dispose());
        panel6.add(cancel);
        add(panel6);
        setLayout(new GridLayout(7, 1));
        pack();
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        setLocation(-1 * getWidth() / 2, -1 * getHeight() / 2);
        setLocationRelativeTo(owner);
    }

    public boolean isOk() {
        return isOk;
    }
}
