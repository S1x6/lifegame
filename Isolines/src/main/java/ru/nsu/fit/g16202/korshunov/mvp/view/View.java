package ru.nsu.fit.g16202.korshunov.mvp.view;

import ru.nsu.fit.g16202.korshunov.mvp.model.Function;
import ru.nsu.fit.g16202.korshunov.mvp.model.Line;
import ru.nsu.fit.g16202.korshunov.mvp.presenter.Presenter;

import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

@SuppressWarnings("Duplicates")
public class View extends JPanel {

    public static final int HORIZONTAL_MARGIN = 10;
    public static final int VERTICAL_MARGIN = 10;
    public static final double LEGEND_RATIO_VERTICAL = 0.1;

    private Presenter presenter;
    private int gridWidth;
    private int gridHeight;
    private int userAreaWidth;
    private int userAreaHeight;
    private BufferedImage userIsolinesImage;
    private BufferedImage userPointsImage;
    private BufferedImage levelIsolinesImage;
    private BufferedImage interpolatedLegendImage;
    private BufferedImage legendImage;
    private BufferedImage levelsPointsImage;
    private boolean shouldDrawGrid;
    private BufferedImage coloredImage;
    private BufferedImage interpolatedColoredImage;
    private boolean shouldDrawLevelIsolines;
    private boolean shouldDrawInterpolated;
    private boolean shouldDrawPoints;
    private Color isolineColor;
    private List<Line> dynamicIsoline = new ArrayList<>();

    public View(Color color) {
        super();
        isolineColor = color;
        setLayout(null);
    }

    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        if (shouldDrawInterpolated) {
            if (interpolatedColoredImage != null) {
                g.drawImage(interpolatedColoredImage, HORIZONTAL_MARGIN, VERTICAL_MARGIN, userAreaWidth, (int) Math.round(userAreaHeight * (1 - LEGEND_RATIO_VERTICAL)), this);
            }
            if (interpolatedLegendImage != null && coloredImage != null) {
                g.drawImage(interpolatedLegendImage, HORIZONTAL_MARGIN, VERTICAL_MARGIN * 2 + (int) (userAreaHeight * (1 - LEGEND_RATIO_VERTICAL)), userAreaWidth, (int) (userAreaHeight * (LEGEND_RATIO_VERTICAL)), this);
            }
        } else {
            if (coloredImage != null) {
                g.drawImage(coloredImage, HORIZONTAL_MARGIN, VERTICAL_MARGIN, userAreaWidth, (int) (userAreaHeight * (1 - LEGEND_RATIO_VERTICAL)), this);
            }
            if (legendImage != null && coloredImage != null) {
                g.drawImage(legendImage, HORIZONTAL_MARGIN, VERTICAL_MARGIN * 2 + (int) (userAreaHeight * (1 - LEGEND_RATIO_VERTICAL)), userAreaWidth, (int) (userAreaHeight * (LEGEND_RATIO_VERTICAL)), this);
            }
        }
        if (userIsolinesImage != null) {
            g.drawImage(userIsolinesImage, HORIZONTAL_MARGIN, VERTICAL_MARGIN, userAreaWidth, userAreaHeight, this);
        }
        if (levelIsolinesImage != null && shouldDrawLevelIsolines) {
            g.drawImage(levelIsolinesImage, HORIZONTAL_MARGIN, VERTICAL_MARGIN, userAreaWidth, userAreaHeight, this);
        }
        if (shouldDrawPoints) {
            g.drawImage(userPointsImage, HORIZONTAL_MARGIN, VERTICAL_MARGIN, this);
            if (shouldDrawLevelIsolines) {
                g.drawImage(levelsPointsImage, HORIZONTAL_MARGIN, VERTICAL_MARGIN, this);
            }
        }
        if (!dynamicIsoline.isEmpty()) {
            for (Line line :
                    dynamicIsoline) {
                g.setColor(isolineColor);
                g.drawLine(line.getStartPoint().getX() + HORIZONTAL_MARGIN,
                        line.getStartPoint().getY() + VERTICAL_MARGIN,
                        line.getEndPoint().getX() + HORIZONTAL_MARGIN,
                        line.getEndPoint().getY() + VERTICAL_MARGIN);
                g.setColor(new Color(255, 150, 150));
                if (shouldDrawPoints) {
                    g.fillOval(HORIZONTAL_MARGIN + line.getStartPoint().getX() - 2, VERTICAL_MARGIN + line.getStartPoint().getY() - 2, 4, 4);
                    g.fillOval(HORIZONTAL_MARGIN + line.getEndPoint().getX() - 2, VERTICAL_MARGIN + line.getEndPoint().getY() - 2, 4, 4);
                }
            }
        }
        if (shouldDrawGrid) {
            drawGrid((Graphics2D) g);
        }
        dynamicIsoline.clear();
    }

    public void addLevelIsoline(List<Line> isoline, Color color) {
        if (levelIsolinesImage == null) {
            recalculateUserArea();
            levelIsolinesImage = new BufferedImage(userAreaWidth, userAreaHeight, BufferedImage.TYPE_INT_ARGB);
            Graphics2D g = levelIsolinesImage.createGraphics();
            g.setColor(new Color(0, 0, 0, 0));
            g.fillRect(0, 0, levelIsolinesImage.getWidth(), levelIsolinesImage.getHeight());
            g.dispose();
        }
        if (levelsPointsImage == null) {
            levelsPointsImage = new BufferedImage(userAreaWidth, userAreaHeight, BufferedImage.TYPE_INT_ARGB);
            Graphics2D g = levelsPointsImage.createGraphics();
            g.setColor(new Color(0, 0, 0, 0));
            g.fillRect(0, 0, levelsPointsImage.getWidth(), levelsPointsImage.getHeight());
            g.dispose();
        }
        drawIsoline(levelIsolinesImage, isoline, color);
        for (Line line :
                isoline) {
            Graphics2D g = levelsPointsImage.createGraphics();
            g.setColor(new Color(255, 150, 150));
            g.fillOval(line.getStartPoint().getX() - 2, line.getStartPoint().getY() - 2, 4, 4);
            g.fillOval(line.getEndPoint().getX() - 2, line.getEndPoint().getY() - 2, 4, 4);
        }
    }

    private void drawGrid(Graphics2D g) {
        //g.setStroke(new BasicStroke(1, BasicStroke.CAP_BUTT, BasicStroke.JOIN_BEVEL, 0, new float[]{4}, 0));
        g.setColor(Color.RED);
        double height = userAreaHeight * (1 - LEGEND_RATIO_VERTICAL);
        double stepX = (double) userAreaWidth / (gridWidth - 1);
        double stepY = height / (gridHeight - 1);
        double x = HORIZONTAL_MARGIN;
        double y = VERTICAL_MARGIN;
        g.drawLine((int) x, (int) y, (int) x + userAreaWidth, (int) y);
        g.drawLine((int) x, (int) y, (int) x, (int) (y + height));
        for (int i = 0; i < gridWidth - 1; ++i) {
            x += stepX;
            g.drawLine((int) x, (int) y, (int) x, (int) (y + height));
        }
        x = HORIZONTAL_MARGIN;
        for (int i = 0; i < gridHeight - 1; ++i) {
            y += stepY;
            g.drawLine((int) x, (int) y, (int) x + userAreaWidth, (int) y);
        }
        g.setStroke(new BasicStroke(1));
    }

    public void recalculateUserArea() {
        userAreaWidth = getWidth() - 2 * HORIZONTAL_MARGIN;
        userAreaHeight = getHeight() - 2 * VERTICAL_MARGIN;
    }

    public int getUserAreaWidth() {
        return userAreaWidth;
    }

    public int getUserAreaHeight() {
        return userAreaHeight;
    }

    public void setPresenter(Presenter presenter) {
        this.presenter = presenter;
    }

    private void drawIsoline(BufferedImage image, List<Line> isoline, Color color) {
        isolineColor = color;
        Graphics2D g = image.createGraphics();
        g.setColor(color);
        for (Line line : isoline) {
            g.drawLine(line.getStartPoint().getX(), line.getStartPoint().getY(),
                    line.getEndPoint().getX(), line.getEndPoint().getY());
        }
        g.dispose();
    }

    public void addIsoline(List<Line> isoline, Color color) {
        if (userIsolinesImage == null) {
            recalculateUserArea();
            userIsolinesImage = new BufferedImage(userAreaWidth, userAreaHeight, BufferedImage.TYPE_INT_ARGB);
            Graphics2D g = userIsolinesImage.createGraphics();
            g.setColor(new Color(0, 0, 0, 0));
            g.fillRect(0, 0, userIsolinesImage.getWidth(), userIsolinesImage.getHeight());
            g.dispose();
        }
        if (userPointsImage == null) {
            recalculateUserArea();
            userPointsImage = new BufferedImage(userAreaWidth, userAreaHeight, BufferedImage.TYPE_INT_ARGB);
            Graphics2D g = userPointsImage.createGraphics();
            g.setColor(new Color(0, 0, 0, 0));
            g.fillRect(0, 0, userPointsImage.getWidth(), userPointsImage.getHeight());
            g.dispose();
        }
        drawIsoline(userIsolinesImage, isoline, color);
        for (Line line :
                isoline) {
            Graphics2D g = userPointsImage.createGraphics();
            g.setColor(new Color(255, 150, 150));
            g.fillOval(line.getStartPoint().getX() - 2, line.getStartPoint().getY() - 2, 4, 4);
            g.fillOval(line.getEndPoint().getX() - 2, line.getEndPoint().getY() - 2, 4, 4);
        }
    }

    public void drawIsoline(List<Line> isoline, int color) {
        Graphics2D g = coloredImage.createGraphics();
        for (Line line :
                isoline) {
            g.setColor(new Color(color));
            g.drawLine(line.getStartPoint().getX(),
                    line.getStartPoint().getY(),
                    line.getEndPoint().getX(),
                    line.getEndPoint().getY());
        }
        g.dispose();
    }

    public void setGridSize(Dimension dimension) {
        gridWidth = dimension.width;
        gridHeight = dimension.height;
    }

    private BufferedImage drawColoredMap(int width, int height, Function colorFunction, Function valueFunction) {
        BufferedImage coloredImage = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
        for (int j = 0; j < coloredImage.getHeight(); ++j) {
            for (int i = 0; i < coloredImage.getWidth(); i++) {
                coloredImage.setRGB(i, j, ((int) colorFunction.getValue(
                        (Double) valueFunction.getValue(i, width, j, height)))
                );
            }
        }
        return coloredImage;
    }

    public void setDynamicIsoline(List<Line> isoline) {
        this.dynamicIsoline.clear();
        this.dynamicIsoline.addAll(isoline);
    }

    public void setShowGrid(boolean showGrid) {
        this.shouldDrawGrid = showGrid;
    }

    public void setShowLevels(boolean show) {
        this.shouldDrawLevelIsolines = show;
    }

    public void drawUserArea(Function colorFunction, Function valueFunction) {
        coloredImage = drawColoredMap(userAreaWidth, (int) ((1 - LEGEND_RATIO_VERTICAL) * userAreaHeight), colorFunction, valueFunction);
    }

    public void drawLegend(Function colorFunction, Function valueFunction, List<Double> levels) {
        BufferedImage image = drawColoredMap((int) (userAreaWidth * 0.9), (int) (userAreaHeight * LEGEND_RATIO_VERTICAL), colorFunction, valueFunction);
        legendImage = new BufferedImage(userAreaWidth, (int) (userAreaHeight * LEGEND_RATIO_VERTICAL), BufferedImage.TYPE_INT_ARGB);
        Graphics2D g = legendImage.createGraphics();
        g.setColor(new Color(0, 0, 0, 0));
        g.fillRect(0, 0, legendImage.getWidth(), legendImage.getHeight());
        g.setColor(Color.BLACK);
        int offsetX = (int) (userAreaWidth * 0.05);
        g.drawImage(image, offsetX, 0, image.getWidth(), (int) (image.getHeight() * 0.7), this);
        g.drawLine(offsetX, 0, offsetX, (int) (image.getHeight() * 0.7));
        double step = (userAreaWidth * 0.9) / (levels.size() + 1);
        for (int i = 1; i <= levels.size(); i++) {
            Double d = (double) Math.round(levels.get(i - 1) * 1000) / 1000;
            String string = d.toString();
            int width = g.getFontMetrics().stringWidth(string);
            g.drawString(string, (int) (step * i) - width / 2 + offsetX, (int) (image.getHeight() * 0.9));
            g.drawLine((int) (step * i) + offsetX, 0, (int) (step * i) + offsetX, (int) (image.getHeight() * 0.7));
        }
        g.drawLine(offsetX, 0, (int) (legendImage.getWidth() * 0.95), 0);
        g.drawLine((int) (legendImage.getWidth() * 0.95), 0, (int) (legendImage.getWidth() * 0.95), (int) (image.getHeight() * 0.7));
        g.drawLine(offsetX, (int) (image.getHeight() * 0.7), (int) (legendImage.getWidth() * 0.95), (int) (image.getHeight() * 0.7));
        g.dispose();
    }

    public void drawInterpolatedUserArea(Function colorFunction, Function valueFunction) {
        interpolatedColoredImage = drawColoredMap(userAreaWidth, (int) ((1 - LEGEND_RATIO_VERTICAL) * userAreaHeight), colorFunction, valueFunction);
    }

    public void drawInterpolatedLegend(Function colorFunction, Function valueFunction, List<Double> levels) {
        BufferedImage image = drawColoredMap((int) (userAreaWidth * 0.9), (int) (userAreaHeight * LEGEND_RATIO_VERTICAL), colorFunction, valueFunction);
        interpolatedLegendImage = new BufferedImage(userAreaWidth, (int) (userAreaHeight * LEGEND_RATIO_VERTICAL), BufferedImage.TYPE_INT_ARGB);
        Graphics2D g = interpolatedLegendImage.createGraphics();
        g.setColor(new Color(0, 0, 0, 0));
        g.fillRect(0, 0, interpolatedLegendImage.getWidth(), interpolatedLegendImage.getHeight());
        g.setColor(Color.BLACK);
        int offsetX = (int) (userAreaWidth * 0.05);
        g.drawImage(image, offsetX, 0, image.getWidth(), (int) (image.getHeight() * 0.7), this);
        g.drawLine(offsetX, 0, offsetX, (int) (image.getHeight() * 0.7));
        double step = (userAreaWidth * 0.9) / (levels.size() + 1);
        for (int i = 1; i <= levels.size(); i++) {
            Double d = (double) Math.round(levels.get(i - 1) * 1000) / 1000;
            String string = d.toString();
            int width = g.getFontMetrics().stringWidth(string);
            g.drawString(string, (int) (step * i) - width / 2 + offsetX, (int) (image.getHeight() * 0.9));
            g.drawLine((int) (step * i) + offsetX, 0, (int) (step * i) + offsetX, (int) (image.getHeight() * 0.7));
        }
        g.drawLine(offsetX, 0, (int) (interpolatedLegendImage.getWidth() * 0.95), 0);
        g.drawLine((int) (legendImage.getWidth() * 0.95), 0, (int) (legendImage.getWidth() * 0.95), (int) (image.getHeight() * 0.7));
        g.drawLine(offsetX, (int) (image.getHeight() * 0.7), (int) (interpolatedLegendImage.getWidth() * 0.95), (int) (image.getHeight() * 0.7));
        g.dispose();
    }

    public void setShouldDrawInterpolated(boolean shouldDrawInterpolated) {
        this.shouldDrawInterpolated = shouldDrawInterpolated;
    }

    public void setShouldDrawPoints(boolean s) {
        this.shouldDrawPoints = s;
    }

    public void deleteIsolines() {
        if (userIsolinesImage == null) {
            return;
        }
        Graphics2D g = userIsolinesImage.createGraphics();
        g.setComposite(AlphaComposite.Clear);
        g.setColor(new Color(0, 0, 0, 0));
        g.fillRect(0, 0, userIsolinesImage.getWidth(), userIsolinesImage.getHeight());
        g.dispose();
        g = userPointsImage.createGraphics();
        g.setComposite(AlphaComposite.Clear);
        g.setColor(new Color(0, 0, 0, 0));
        g.fillRect(0, 0, userPointsImage.getWidth(), userPointsImage.getHeight());
        g.dispose();
    }

    public void drawSpanColorMap(Function valueFunction, Function colorFunction) {
        coloredImage = new BufferedImage(userAreaWidth, (int) ((1 - LEGEND_RATIO_VERTICAL) * userAreaHeight), BufferedImage.TYPE_INT_ARGB);
        Graphics2D g = coloredImage.createGraphics();
        g.setColor(new Color(0, 0, 0, 0));
        g.fillRect(0,0, coloredImage.getWidth(), coloredImage.getHeight());
        g.dispose();
        double height = userAreaHeight * (1 - LEGEND_RATIO_VERTICAL);
        double stepY = (height-1) / (gridHeight - 1);
        for (double i = 0; (int)i < coloredImage.getHeight(); i += stepY) {
            int h = (int) i;
            for (int x = 0; x < coloredImage.getWidth(); x++) {
                Double interpolatedFunctionValue = (Double) valueFunction.getValue(
                        x, coloredImage.getWidth(), h, coloredImage.getHeight()
                );
                Color color = new Color((Integer) colorFunction.getValue(
                        interpolatedFunctionValue
                ));
                fillArea(coloredImage, levelIsolinesImage, x, h,
                        color.getRGB(), isolineColor.getRGB());
            }
        }
    }

    BufferedImage fillArea(BufferedImage image, BufferedImage isolineImage, int x, int y, int color, int isolineColor) {
        if (isolineImage.getRGB(x, y) == isolineColor ||
                image.getRGB(x, y) == color) {
            return null;
        }
        Stack<Span> spans = new Stack<>();
        int oc = image.getRGB(x, y);
        int xLeft = x;
        int xRight = x;
        while (xLeft > 0 && image.getRGB(xLeft, y) == oc && isolineImage.getRGB(xLeft, y) != isolineColor) {
            xLeft--;
        }
        while (xRight < image.getWidth() && image.getRGB(xRight, y) == oc && isolineImage.getRGB(xRight, y) != isolineColor) {
            xRight++;
        }
        spans.push(new Span(xLeft, xRight, y));
        while (spans.size() != 0) {
            Span currentSpan = spans.pop();
            currentSpan.fill(image, color);
            xLeft = currentSpan.getFx() + 1; // spans above
            int y1 = currentSpan.getY() - 1;
            if (y1 >= 0) {

                while (xLeft > 0 && image.getRGB(xLeft, y1) == oc && isolineImage.getRGB(xLeft, y1) != isolineColor) {
                    xLeft--;
                }
                xRight = currentSpan.getFx() + 1;
                makeSpans(image, isolineImage, spans, oc, xLeft, xRight, currentSpan, y1);
            }
            xLeft = currentSpan.getFx() + 1; // spans below
            y1 = currentSpan.getY() + 1;
            if (y1 == image.getHeight()) {
                continue;
            }
            while (xLeft > 0 && image.getRGB(xLeft, y1) == oc && isolineImage.getRGB(xLeft, y1) != isolineColor) {
                xLeft--;
            }
            xRight = currentSpan.getFx() + 1;
            makeSpans(image, isolineImage, spans, oc, xLeft, xRight, currentSpan, y1);
        }
        return image;
    }

    private void makeSpans(BufferedImage image, BufferedImage isolineImage, Stack<Span> spans, int oc, int xLeft, int xRight, Span currentSpan, int y1) {
        while (xRight < currentSpan.getTx()) {
            while (xRight < image.getWidth() && image.getRGB(xRight, y1) == oc && isolineImage.getRGB(xRight, y1) != isolineColor.getRGB()) {
                xRight++;
            }
//            if (isolineImage.getRGB(xRight, y1) == isolineColor.getRGB()) {
//                System.out.println("border found " + xRight + " " + y1);
//            }
            if (xRight - xLeft > 1) {
                spans.push(new Span(xLeft, xRight, y1));
            }
            xLeft = xRight;
            xRight++;
        }
    }

    public void clear() {
        userIsolinesImage = null;
        userPointsImage = null;
        levelIsolinesImage = null;
        interpolatedLegendImage = null;
        legendImage = null;
        levelsPointsImage = null;
        coloredImage = null;
        interpolatedColoredImage = null;
    }
}
