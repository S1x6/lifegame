package ru.nsu.fit.g16202.korshunov.mvp.presenter;

import ru.nsu.fit.g16202.korshunov.mvp.model.Line;
import ru.nsu.fit.g16202.korshunov.mvp.model.Model;
import ru.nsu.fit.g16202.korshunov.mvp.view.View;
import ru.nsu.fit.g16202.korshunov.ui.StatusBar;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.List;

public class Presenter {

    private Model model;
    private View view;
    private Timer resizeTimer;

    public Presenter(Model model, View view, StatusBar statusBar) {
        this.model = model;
        this.view = view;
        resizeTimer = new Timer(300, e -> onSettingsChanged());
        resizeTimer.setRepeats(false);
        view.setPresenter(this);
        view.setGridSize(new Dimension(model.getGridWidth(), model.getGridHeight()));
        view.repaint();
        view.addComponentListener(new ComponentAdapter() {
            @Override
            public void componentResized(ComponentEvent e) {
                super.componentResized(e);
                if (resizeTimer.isRunning()) {
                    resizeTimer.restart();
                } else {
                    resizeTimer.start();
                }
            }
        });
        view.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseReleased(MouseEvent e) {
                super.mouseReleased(e);
                if (
                        e.getY() < View.VERTICAL_MARGIN || e.getY() > View.VERTICAL_MARGIN + view.getUserAreaHeight() * (1 - View.LEGEND_RATIO_VERTICAL) ||
                                e.getX() < View.HORIZONTAL_MARGIN || e.getX() > View.HORIZONTAL_MARGIN + view.getUserAreaWidth()
                ) {
                    return;
                }
                view.addIsoline(createIsoline(e.getX() - View.HORIZONTAL_MARGIN, (e.getY() - View.VERTICAL_MARGIN)), model.getLineColor());
                view.repaint();
            }
        });
        view.addMouseMotionListener(new MouseMotionAdapter() {
            @Override
            public void mouseDragged(MouseEvent e) {
                super.mouseDragged(e);
                view.setDynamicIsoline(createIsoline(e.getX() - View.HORIZONTAL_MARGIN, e.getY() - View.VERTICAL_MARGIN));
                view.repaint();
            }

            @Override
            public void mouseMoved(MouseEvent e) {
                super.mouseMoved(e);
                if (
                        e.getY() < View.VERTICAL_MARGIN || e.getY() > View.VERTICAL_MARGIN + view.getUserAreaHeight() * (1 - View.LEGEND_RATIO_VERTICAL) ||
                                e.getX() < View.HORIZONTAL_MARGIN || e.getX() > View.HORIZONTAL_MARGIN + view.getUserAreaWidth()
                ) {
                    statusBar.setStatus("");
                    return;
                }
                double x = normalizeX(e.getX() - View.HORIZONTAL_MARGIN, view.getUserAreaWidth());
                double y = normalizeY(e.getY() - View.VERTICAL_MARGIN, (int) (view.getUserAreaHeight() * (1 - View.LEGEND_RATIO_VERTICAL)));
                double value = model.getFunctionValue(x, y);
                x = Math.round(x * 1000.0) / 1000.0;
                y = Math.round(y * 1000.0) / 1000.0;
                value = Math.round(value * 1000.0) / 1000.0;
                statusBar.setStatus("X:" + (x) + " Y:" + (y) +
                        " Value:" + value);
            }
        });
        model.setNodesCoordinates(view.getUserAreaWidth(), (int) ((1 - View.LEGEND_RATIO_VERTICAL) * view.getUserAreaHeight()));
    }

    private void initView() {
        view.drawSpanColorMap(
                o -> model.getInterpolatedValue(normalizeX((int) o[0], (int) o[1]),
                        normalizeY((int) o[2], (int) o[3])),
                o -> model.getColor((Double) o[0])
        );
        for (Double level :
                model.getLevels()) {
            view.drawIsoline(model.getIsolineLines(level), model.getColor(level));
        }
        view.drawLegend(
                o -> model.getColor((Double) o[0]),
                o -> model.getMinFunctionValue() + (model.getMaxFunctionValue() - model.getMinFunctionValue()) /
                        ((double) (int) o[1] / (int) o[0]),
                model.getLevels()
        );
        view.drawInterpolatedUserArea(
                o -> model.getInterpolatedColor((Double) o[0]),
                o -> model.getInterpolatedValue(normalizeX((int) o[0], (int) o[1]),
                        normalizeY((int) o[2], (int) o[3])));
        view.drawInterpolatedLegend(
                o -> model.getInterpolatedColor((Double) o[0]),
                o -> model.getMinFunctionValue() + (model.getMaxFunctionValue() - model.getMinFunctionValue()) /
                        ((double) (int) o[1] / (int) o[0]),
                model.getLevels()
        );
    }

    private List<Line> createIsoline(int x, int y) {
        return model.getIsolineLines(
                model.getFunctionValue(normalizeX(x, view.getUserAreaWidth()),
                        normalizeY(y, (int) ((1 - View.LEGEND_RATIO_VERTICAL) * view.getUserAreaHeight())))
        );
    }

    private double normalizeX(int x, int width) {
        return Model.MIN_X + (double) x / width * (Model.MAX_X - Model.MIN_X);
    }

    private double normalizeY(int y, int height) {
        return Model.MIN_Y + (double) y / height * (Model.MAX_Y - Model.MIN_Y);
    }

    public void showGrid(boolean showGrid) {
        view.setShowGrid(showGrid);
        view.repaint();
    }

    public void interpolate(boolean interpolate) {
        view.setShouldDrawInterpolated(interpolate);
        view.repaint();
    }

    public View getView() {
        return view;
    }

    public void showLevels(boolean show) {
        view.setShowLevels(show);
        view.repaint();
    }

    public void showPoints(boolean s) {
        view.setShouldDrawPoints(s);
        view.repaint();
    }

    public void deleteIsolines() {
        view.deleteIsolines();
        view.repaint();
    }

    public void onSettingsChanged() {
        model.init();
        view.clear();
        view.setGridSize(new Dimension(model.getGridWidth(), model.getGridHeight()));
        view.recalculateUserArea();
        model.setNodesCoordinates(view.getUserAreaWidth(), (int) ((1 - View.LEGEND_RATIO_VERTICAL) * view.getUserAreaHeight()));
        for (int i = 0; i < model.getLevels().size(); i++) {
            view.addLevelIsoline(model.getIsolineLines(model.getLevels().get(i)), model.getLineColor());
        }
        initView();
        view.repaint();
    }
}