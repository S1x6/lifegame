package ru.nsu.fit.g16202.korshunov.mvp.model;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;

public class SettingsObject {

    private int width;
    private int height;
    private int levelNum;
    private List<Double> levels = new ArrayList<>();
    private List<Color> colors = new ArrayList<>();
    private Color lineColor = Color.BLACK;

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public int getLevelNum() {
        return levelNum;
    }

    public void setLevelNum(int levelNum) {
        this.levelNum = levelNum;
    }

    public List<Color> getColors() {
        return colors;
    }

    public void setColors(List<Color> colors) {
        this.colors.clear();
        this.colors.addAll(colors);
    }

    public Color getLineColor() {
        return lineColor;
    }

    public void setLineColor(Color lineColor) {
        this.lineColor = lineColor;
    }

    public List<Double> getLevels() {
        return levels;
    }

    public void setLevels(List<Double> levels) {
        this.levels.clear();
        this.levels.addAll(levels);
    }
}
