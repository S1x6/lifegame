package ru.nsu.fit.g16202.korshunov.mvp.model;

public class Node extends Point{

    private double value;
    private boolean isBigger;
    private DoublePoint functionPoint = new DoublePoint(0,0);

    public Node(int x, int y) {
        super(x, y);
    }

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }

    public boolean isBigger() {
        return isBigger;
    }

    public void setFunctionX(double x) {
        functionPoint.setX(x);
    }

    public void setFunctionY(double y) {
        functionPoint.setY(y);
    }

    public double getFunctionX() {
        return functionPoint.getX();
    }

    public double getFunctionY() {
        return functionPoint.getY();
    }

    public void setBigger(boolean bigger) {
        isBigger = bigger;
    }
}
