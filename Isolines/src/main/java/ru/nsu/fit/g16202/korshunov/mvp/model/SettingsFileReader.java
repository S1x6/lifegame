package ru.nsu.fit.g16202.korshunov.mvp.model;

import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class SettingsFileReader {

    public static SettingsObject readSettings(String fileName) throws IOException {
        SettingsObject object = new SettingsObject();
        try {
            Scanner scanner = new Scanner(new File(fileName));
            String line = getLineWithoutComment(scanner.nextLine());
            String[] lines = line.split(" ");
            object.setWidth(Integer.valueOf(lines[0]));
            object.setHeight(Integer.valueOf(lines[1]));
            line = scanner.nextLine();
            int n = Integer.valueOf(line);
            object.setLevelNum(n);
            List<Color> colors = new ArrayList<>(n+1);
            for (int i = 0; i < n+1; ++i) {
                lines = scanner.nextLine().split(" ");
                colors.add(new Color(Integer.valueOf(lines[0]), Integer.valueOf(lines[1]), Integer.valueOf(lines[2])));
            }
            object.setColors(colors);
            lines = scanner.nextLine().split(" ");
            object.setLineColor(new Color(Integer.valueOf(lines[0]), Integer.valueOf(lines[1]), Integer.valueOf(lines[2])));

        } catch (NumberFormatException | IndexOutOfBoundsException ex) {
            throw new IOException();
        }
        return object;
    }

    private static String getLineWithoutComment(String line) {
        int commentIndex = line.indexOf("//");
        if (commentIndex != -1) {
            line = line.substring(0, commentIndex);
        }
        line = line.trim();
        return line;

    }

}
