package ru.nsu.fit.g16202.korshunov.frame;

import ru.nsu.fit.g16202.korshunov.ui.StatusBar;

import javax.swing.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

public class DefaultToolBarToggleButton extends JToggleButton {

    interface OnToggleClickListener {
        void onClick(MouseEvent e, boolean isSelected);
    }

    DefaultToolBarToggleButton(String imagePath, String title, StatusBar statusBar, OnToggleClickListener listener) {
        super(new ImageIcon(ClassLoader.getSystemResource(imagePath)));
        this.setToolTipText(title);
        this.setFocusPainted(false);
        this.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
                listener.onClick(e, isSelected());
            }

            @Override
            public void mousePressed(MouseEvent e) {

            }

            @Override
            public void mouseReleased(MouseEvent e) {

            }

            @Override
            public void mouseEntered(MouseEvent e) {
                statusBar.setStatus(title);
            }

            @Override
            public void mouseExited(MouseEvent e) {
                statusBar.setStatus("");
            }
        });
    }

}
