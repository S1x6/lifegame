package ru.nsu.fit.g16202.korshunov.mvp.model;

public interface Function {
    Object getValue(Object ... o);
}
