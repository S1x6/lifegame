package ru.nsu.fit.g16202.korshunov.mvp.model;

import java.awt.*;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Model {

    public static double MAX_X = 5.0;
    public static double MAX_Y = 5.0;
    public static double MIN_X = -5.0;
    public static double MIN_Y = -5.0;

    private double maxFunctionValue = 0.0;
    private double minFunctionValue = 0.0;

    private SettingsObject settingsObject;
    private List<Node> nodes = new ArrayList<>();

    public Model(String fileName) {
        try {
            settingsObject = SettingsFileReader.readSettings(fileName);
        } catch (IOException ex) {
            settingsObject = new SettingsObject();
            settingsObject.setLineColor(Color.BLACK);
            settingsObject.setLevelNum(3);
            settingsObject.setWidth(100);
            settingsObject.setHeight(100);
            settingsObject.setColors(Arrays.asList(new Color(255, 0, 0),
                    new Color(255, 255, 0), new Color(0, 255, 255),
                    new Color(0, 0, 255)));
        }
        init();
    }

    public void init() {
        calculateNodes();
        double levelStep = (maxFunctionValue - minFunctionValue) / (settingsObject.getLevelNum() + 1);
        double levelValue = minFunctionValue;
        List<Double> levels = new ArrayList<>(settingsObject.getLevelNum());
        for (int i = 0; i < settingsObject.getLevelNum(); i++) {
            levelValue += levelStep;
            System.out.println(levelValue);
            levels.add(levelValue);
        }
        settingsObject.setLevels(levels);
    }

    public int getGridWidth() {
        return settingsObject.getWidth();
    }

    public int getGridHeight() {
        return settingsObject.getHeight();
    }

    public void setNodesCoordinates(int width, int height) {
        double stepPixelX = (double) width / (settingsObject.getWidth() - 1);
        double stepPixelY = (double) height / (settingsObject.getHeight() - 1);
        for (int j = 0; j < settingsObject.getHeight(); ++j) {
            for (int i = 0; i < settingsObject.getWidth(); i++) {
                Node node = nodes.get(j * settingsObject.getWidth() + i);
                node.setX((int) Math.round(i * stepPixelX));
                node.setY((int) Math.round(j * stepPixelY));
            }
        }
    }

    public double getInterpolatedValue(double x, double y) {
        int i = (int) ((x - MIN_X) * (settingsObject.getWidth() - 1) / (MAX_X - MIN_X));
        int j = (int) ((y - MIN_Y) * (settingsObject.getHeight() - 1) / (MAX_Y - MIN_Y));
        int iNext = i + 1 == settingsObject.getWidth() ? i : i + 1;
        int jNext = j + 1 == settingsObject.getHeight() ? j : j + 1;
        double stepX = Math.abs(nodes.get(1).getFunctionX() - nodes.get(0).getFunctionX());
        double stepY = Math.abs(nodes.get(0).getFunctionY() - nodes.get(settingsObject.getWidth()).getFunctionY());
        double leftPart = (x - nodes.get(j * settingsObject.getWidth() + i).getFunctionX()) / stepX;
        double p1 = leftPart * nodes.get(j * settingsObject.getWidth() + iNext).getValue() +
                (1 - leftPart) * nodes.get(j * settingsObject.getWidth() + i).getValue();
        double p2 = leftPart * nodes.get(jNext * settingsObject.getWidth() + iNext).getValue() +
                (1 - leftPart) * nodes.get(jNext * settingsObject.getWidth() + i).getValue();
        double upperPart = (y - nodes.get(j * settingsObject.getWidth() + i).getFunctionY()) / stepY;
        return p2 * upperPart + (1 - upperPart) * p1;
//        System.out.println(getFunctionValue(x,y) + " " + p1 + " " + p2 + " ");
//        return 0.0;
    }

    public void calculateNodes() {
        nodes.clear();
        minFunctionValue = 0;
        maxFunctionValue = 0;
        double stepX = (MAX_X - MIN_X) / (settingsObject.getWidth() - 1);
        double stepY = (MAX_Y - MIN_Y) / (settingsObject.getHeight() - 1);
        for (int j = 0; j < settingsObject.getHeight(); ++j) {
            for (int i = 0; i < settingsObject.getWidth(); i++) {
                double value = getFunctionValue(MIN_X + i * stepX, MIN_Y + j * stepY);
                maxFunctionValue = Math.max(maxFunctionValue, value);
                minFunctionValue = Math.min(minFunctionValue, value);
                Node node = new Node(0, 0);
                node.setFunctionX(MIN_X + i * stepX);
                node.setFunctionY(MIN_Y + j * stepY);
                node.setValue(value);
                nodes.add(node);
            }
        }
    }

    public int getInterpolatedColor(double functionValue) {
        int levelNum = getLevelIntervalNum(functionValue);
        if (levelNum == 0) {
            return settingsObject.getColors().get(0).getRGB();
        }
        if (levelNum == getLevels().size() + 1) {
            return settingsObject.getColors().get(settingsObject.getColors().size() - 1).getRGB();
        }
        levelNum--;
        double halfStep = Math.abs((getLevels().get(0) - minFunctionValue) / 2);
        int nextLevelNum = levelNum + 1;
        double level = levelNum == 0 ? minFunctionValue : settingsObject.getLevels().get(levelNum - 1);
        double nextLevel = nextLevelNum - 1 == getLevels().size() ? maxFunctionValue - halfStep : settingsObject.getLevels().get(nextLevelNum - 1);
        int prevR = (settingsObject.getColors().get(levelNum).getRGB() >> 16) & 0x0000ff;
        int nextR = (settingsObject.getColors().get(nextLevelNum).getRGB() >> 16) & 0x0000ff;
        int r = (int) Math.round(prevR * (nextLevel + halfStep - functionValue) / (nextLevel - level) +
                Math.round(nextR * (functionValue - level - halfStep) / (nextLevel - level)));
        int g = (int) Math.round(((settingsObject.getColors().get(levelNum).getRGB() >> 8) & 0x0000ff) *
                (nextLevel + halfStep - functionValue) / (nextLevel - level) +
                Math.round(((settingsObject.getColors().get(nextLevelNum).getRGB() >> 8) & 0x0000ff) *
                        (functionValue - level - halfStep) / (nextLevel - level)));
        int b = (int) Math.round(((settingsObject.getColors().get(levelNum).getRGB()) & 0x0000ff) *
                (nextLevel + halfStep - functionValue) / (nextLevel - level) +
                Math.round(((settingsObject.getColors().get(nextLevelNum).getRGB()) & 0x0000ff) *
                        (functionValue - level - halfStep) / (nextLevel - level)));
        r = Math.min(255, r);
        g = Math.min(255, g);
        b = Math.min(255, b);
        return (r << 16) | (g << 8) | b;
    }

    public int getLevelIntervalNum(double functionValue) {
        int i = 0;
        double halfStep = Math.abs(settingsObject.getLevels().get(0) - minFunctionValue) / 2;
        if (functionValue <= minFunctionValue + halfStep) {
            return 0;
        }
        for (; i < settingsObject.getLevels().size(); i++) {
            if (settingsObject.getLevels().get(i) + halfStep >= functionValue) {
                return i + 1;
            }
        }
        return i + 1;
    }

    public double getFunctionValue(double x, double y) { //-5 to 5
        return x * Math.cos(x) - Math.sin(y);
//        return Math.sin(y) * Math.cos(x);
//        return (Math.sqrt(Math.pow(2*x, 2) + Math.pow(y,2))) + 2*Math.sin(x) + Math.cos(y);
//        return Math.sin(y) + Math.cos(x);
//        return -1 * Math.sin(2 * x + 2 * y) + 1;
//        return -1 * Math.sin(Math.pow(x, 2) + Math.pow(y, 2)) + 1;
    }

    public List<Line> getIsolineLines(double value) {
        //System.out.println(value);
        while (setSigns(value) == -1) {
            System.out.println("3 points");
            value += (maxFunctionValue - minFunctionValue) / 1000;
        }
        ArrayList<Line> lines = new ArrayList<>();
        List<Point> isolinePoints = new ArrayList<>(4);
        for (int j = 0; j < settingsObject.getHeight() - 1; j++) {
            for (int i = 0; i < settingsObject.getWidth() - 1; i++) {
                Node leftTop = nodes.get(j * settingsObject.getWidth() + i);
                Node rightTop = nodes.get(j * settingsObject.getWidth() + i + 1);
                Node leftBottom = nodes.get((j + 1) * settingsObject.getWidth() + i);
                Node rightBottom = nodes.get((j + 1) * settingsObject.getWidth() + i + 1);
                isolinePoints.clear();
                if (leftTop.isBigger() != rightTop.isBigger()) {
                    int step = rightTop.getX() - leftTop.getX();
                    int x = leftTop.getX() + (int) (step * (value - leftTop.getValue()) / (rightTop.getValue() - leftTop.getValue()));
                    isolinePoints.add(new Point(x, leftTop.getY()));
                }
                if (rightTop.isBigger() != rightBottom.isBigger()) {
                    int step = rightBottom.getY() - rightTop.getY();
                    int y = rightTop.getY() + (int) (step * (value - rightTop.getValue()) / (rightBottom.getValue() - rightTop.getValue()));
                    isolinePoints.add(new Point(rightTop.getX(), y));
                }
                if (rightBottom.isBigger() != leftBottom.isBigger()) {
                    int step = rightBottom.getX() - leftBottom.getX();
                    int x = leftBottom.getX() + (int) (step * (value - leftBottom.getValue()) / (rightBottom.getValue() - leftBottom.getValue()));
                    isolinePoints.add(new Point(x, rightBottom.getY()));
                }
                if (leftTop.isBigger() != leftBottom.isBigger()) {
                    int step = leftBottom.getY() - leftTop.getY();
                    int y = leftTop.getY() + (int) (step * (value - leftTop.getValue()) / (leftBottom.getValue() - leftTop.getValue()));
                    isolinePoints.add(new Point(leftTop.getX(), y));
                }
                if (isolinePoints.size() == 2) { // 2 points
                    lines.add(new Line(isolinePoints.get(0), isolinePoints.get(1)));
                } else if (isolinePoints.size() == 4) { // 4 points
                    if ((leftBottom.getValue() + leftTop.getValue() + rightBottom.getValue() + rightTop.getValue()) / 4 > value) {
                        if (leftTop.isBigger()) {
                            lines.add(new Line(isolinePoints.get(0), isolinePoints.get(1)));
                            lines.add(new Line(isolinePoints.get(2), isolinePoints.get(3)));
                        } else {
                            lines.add(new Line(isolinePoints.get(1), isolinePoints.get(2)));
                            lines.add(new Line(isolinePoints.get(3), isolinePoints.get(0)));
                        }
                    } else {
                        if (!leftTop.isBigger()) {
                            lines.add(new Line(isolinePoints.get(0), isolinePoints.get(1)));
                            lines.add(new Line(isolinePoints.get(2), isolinePoints.get(3)));
                        } else {
                            lines.add(new Line(isolinePoints.get(1), isolinePoints.get(2)));
                            lines.add(new Line(isolinePoints.get(3), isolinePoints.get(0)));
                        }
                    }
                }
            }
        }
        return lines;
    }

    private int setSigns(double value) {
        for (Node node : nodes) {
            if (node.getValue() == value) {
                return -1;
            }
            node.setBigger(node.getValue() > value);
        }
        return 0;
    }

    public int getColor(double functionValue) {
        int i = 0;
        for (; i < settingsObject.getLevels().size(); ++i) {
            if (settingsObject.getLevels().get(i) > functionValue) {
                break;
            }
        }
        return settingsObject.getColors().get(i).getRGB();

    }

    public double getMaxFunctionValue() {
        return maxFunctionValue;
    }

    public double getMinFunctionValue() {
        return minFunctionValue;
    }

    public Color getLineColor() {
        return settingsObject.getLineColor();
    }

    public List<Double> getLevels() {
        return settingsObject.getLevels();
    }

    public void setGridWidth(int width) {
        settingsObject.setWidth(width);
    }

    public void setGridHeight(Integer height) {
        settingsObject.setHeight(height);
    }
}
