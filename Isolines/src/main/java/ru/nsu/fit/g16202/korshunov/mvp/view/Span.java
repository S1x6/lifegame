package ru.nsu.fit.g16202.korshunov.mvp.view;

import java.awt.image.BufferedImage;

public class Span {
    private int fx;
    private int tx;
    private int y;

    public Span(int fx, int tx, int y) {
        this.fx = fx;
        this.tx = tx;
        this.y = y;
    }

    public void fill(BufferedImage image, int color) {
        for (int i = fx + 1; i < tx; ++i) {
            image.setRGB(i, y, color);
        }
    }

    public int getFx() {
        return fx;
    }

    public int getTx() {
        return tx;
    }

    public int getY() {
        return y;
    }
}
