package ru.nsu.fit.g16202.korshunov.frame;

import ru.nsu.fit.g16202.korshunov.mvp.model.Model;
import ru.nsu.fit.g16202.korshunov.mvp.presenter.Presenter;
import ru.nsu.fit.g16202.korshunov.mvp.view.View;
import ru.nsu.fit.g16202.korshunov.ui.SettingsDialog;
import ru.nsu.fit.g16202.korshunov.ui.StatusBar;

import javax.swing.*;
import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

public class MainFrame extends JFrame {

    Presenter presenter;
    private StatusBar statusBar;
    private JToggleButton grid;
    private JToggleButton interpolation;
    private JToggleButton points;
    private JToggleButton levels;
    private JCheckBoxMenuItem gridMenu;
    private JCheckBoxMenuItem interpolationMenu;
    private JCheckBoxMenuItem pointsMenu;
    private JCheckBoxMenuItem levelsMenu;

    public static void main(String[] args) {
        new MainFrame();
    }

    private MainFrame() {
        super();
        Dimension minDim = new Dimension(1100, 700);
        setSize(minDim);
        setMinimumSize(minDim);
        setTitle("Isolines NSU_g16202_Korshunov");
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
        Path path = new File("./Isolines_NSU_g16202_Korshunov_Data").toPath();
        if (!Files.exists(path)) {
            try {
                Files.createDirectory(path);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        this.setLocation(dim.width / 2 - this.getSize().width / 2, dim.height / 2 - this.getSize().height / 2);
        this.setLayout(new BorderLayout());
        Model model = new Model("./Isolines_NSU_g16202_Korshunov_Data/config.txt");
        initMenu(model);
        initStatusBar();
        initToolbar(model);
        presenter = new Presenter(model, new View(model.getLineColor()), statusBar);
        add(presenter.getView(), BorderLayout.CENTER);
        pack();
        setVisible(true);

    }

    private void initToolbar(Model model) {
        JToolBar toolBar = new JToolBar();
        JButton settings = new DefaultToolBarButton("settings.png", "Настройки", statusBar, e -> {
            settingsOnClick(model);
        });
        toolBar.add(settings);
        JButton clear = new DefaultToolBarButton("delete.png", "Очистить изолинии", statusBar, e -> {
            presenter.deleteIsolines();
        });
        toolBar.add(clear);
        grid = new DefaultToolBarToggleButton("grid.png", "Сетка", statusBar, (e, s) -> {
            presenter.showGrid(s);
            gridMenu.setSelected(s);
        });
        toolBar.add(grid);
        levels = new DefaultToolBarToggleButton("levels.png", "Уровни", statusBar, (e, s) -> {
            presenter.showLevels(s);
            levelsMenu.setSelected(s);
        });
        toolBar.add(levels);
        interpolation = new DefaultToolBarToggleButton("inter.png", "Интерполяция", statusBar, (e, s) -> {
            presenter.interpolate(s);
            interpolationMenu.setSelected(s);
        });
        toolBar.add(interpolation);
        points = new DefaultToolBarToggleButton("points.png", "Точки", statusBar, (e, s) -> {
            presenter.showPoints(s);
            pointsMenu.setSelected(s);
        });
        toolBar.add(points);
        JButton about = new DefaultToolBarButton("info.png", "О программе", statusBar, e -> {
            JOptionPane.showMessageDialog(this,
                    "Приложение \"Изолинии\" \nАвтор: Коршунов Вадим\nГруппа: 16020\nВерсия 1.0",
                    "О программе", JOptionPane.INFORMATION_MESSAGE, new ImageIcon(ClassLoader.getSystemResource("me.jpg")));
        });
        toolBar.add(about);
        add(toolBar, BorderLayout.NORTH);
    }

    private void initStatusBar() {
        statusBar = new StatusBar(this);
        add(statusBar, BorderLayout.SOUTH);
    }

    JMenuItem about = new JMenuItem("О программе");

    private void initMenu(Model model) {
        JMenuBar menuBar = new JMenuBar();
        JMenu fileMenu = new JMenu("Файл");
        JMenuItem options = new JMenuItem("Настройки");
        options.addActionListener(e -> settingsOnClick(model));
        fileMenu.add(options);
        JMenuItem exit = new JMenuItem("Выход");
        exit.addActionListener(e -> System.exit(0));
        fileMenu.add(exit);
        JMenu viewMenu = new JMenu("Функция");
        JMenuItem delete = new JMenuItem("Удалить изолинии");
        delete.addActionListener(e -> presenter.deleteIsolines());
        viewMenu.add(delete);
        gridMenu = new JCheckBoxMenuItem("Сетка");
        gridMenu.addActionListener(e -> {
            grid.setSelected(gridMenu.isSelected());
            presenter.showGrid(gridMenu.isSelected());
        });
        viewMenu.add(gridMenu);
        interpolationMenu = new JCheckBoxMenuItem("Интерполяция");
        interpolationMenu.addActionListener(e -> {
            interpolation.setSelected(interpolationMenu.isSelected());
            presenter.interpolate(interpolationMenu.isSelected());
        });
        viewMenu.add(interpolationMenu);
        pointsMenu = new JCheckBoxMenuItem("Точки");
        pointsMenu.addActionListener(e -> {
            points.setSelected(pointsMenu.isSelected());
            presenter.showPoints(pointsMenu.isSelected());
        });
        viewMenu.add(pointsMenu);
        levelsMenu = new JCheckBoxMenuItem("Уровни");
        levelsMenu.addActionListener(e -> {
            levels.setSelected(levelsMenu.isSelected());
            presenter.showLevels(levelsMenu.isSelected());
        });
        viewMenu.add(levelsMenu);
        about.addActionListener(e -> JOptionPane.showMessageDialog(this,
                "Приложение \"Изолинии\" \nАвтор: Коршунов Вадим\nГруппа: 16020\nВерсия 1.0",
                "О программе", JOptionPane.INFORMATION_MESSAGE, new ImageIcon(ClassLoader.getSystemResource("me.jpg"))));
        menuBar.add(fileMenu);
        menuBar.add(viewMenu);
        menuBar.add(about);
        this.setJMenuBar(menuBar);
    }

    private void settingsOnClick(Model model) {
        SettingsDialog dialog = new SettingsDialog(this);
        dialog.width.setText(String.valueOf(model.getGridWidth()));
        dialog.height.setText(String.valueOf(model.getGridHeight()));
        dialog.maxX.setText(String.valueOf(Model.MAX_X));
        dialog.maxY.setText(String.valueOf(Model.MAX_Y));
        dialog.minX.setText(String.valueOf(Model.MIN_X));
        dialog.minY.setText(String.valueOf(Model.MIN_Y));
        dialog.setVisible(true);
        if (dialog.isOk()) {
            model.setGridWidth(Integer.valueOf(dialog.width.getText()));
            model.setGridHeight(Integer.valueOf(dialog.height.getText()));
            Model.MIN_Y = Double.valueOf(dialog.minY.getText());
            Model.MIN_X = Double.valueOf(dialog.minX.getText());
            Model.MAX_X = Double.valueOf(dialog.maxX.getText());
            Model.MAX_Y = Double.valueOf(dialog.maxY.getText());
            presenter.onSettingsChanged();
        }
    }

}
