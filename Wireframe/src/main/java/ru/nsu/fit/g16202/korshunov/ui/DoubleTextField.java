package ru.nsu.fit.g16202.korshunov.ui;

import com.sun.istack.internal.Nullable;

import javax.swing.*;
import javax.swing.plaf.basic.BasicArrowButton;
import java.awt.*;
import java.awt.event.ActionListener;

public class DoubleTextField extends JPanel {

    private Double max;
    private Double min;
    private Double step;
    private JTextField textField = new JTextField(5);
    private ActionListener customClickListener = null;

    public DoubleTextField(String label, @Nullable Double min, @Nullable Double max, Double initValue, Double step) {
        setLayout(new FlowLayout());
        add(new JLabel(label));
        add(textField);
        this.step = step;
        JPanel panel = new JPanel();
        panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
        JButton upButton = new BasicArrowButton(SwingConstants.NORTH);
        JButton downButton = new BasicArrowButton(SwingConstants.SOUTH);
        panel.add(upButton);
        panel.add(downButton);
        add(panel);
        this.max = max;
        this.min = min;
        upButton.addActionListener(e -> {
            double oldValue;
            try {
                oldValue = Double.valueOf(textField.getText());
            } catch (NumberFormatException ex) {
                return;
            }
            if (this.max == null) {
                textField.setText(String.valueOf(normalize(oldValue + this.step)));
            } else {
                if (getValue().equals(this.max)) {
                    return;
                }
                textField.setText(oldValue + this.step > this.max ? String.valueOf(normalize(this.max)) : String.valueOf(normalize(oldValue + this.step)));
            }
            if (customClickListener != null) {
                customClickListener.actionPerformed(e);
            }
        });
        downButton.addActionListener(e -> {
            double oldValue;
            try {
                oldValue = Double.valueOf(textField.getText());
            } catch (NumberFormatException ex) {
                return;
            }
            if (this.min == null) {
                textField.setText(String.valueOf(normalize(oldValue - this.step)));
            } else {
                if (getValue().equals(this.min)) {
                    return;
                }
                textField.setText(oldValue - this.step < this.min ? String.valueOf(normalize(this.min)) : String.valueOf(normalize(oldValue - this.step)));
            }
            if (customClickListener != null) {
                customClickListener.actionPerformed(e);
            }
        });
        textField.setText(initValue.toString());
        textField.setInputVerifier(new InputVerifier() {
            @Override
            public boolean verify(JComponent input) {
                String text = ((JTextField) input).getText();
                double num;
                try {
                    num = Double.valueOf(text);
                } catch (NumberFormatException e) {
                    return false;
                }
                if ((DoubleTextField.this.max == null || num <= DoubleTextField.this.max) && (DoubleTextField.this.min == null || num >= DoubleTextField.this.min)) {
                    return true;
                }
                return false;
            }
        });
    }

    private Double normalize(Double v) {
        return ((int) (v * 100)) / 100.;
    }

    public void setEnabled(boolean e) {
        textField.setEnabled(e);
    }

    public void setOnArrowsClickedListener(ActionListener listener) {
        customClickListener = listener;
    }

    public Double getValue() {
        return Double.valueOf(textField.getText());
    }

    public void setValue(double value) {
        textField.setText(String.valueOf(value));
    }

    public boolean validateData() {
        try {
            if ((this.max != null && getValue() > this.max) || (this.min != null && getValue() < this.min))
                return false;
        } catch (NumberFormatException ex) {
            return false;
        }
        return true;
    }

    public void setMax(double max) {
        this.max = max;
    }

    public void setMin(double min) {
        this.min = min;
    }
}
