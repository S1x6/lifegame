package ru.nsu.fit.g16202.korshunov.ui;

import com.sun.istack.internal.Nullable;

import javax.swing.*;
import javax.swing.plaf.basic.BasicArrowButton;
import java.awt.*;
import java.awt.event.ActionListener;

public class IntegerTextField extends JPanel {

    private Integer max;
    private Integer min;
    private JTextField textField = new JTextField(5);
    private ActionListener customClickListener = null;

    public IntegerTextField(String label, @Nullable Integer min, @Nullable Integer max, Integer initValue) {
        setLayout(new FlowLayout());
        add(new JLabel(label));
        add(textField);
        JPanel panel = new JPanel();
        panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
        JButton upButton = new BasicArrowButton(SwingConstants.NORTH);
        JButton downButton = new BasicArrowButton(SwingConstants.SOUTH);
        panel.add(upButton);
        panel.add(downButton);
        add(panel);
        this.max = max;
        this.min = min;
        upButton.addActionListener(e -> {
            int oldValue;
            try {
                oldValue = Integer.valueOf(textField.getText());
            } catch (NumberFormatException ex) {
                return;
            }
            if (this.max == null) {
                textField.setText(String.valueOf(oldValue + 1));
            } else {
                if (getValue().equals(this.max)) {
                    return;
                }
                textField.setText(oldValue + 1 > this.max ? String.valueOf(this.max) : String.valueOf(oldValue + 1));
            }
            if (customClickListener != null) {
                customClickListener.actionPerformed(e);
            }
        });
        downButton.addActionListener(e -> {
            int oldValue;
            try {
                oldValue = Integer.valueOf(textField.getText());
            } catch (NumberFormatException ex) {
                return;
            }
            if (this.min == null) {
                textField.setText(String.valueOf(oldValue - 1));
            } else {
                if (getValue().equals(this.min)) {
                    return;
                }
                textField.setText(oldValue - 1 < this.min ? String.valueOf(this.min) : String.valueOf(oldValue - 1));
            }
            if (customClickListener != null) {
                customClickListener.actionPerformed(e);
            }
        });
        textField.setText(initValue.toString());
        textField.setInputVerifier(new InputVerifier() {
            @Override
            public boolean verify(JComponent input) {
                String text = ((JTextField) input).getText();
                int num;
                try {
                    num = Integer.parseInt(text);
                } catch (NumberFormatException e) {
                    return false;
                }
                if ((IntegerTextField.this.max == null || num <= IntegerTextField.this.max) && (IntegerTextField.this.min == null || num >= IntegerTextField.this.min)) {
                    return true;
                }
                return false;
            }
        });
    }

    public void setEnabled(boolean e) {
        textField.setEnabled(e);
    }

    public void setOnArrowsClickedListener(ActionListener listener) {
        customClickListener = listener;
    }

    public Integer getValue() {
        return Integer.valueOf(textField.getText());
    }

    public void setValue(int value) {
        textField.setText(String.valueOf(value));
    }

    public void setMax(Integer max) {
        this.max = max;
    }

    public boolean validateData() {
        try {
            if ((this.max != null && getValue() > this.max) || (this.min != null && getValue() < this.min))
                return false;
        } catch (NumberFormatException ex) {
            return false;
        }
        return true;
    }

    public void setMin(Integer min) {
        this.min = min;
    }
}