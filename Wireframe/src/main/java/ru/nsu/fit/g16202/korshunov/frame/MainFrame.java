package ru.nsu.fit.g16202.korshunov.frame;

import ru.nsu.fit.g16202.korshunov.mvp.model.Matrix;
import ru.nsu.fit.g16202.korshunov.mvp.model.Point;
import ru.nsu.fit.g16202.korshunov.mvp.presenter.Presenter;
import ru.nsu.fit.g16202.korshunov.ui.StatusBar;

import javax.swing.*;

import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

public class MainFrame extends JFrame {

    Presenter presenter;
    StatusBar statusBar;

    public static void main(String[] args) {
        try {
            UIManager.setLookAndFeel(
                    UIManager.getSystemLookAndFeelClassName());
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | UnsupportedLookAndFeelException e) {
            e.printStackTrace();
        }
        new MainFrame();
    }

    private MainFrame() {
        super();
        Dimension minDim = new Dimension(1100, 700);
        setSize(minDim);
        setMinimumSize(minDim);
        setTitle("Wireframe NSU_g16202_Korshunov");
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
        Path path = new File("./Wireframe_NSU_g16202_Korshunov_Data").toPath();
        if (!Files.exists(path)) {
            try {
                Files.createDirectory(path);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        this.setLocation(dim.width / 2 - this.getSize().width / 2, dim.height / 2 - this.getSize().height / 2);
        this.setLayout(new BorderLayout());
        initMenu();
        initStatusBar();
        initToolbar();
        presenter = new Presenter(this);
        add(presenter.getViewPort(), BorderLayout.CENTER);
        pack();
        setVisible(true);
    }

    private void initToolbar() {
        JToolBar toolBar = new JToolBar();
        toolBar.setFloatable(false);
        JButton open = new DefaultToolBarButton("folder.png", "Открыть файл сцены", statusBar, e -> presenter.openNewScene());
        toolBar.add(open);
        JButton save = new DefaultToolBarButton("save.png", "Сохранить сцену в файл", statusBar, e -> presenter.saveSceneToFile());
        toolBar.add(save);
        JButton init = new DefaultToolBarButton("home-button.png", "Сброс сцены", statusBar, e -> presenter.init());
        toolBar.add(init);
        JButton settings = new DefaultToolBarButton("settings.png", "Настройки сцены", statusBar, e -> presenter.openSettings());
        toolBar.add(settings);
        JButton add = new DefaultToolBarButton("add.png", "Добавить объект", statusBar, e -> presenter.createNewSpline());
        toolBar.add(add);
        JButton delete = new DefaultToolBarButton("delete.png", "Редактировать объект", statusBar, e -> presenter.deleteSpline());
        toolBar.add(delete);
        JButton left = new DefaultToolBarButton("left-arrow.png", "Предыдущий объект", statusBar, e -> presenter.selectPrevious());
        toolBar.add(left);
        JButton right = new DefaultToolBarButton("right-arrow.png", "Следующий объект", statusBar, e -> presenter.selectNext());
        toolBar.add(right);
        JButton about = new DefaultToolBarButton("info.png", "О программе", statusBar, e ->{
                int reply = JOptionPane.showConfirmDialog(this, "Показать инструкцию?","О программе",JOptionPane.YES_NO_OPTION);
                if (reply == JOptionPane.YES_OPTION) {
                    JOptionPane.showMessageDialog(this,
                            "Q - поворот тела вокруг оси X\n" +
                                    "W - поворот тела вокруг оси Y\n" +
                                    "E - поворот тела вокруг оси Z\n" +
                                    "A - перемещение опорной точки тела по оси X\n" +
                                    "S - перемещение опорной точки тела по оси Y\n" +
                                    "D - перемещение опорной точки тела по оси Z\n" +
                                    "Z - изменение масштаба по оси X (не сохраняется в файл, поэтому теряется после перезагрузки сцены)\n" +
                                    "X - изменение масштаба по оси X (не сохраняется в файл, поэтому теряется после перезагрузки сцены)\n" +
                                    "C - изменение масштаба по оси X (не сохраняется в файл, поэтому теряется после перезагрузки сцены)\n" +
                                    "F - перемещение камеры по оси X (камера всегда смотрит в точку (0, 0, 0)\n" +
                                    "G - перемещение камеры по оси Y (камера всегда смотрит в точку (0, 0, 0)\n" +
                                    "H - перемещение камеры по оси Z (камера всегда смотрит в точку (0, 0, 0)\n" +
                                    "V - поворот камеры по оси X\n" +
                                    "B - поворот камеры по оси Y\n" +
                                    "N - поворот камеры по оси Z",
                            "О программе", JOptionPane.INFORMATION_MESSAGE);
                }
                JOptionPane.showMessageDialog(this,
                "Приложение \"Wireframe\" \nАвтор: Коршунов Вадим\nГруппа: 16202\nВерсия 1.0",
                "О программе", JOptionPane.INFORMATION_MESSAGE, new ImageIcon(ClassLoader.getSystemResource("me.jpg")));
        });
        toolBar.add(about);
        add(toolBar, BorderLayout.NORTH);
    }

    private void initStatusBar() {
        statusBar = new StatusBar(this);
        add(statusBar, BorderLayout.SOUTH);
    }

    private void initMenu() {
//        JMenuBar menuBar = new JMenuBar();
//        JMenu fileMenu = new JMenu("Файл");
//        JMenuItem options = new JMenuItem("Настройки");
//        options.addActionListener(e -> settingsOnClick(model));
//        fileMenu.add(options);
//        JMenuItem exit = new JMenuItem("Выход");
//        exit.addActionListener(e -> System.exit(0));
//        fileMenu.add(exit);
//        JMenu viewMenu = new JMenu("Функция");
//        JMenuItem delete = new JMenuItem("Удалить изолинии");
//        delete.addActionListener(e -> presenter.deleteIsolines());
//        viewMenu.add(delete);
//        gridMenu = new JCheckBoxMenuItem("Сетка");
//        gridMenu.addActionListener(e -> {
//            grid.setSelected(gridMenu.isSelected());
//            presenter.showGrid(gridMenu.isSelected());
//        });
//        viewMenu.add(gridMenu);
//        interpolationMenu = new JCheckBoxMenuItem("Интерполяция");
//        interpolationMenu.addActionListener(e -> {
//            interpolation.setSelected(interpolationMenu.isSelected());
//            presenter.interpolate(interpolationMenu.isSelected());
//        });
//        viewMenu.add(interpolationMenu);
//        pointsMenu = new JCheckBoxMenuItem("Точки");
//        pointsMenu.addActionListener(e -> {
//            points.setSelected(pointsMenu.isSelected());
//            presenter.showPoints(pointsMenu.isSelected());
//        });
//        viewMenu.add(pointsMenu);
//        levelsMenu = new JCheckBoxMenuItem("Уровни");
//        levelsMenu.addActionListener(e -> {
//            levels.setSelected(levelsMenu.isSelected());
//            presenter.showLevels(levelsMenu.isSelected());
//        });
//        viewMenu.add(levelsMenu);
//        about.addActionListener(e -> JOptionPane.showMessageDialog(this,
//                "Приложение \"Изолинии\" \nАвтор: Коршунов Вадим\nГруппа: 16020\nВерсия 1.0",
//                "О программе", JOptionPane.INFORMATION_MESSAGE, new ImageIcon(ClassLoader.getSystemResource("me.jpg"))));
//        menuBar.add(fileMenu);
//        menuBar.add(viewMenu);
//        menuBar.add(about);
//        this.setJMenuBar(menuBar);
    }


}
