package ru.nsu.fit.g16202.korshunov.mvp.presenter;

import java.awt.*;
import java.awt.event.KeyEvent;
import java.util.function.Consumer;

public class KeyListener {

    private boolean isQPressed = false;
    private boolean isWPressed = false;
    private boolean isEPressed = false;
    private boolean isAPressed = false;
    private boolean isSPressed = false;
    private boolean isDPressed = false;
    private boolean isZPressed = false;
    private boolean isXPressed = false;
    private boolean isCPressed = false;
    private boolean isVPressed = false;
    private boolean isBPressed = false;
    private boolean isNPressed = false;
    private boolean isFPressed = false;
    private boolean isGPressed = false;
    private boolean isHPressed = false;

    public KeyListener() {
        KeyboardFocusManager.getCurrentKeyboardFocusManager().addKeyEventDispatcher(ke -> {
            switch (ke.getID()) {
                case KeyEvent.KEY_PRESSED:
                    switch (ke.getKeyCode()) {
                        case KeyEvent.VK_Q:
                            isQPressed = true;
                            break;
                        case KeyEvent.VK_W:
                            isWPressed = true;
                            break;
                        case KeyEvent.VK_E:
                            isEPressed = true;
                            break;
                        case KeyEvent.VK_A:
                            isAPressed = true;
                            break;
                        case KeyEvent.VK_S:
                            isSPressed = true;
                            break;
                        case KeyEvent.VK_D:
                            isDPressed = true;
                            break;
                        case KeyEvent.VK_Z:
                            isZPressed = true;
                            break;
                        case KeyEvent.VK_X:
                            isXPressed = true;
                            break;
                        case KeyEvent.VK_C:
                            isCPressed = true;
                            break;
                        case KeyEvent.VK_V:
                            isVPressed = true;
                            break;
                        case KeyEvent.VK_B:
                            isBPressed = true;
                            break;
                        case KeyEvent.VK_N:
                            isNPressed = true;
                            break;
                        case KeyEvent.VK_F:
                            isFPressed = true;
                            break;
                        case KeyEvent.VK_G:
                            isGPressed = true;
                            break;
                        case KeyEvent.VK_H:
                            isHPressed = true;
                            break;
                    }
                    break;

                case KeyEvent.KEY_RELEASED:
                    switch (ke.getKeyCode()) {
                        case KeyEvent.VK_Q:
                            isQPressed = false;
                            break;
                        case KeyEvent.VK_W:
                            isWPressed = false;
                            break;
                        case KeyEvent.VK_E:
                            isEPressed = false;
                            break;
                        case KeyEvent.VK_A:
                            isAPressed = false;
                            break;
                        case KeyEvent.VK_S:
                            isSPressed = false;
                            break;
                        case KeyEvent.VK_D:
                            isDPressed = false;
                            break;
                        case KeyEvent.VK_Z:
                            isZPressed = false;
                            break;
                        case KeyEvent.VK_X:
                            isXPressed = false;
                            break;
                        case KeyEvent.VK_C:
                            isCPressed = false;
                            break;
                        case KeyEvent.VK_V:
                            isVPressed = false;
                            break;
                        case KeyEvent.VK_B:
                            isBPressed = false;
                            break;
                        case KeyEvent.VK_N:
                            isNPressed = false;
                            break;
                        case KeyEvent.VK_F:
                            isFPressed = false;
                            break;
                        case KeyEvent.VK_G:
                            isGPressed = false;
                            break;
                        case KeyEvent.VK_H:
                            isHPressed = false;
                            break;
                    }
                    break;
            }
            return false;
        });
    }

    public boolean isQPressed() {
        return isQPressed;
    }

    public boolean isWPressed() {
        return isWPressed;
    }

    public boolean isEPressed() {
        return isEPressed;
    }

    public boolean isAPressed() {
        return isAPressed;
    }

    public boolean isSPressed() {
        return isSPressed;
    }

    public boolean isDPressed() {
        return isDPressed;
    }

    public boolean isZPressed() {
        return isZPressed;
    }

    public boolean isVPressed() {
        return isVPressed;
    }

    public boolean isXPressed() {
        return isXPressed;
    }

    public boolean isCPressed() {
        return isCPressed;
    }

    public boolean isBPressed() {
        return isBPressed;
    }

    public boolean isNPressed() {
        return isNPressed;
    }

    public boolean isFPressed() {
        return isFPressed;
    }

    public boolean isGPressed() {
        return isGPressed;
    }

    public boolean isHPressed() {
        return isHPressed;
    }
}

