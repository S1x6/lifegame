package ru.nsu.fit.g16202.korshunov.mvp.view;

import ru.nsu.fit.g16202.korshunov.mvp.model.Point;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class ReferencePoint extends JComponent {

    protected java.awt.Point anchorPoint;
    private Point point;
    private Double scaleCoefficient;
    private int centerOffsetX;
    private int centerOffsetY;
//    private int index;

    public ReferencePoint(Point point, Double scaleCoefficient) {
        this.point = point;
//        this.index = index;
        this.scaleCoefficient = scaleCoefficient;
        addDragListeners();
    }

    private void addDragListeners() {
        addMouseMotionListener(new MouseAdapter() {

            @Override
            public void mouseMoved(MouseEvent e) {
                anchorPoint = e.getPoint();
                setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            }

            @Override
            public void mouseDragged(MouseEvent e) {
                if (anchorPoint == null) return;
                int anchorX = anchorPoint.x;
                int anchorY = anchorPoint.y;
                java.awt.Point parentOnScreen = getParent().getLocationOnScreen();
                java.awt.Point mouseOnScreen = e.getLocationOnScreen();
                java.awt.Point position = new java.awt.Point(mouseOnScreen.x - parentOnScreen.x -
                        anchorX, mouseOnScreen.y - parentOnScreen.y - anchorY);
                setLocation(position);
                point.setX((position.getX() - getParent().getWidth() / 2.-centerOffsetX) / scaleCoefficient);
                point.setY((position.getY() - getParent().getHeight() / 2.) / scaleCoefficient * -1);
                getParent().repaint();
            }
        });
    }

    public void setScale(Double scaleCoefficient) {
        this.scaleCoefficient = scaleCoefficient;
    }

    public void setCenterOffsetX(int centerOffsetX) {
        this.centerOffsetX = centerOffsetX;
    }

    public void setCenterOffsetY(int centerOffsetY) {
        this.centerOffsetY = centerOffsetY;
    }
}
