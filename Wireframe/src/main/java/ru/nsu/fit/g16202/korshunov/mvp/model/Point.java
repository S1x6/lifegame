package ru.nsu.fit.g16202.korshunov.mvp.model;

public class Point {

    private double x;
    private double y;
    private double z;
    private double w;

    public Point(double x, double y, double z, double w) {
        this.x = x;
        this.y = y;
        this.z = z;
        this.w = w;
    }

    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }

    public double getZ() {
        return z;
    }

    public void setZ(double z) {
        this.z = z;
    }

    public Point getCopy() {
        return new Point(x, y, z, w);
    }

    public double getW() {
        return w;
    }

    public void setW(double w) {
        this.w = w;
    }

    public Point cross(Point v2) {
        return new Point(
                (y * v2.getZ()) - (z * v2.getY()),
                (z * v2.getX()) - (x * v2.getZ()),
                (x * v2.getY()) - (y * v2.getX()), 1
        );
    }

    public void print(String annotation) {
        System.out.println(annotation);
        System.out.println(x + " " + y + " " + z + " " + w);
        System.out.println();
    }

    public void normalize() {
        double coef = Math.sqrt(x * x + y * y + z * z);
        x /= coef;
        y /= coef;
        z /= coef;
    }

    public void add(Point point) {
        x += point.getX();
        y += point.getY();
        z += point.getZ();
    }
}
