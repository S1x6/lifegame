package ru.nsu.fit.g16202.korshunov.mvp.model;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;

public class Wireframe {

    private Spline spline;
    private Point referencePoint;
    private Matrix rotationMatrix;
   // private Matrix initRotationMatrix;
    private Matrix scaleMatrix = Matrix.getIdentityMatrix();
    private Matrix transformationMatrix;
    //    private Matrix initialMatrix;
    private List<List<Point>> points3D = new ArrayList<>();
    private List<List<Point>> transformedPoints3D = new ArrayList<>();
    private static List<Point> axisPoints = new ArrayList<>(4);

    static {
        axisPoints.add(new Point(0, 0, 0, 1));
        axisPoints.add(new Point(1, 0, 0, 1));
        axisPoints.add(new Point(0, -1, 0, 1));
        axisPoints.add(new Point(0, 0, 1, 1));
    }

    public Wireframe(Spline spline, Point referencePoint, Matrix rotationMatrix) {
        this.spline = spline;
        this.referencePoint = referencePoint;
     //   this.initRotationMatrix = rotationMatrix;
        this.rotationMatrix = rotationMatrix;
    }

    public void setColor(Color color) {
        spline.setColor(color);
    }

    public void applyMatrix() {
        transformedPoints3D.clear();
        for (List<Point> points :
                points3D) {
            List<Point> list = new ArrayList<>();
            for (Point p :
                    points) {
                Point newPoint = p.getCopy();
                Matrix.applyMatrixToPoint(newPoint, transformationMatrix);
                list.add(newPoint);
            }
            transformedPoints3D.add(list);
        }
    }

    public Color getColor() {
        return spline.getColor();
    }

    public Spline getSpline() {
        return spline;
    }

    public List<List<Point>> get3DPoints() {
        if (points3D.isEmpty()) {
            calculate3DPoints();
        }
       // points3D.get(0).get(0).print("Point in model coordinates");
        applyMatrix();
        return transformedPoints3D;
    }

    public void setPoints3D(List<List<Point>> points) {
        points3D = points;
    }

    private void clearPoints() {
        for (List<Point> points :
                points3D) {
            points.clear();
        }
        points3D.clear();
    }

    private void calculateTransformationMatrix() {
        transformationMatrix = scaleMatrix.multiply(rotationMatrix);
    }

    public List<List<Point>> calculate3DPoints() {
        clearPoints();
        Configuration c = Configuration.getInstance();
        double dU;
        double dV;
        for (int i = 0; i < (c.getN() - 1) * (c.getK() - 1) + c.getN(); i++) {
            dU = (double) i / ((c.getN() - 1) * (c.getK() - 1) + c.getN());
            double u = c.getA() * (1 - dU) + c.getB() * dU;
            Point uPoint = spline.getPointAt(u);
            List<Point> list = new ArrayList<>();
            for (int j = 0; j <= c.getK() * c.getM(); j++) {
                dV = (double) j / (c.getM() * c.getK());
                double v = Math.toRadians(c.getC()) * (1 - dV) + Math.toRadians(c.getD()) * dV;
                Point p = new Point(-uPoint.getY() * Math.cos(v),
                        -uPoint.getY() * Math.sin(v),
                        uPoint.getX(), 1);
                list.add(p);
            }
            points3D.add(list);
        }
        calculateTransformationMatrix();
        applyMatrix();
        return points3D;
    }

    public List<Point> getAxisPoints() {
        List<Point> points = new ArrayList<>(4);
        Point ref = axisPoints.get(0).getCopy();
        //ref.add(referencePoint);
        Point x = axisPoints.get(1).getCopy();
        Matrix.applyMatrixToPoint(x, rotationMatrix);
//        x.add(referencePoint);
        Point y = axisPoints.get(2).getCopy();
        Matrix.applyMatrixToPoint(y, rotationMatrix);
//        y.add(referencePoint);
        Point z = axisPoints.get(3).getCopy();
        Matrix.applyMatrixToPoint(z, rotationMatrix);
//        z.add(referencePoint);
        points.add(ref);
        points.add(x);
        points.add(y);
        points.add(z);
        return points;
    }

    public void rotate(int angle, Axis axis) {
        Matrix m;
        switch (axis) {
            case X_AXIS:
                m = Matrix.getXRotationMatrix(Math.toRadians(angle));
                break;
            case Y_AXIS:
                m = Matrix.getYRotationMatrix(Math.toRadians(angle));
                break;
            case Z_AXIS:
                m = Matrix.getZRotationMatrix(Math.toRadians(angle));
                break;
            default:
                return;
        }
        rotationMatrix = rotationMatrix.multiply(m);
        calculateTransformationMatrix();
    }

    public void move(double offset, Axis axis) {
        switch (axis) {
            case X_AXIS:
                referencePoint.setX(referencePoint.getX() + offset);
                break;
            case Y_AXIS:
                referencePoint.setY(referencePoint.getY() + offset);
                break;
            case Z_AXIS:
                referencePoint.setZ(referencePoint.getZ() + offset);
                break;
            default:
                return;
        }
    }

    public void scale(double ratio, Axis axis) {
        Matrix m;
        switch (axis) {
            case X_AXIS:
                m = Matrix.getXScaleMatrix(ratio);
                break;
            case Y_AXIS:
                m = Matrix.getYScaleMatrix(ratio);
                break;
            case Z_AXIS:
                m = Matrix.getZScaleMatrix(ratio);
                break;
            default:
                return;
        }
        scaleMatrix = scaleMatrix.multiply(m);
        calculateTransformationMatrix();
    }

    public Point getReferencePoint() {
        return referencePoint;
    }

    public void setReferencePoint(Point point) {
        referencePoint = point;
    }

    public Matrix getRotationMatrix() {
        return rotationMatrix;
    }

    public enum Axis {
        X_AXIS, Y_AXIS, Z_AXIS
    }
}
