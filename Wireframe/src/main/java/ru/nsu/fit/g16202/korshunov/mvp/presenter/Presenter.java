package ru.nsu.fit.g16202.korshunov.mvp.presenter;

import ru.nsu.fit.g16202.korshunov.mvp.model.*;
import ru.nsu.fit.g16202.korshunov.mvp.view.View;
import ru.nsu.fit.g16202.korshunov.ui.dialog.EditorDialog;

import javax.swing.*;
import javax.swing.filechooser.FileFilter;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;
import java.awt.event.MouseWheelEvent;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;

public class Presenter {

    private WorldPresenter worldPresenter;
    private View view;
    private EditorDialog editorDialog;
    private int selectedWireframeIndex;
    private List<Wireframe> wireframes;
    private int lastDraggedX = 0;
    private KeyListener keyListener = new KeyListener();
    private JFrame frame;

    public Presenter(JFrame frame) {
        view = new View();
        this.frame = frame;
        loadFromFile(new File("./Wireframe_NSU_g16202_Korshunov_Data/defaultConfig.txt"));
        editorDialog = new EditorDialog(frame);
        view.addMouseMotionListener(new MouseMotionAdapter() {
            @Override
            public void mouseDragged(MouseEvent e) {
                super.mouseDragged(e);
                int sign;
                double way;
                if (e.getX() - lastDraggedX > 0) {
                    sign = 1;
                    way = 0.95;
                } else {
                    sign = -1;
                    way = 1 / 0.95;
                }
                if (keyListener.isQPressed()) {
                    wireframes.get(selectedWireframeIndex).rotate(2 * sign, Wireframe.Axis.X_AXIS);
                }
                if (keyListener.isWPressed()) {
                    wireframes.get(selectedWireframeIndex).rotate(2 * sign, Wireframe.Axis.Y_AXIS);
                }
                if (keyListener.isEPressed()) {
                    wireframes.get(selectedWireframeIndex).rotate(2 * sign, Wireframe.Axis.Z_AXIS);
                }
                if (keyListener.isAPressed()) {
                    wireframes.get(selectedWireframeIndex).move(0.01 * sign, Wireframe.Axis.X_AXIS);
                }
                if (keyListener.isSPressed()) {
                    wireframes.get(selectedWireframeIndex).move(0.01 * sign, Wireframe.Axis.Y_AXIS);
                }
                if (keyListener.isDPressed()) {
                    wireframes.get(selectedWireframeIndex).move(0.01 * sign, Wireframe.Axis.Z_AXIS);
                }
                if (keyListener.isZPressed()) {
                    wireframes.get(selectedWireframeIndex).scale(way, Wireframe.Axis.X_AXIS);
                }
                if (keyListener.isXPressed()) {
                    wireframes.get(selectedWireframeIndex).scale(way, Wireframe.Axis.Y_AXIS);
                }
                if (keyListener.isCPressed()) {
                    wireframes.get(selectedWireframeIndex).scale(way, Wireframe.Axis.Z_AXIS);
//                    wireframes.get(0).getReferencePoint().print("ref 0");
                }
                lastDraggedX = e.getX();
                worldPresenter.revalidate();
                view.repaint();
            }
        });
//        view.setWireframes(worldPresenter.getWorldWireframes());
    }

    public View getViewPort() {
        return view;
    }

    public void openNewScene() {
        JFileChooser f = new JFileChooser();
        f.setCurrentDirectory(new File("./Wireframe_NSU_g16202_Korshunov_Data"));
        f.setFileFilter(new FileNameExtensionFilter("Файл сцены", "txt"));
        f.showDialog(frame, "Открыть сцену");
        File file = f.getSelectedFile();
        if (file != null) {
            loadFromFile(file);
        }
        view.resetCamera();
    }

    public void saveSceneToFile() {
        final JFileChooser fc = new JFileChooser();
        FileFilter filter = new FileNameExtensionFilter("Сохранить сцену", "txt");
        fc.setFileFilter(filter);
        File dataDir = new File("./Wireframe_NSU_g16202_Korshunov_Data");
        fc.setCurrentDirectory(dataDir);
        int ret = fc.showDialog(frame, "Сохранить модель");
        if (ret == JFileChooser.APPROVE_OPTION) {
            File file = fc.getSelectedFile();
            if (!file.toPath().endsWith(".txt")) {
                file = new File(file.toPath().toString() + ".txt");
            }
            System.out.println(file.toPath());
            try {
                Files.deleteIfExists(file.toPath());
                file.createNewFile();
                FileWriter fw = new FileWriter(file);
                Configuration c = Configuration.getInstance();
                fw.write(c.getN()+ " " + c.getM() + " " + c.getK() + " " + c.getA() + " " + c.getB() + " " + c.getC() + " " + c.getD() + "\n");
                fw.write(c.getZn() + " " + c.getZn() + " " + c.getSw() + " " + c.getSh()+ "\n");
                fw.write(1 + " " + 1 + " " + 1 + " " + "\n");
                fw.write(1 + " " + 1 + " " + 1 + " " + "\n");
                fw.write(1 + " " + 1 + " " + 1 + " " + "\n");
                fw.write(c.getBackgroundColor().getRed() + " " + c.getBackgroundColor().getGreen()+ " " + c.getBackgroundColor().getBlue()+ "\n");
                fw.write(wireframes.size()+ "\n");
                for (Wireframe w :
                        wireframes) {
                    fw.write(w.getColor().getRed() + " " + w.getColor().getGreen() + " " + w.getColor().getBlue()+ "\n");
                    fw.write(w.getReferencePoint().getX() + " " + w.getReferencePoint().getY()+ " " + w.getReferencePoint().getZ()+ "\n");
                    fw.write(w.getRotationMatrix().getElement(0,0)+" "+w.getRotationMatrix().getElement(0,1)+
                            " "+w.getRotationMatrix().getElement(0,2)+" "+ "\n");
                    fw.write(w.getRotationMatrix().getElement(1,0)+" "+w.getRotationMatrix().getElement(1,1)+
                            " "+w.getRotationMatrix().getElement(1,2)+" "+ "\n");
                    fw.write(w.getRotationMatrix().getElement(2,0)+" "+w.getRotationMatrix().getElement(2,1)+
                            " "+w.getRotationMatrix().getElement(2,2)+" "+ "\n");
                    fw.write(w.getSpline().getReferencePoints().size()+ "\n");
                    for (Point p :
                            w.getSpline().getReferencePoints()) {
                        fw.write(p.getX() + " " + p.getY() + " " + p.getZ()+ "\n");
                    }
                }
                fw.flush();
                fw.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }


    public void openSettings() {
        List<Spline> splines = new ArrayList<>(wireframes.size());
        for (Wireframe wireframe :
                wireframes) {
            splines.add(wireframe.getSpline());
        }
        editorDialog.open(splines, selectedWireframeIndex);
        for (Wireframe wireframe :
                wireframes) {
            wireframe.calculate3DPoints();
        }
        view.repaint();
    }

    public void createNewSpline() {
        Spline s = new Spline();
        s.addReferencePoint(new Point(-2, 1, 0, 1));
        s.addReferencePoint(new Point(-1, 1, 0, 1));
        s.addReferencePoint(new Point(0, 1, 0, 1));
        s.addReferencePoint(new Point(1, 1, 0, 1));
        Wireframe w = new Wireframe(s, new Point(0, 0, 0, 1), Matrix.getIdentityMatrix());
        wireframes.add(w);
        selectedWireframeIndex = wireframes.size() - 1;
        wireframes.get(selectedWireframeIndex).getSpline().getSplineCurve();
        view.setSelectedIndex(selectedWireframeIndex);
        openSettings();
        worldPresenter.revalidate();
        view.resetCamera();
        view.repaint();
    }

    public void selectPrevious() {
        selectedWireframeIndex -= 1;
        if (selectedWireframeIndex < 0) {
            selectedWireframeIndex += wireframes.size();
        }
        view.setSelectedIndex(selectedWireframeIndex);
        view.repaint();
    }

    public void selectNext() {
        selectedWireframeIndex = (selectedWireframeIndex + 1) % wireframes.size();
        view.setSelectedIndex(selectedWireframeIndex);
        view.repaint();
    }

    public void deleteSpline() {
        if (wireframes.size() == 1) return;
        if (selectedWireframeIndex != -1) {
            wireframes.remove(selectedWireframeIndex);
            if (selectedWireframeIndex == wireframes.size()) {
                selectPrevious();
            }
        }
        worldPresenter.revalidate();
        view.resetCamera();
    }

    public void init() {
        view.resetCamera();
    }

    private void loadFromFile(File file) {
        List<Wireframe> wireframes = Configuration.loadConfiguration(file);
        if (wireframes == null) {
            JOptionPane.showMessageDialog(view,
                    "Не удалось открыть файл конфигурации \"./Wireframe_NSU_g16202_Korshunov_Data/defaultConfig.txt\"");
            System.exit(0);
        }
        this.wireframes = wireframes;
        selectedWireframeIndex = 0;
        for (Wireframe wireframe :
                wireframes) {
            wireframe.getSpline().getSplineCurve();
            //wireframe.setReferencePoint(new Point(200, 80, 0));
        }
        view.setWireframes(wireframes);
        worldPresenter = new WorldPresenter(wireframes);
        view.repaint();
    }
}
