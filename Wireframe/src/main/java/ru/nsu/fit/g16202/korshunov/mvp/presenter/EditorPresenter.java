package ru.nsu.fit.g16202.korshunov.mvp.presenter;

import ru.nsu.fit.g16202.korshunov.mvp.view.EditorView;

public class EditorPresenter {

    private EditorView view;

    public EditorPresenter() {
        view = new EditorView();
    }

    public EditorView getView() {
        return view;
    }
}
