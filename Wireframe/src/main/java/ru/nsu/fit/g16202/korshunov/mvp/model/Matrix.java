package ru.nsu.fit.g16202.korshunov.mvp.model;

public class Matrix {

    private double[][] elements;

    public Matrix(double[][] elements) {
        this.elements = elements;
    }

    public static Matrix getIdentityMatrix() {
        return new Matrix(new double[][]{{1, 0, 0, 0}, {0, 1, 0, 0}, {0, 0, 1, 0}, {0, 0, 0, 1}});
    }

    public static Matrix getXRotationMatrix(double angle) {
        return new Matrix(new double[][]{
                {1, 0, 0, 0},
                {0, Math.cos(angle), -Math.sin(angle), 0},
                {0, Math.sin(angle), Math.cos(angle), 0},
                {0, 0, 0, 1}
        });
    }

    public static Matrix getYRotationMatrix(double angle) {
        return new Matrix(new double[][]{
                {Math.cos(angle), 0, Math.sin(angle), 0},
                {0, 1, 0, 0},
                {-Math.sin(angle), 0, Math.cos(angle), 0},
                {0, 0, 0, 1}
        });
    }

    public static Matrix getZRotationMatrix(double angle) {
        return new Matrix(new double[][]{
                {Math.cos(angle), -Math.sin(angle), 0, 0},
                {Math.sin(angle), Math.cos(angle), 0, 0},
                {0, 0, 1, 0},
                {0, 0, 0, 1}
        });
    }

    public static Matrix getXTranslationMatrix(double offset) {
        return new Matrix(new double[][]{
                {1, 0, 0, 0},
                {0, 1, 0, 0},
                {0, 0, 1, 0},
                {offset, 0, 0, 1}
        });
    }

    public static Matrix getYTranslationMatrix(double offset) {
        return new Matrix(new double[][]{
                {1, 0, 0, 0},
                {0, 1, 0, 0},
                {0, 0, 1, 0},
                {0, offset, 0, 1}
        });
    }

    public static Matrix getZTranslationMatrix(double offset) {
        return new Matrix(new double[][]{
                {1, 0, 0, 0},
                {0, 1, 0, 0},
                {0, 0, 1, 0},
                {0, 0, offset, 1}
        });
    }

    public static Matrix getXScaleMatrix(double ratio) {
        return new Matrix(new double[][]{
                {ratio, 0, 0, 0},
                {0, 1, 0, 0},
                {0, 0, 1, 0},
                {0, 0, 0, 1}
        });
    }

    public static Matrix getYScaleMatrix(double ratio) {
        return new Matrix(new double[][]{
                {1, 0, 0, 0},
                {0, ratio, 0, 0},
                {0, 0, 1, 0},
                {0, 0, 0, 1}
        });
    }

    public static Matrix getZScaleMatrix(double ratio) {
        return new Matrix(new double[][]{
                {1, 0, 0, 0},
                {0, 1, 0, 0},
                {0, 0, ratio, 0},
                {0, 0, 0, 1}
        });
    }

    public static void applyMatrixToPoint(Point point, Matrix matrix) {
        Matrix p = new Matrix(new double[][]{
                {point.getX(), point.getY(), point.getZ(), point.getW()},
                {0, 0, 0, 0},
                {0, 0, 0, 0},
                {0, 0, 0, 0}
        });
        p = p.multiply(matrix);
        point.setX(p.getElement(0, 0));
        point.setY(p.getElement(0, 1));
        point.setZ(p.getElement(0, 2));
        point.setW(p.getElement(0, 3));
    }

    public void setElements(double[][] elements) {
        this.elements = elements;
    }

    public Matrix multiply(Matrix b) {
        double[][] mResult = new double[4][4];
        for (int i = 0; i < 4; i++) {         // rows from m1
            for (int j = 0; j < 4; j++) {     // columns from m2
                for (int k = 0; k < 4; k++) { // columns from m1
                    mResult[i][j] += elements[i][k] * b.elements[k][j];
                }
            }
        }
        return new Matrix(mResult);
    }

    public double getElement(int i, int j) {
        return elements[i][j];
    }


    public void setElement(double value, int i, int j) {
        elements[i][j] = value;
    }

    public void print(String annotation) {
        System.out.println(annotation);
        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 4; j++) {
                System.out.print(elements[i][j] + " ");
            }
            System.out.println();
        }
        System.out.println();
    }
}
