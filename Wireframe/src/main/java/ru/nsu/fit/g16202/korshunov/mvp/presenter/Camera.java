package ru.nsu.fit.g16202.korshunov.mvp.presenter;

import ru.nsu.fit.g16202.korshunov.mvp.model.Configuration;
import ru.nsu.fit.g16202.korshunov.mvp.model.Matrix;
import ru.nsu.fit.g16202.korshunov.mvp.model.Point;
import ru.nsu.fit.g16202.korshunov.mvp.model.Wireframe;
import sun.awt.ConstrainableGraphics;
import sun.security.krb5.Config;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.List;

@SuppressWarnings("Duplicates")
public class Camera {

    private Point position = new Point(-10, 0, 0, 1);
    private Point viewPoint = new Point(0, 0, 0, 1);
    private Point upVector = new Point(0, 1, 0, 0);
    private WorldPresenter worldPresenter;
    private double zoom = 1.;
    private Matrix lookAtMatrix = Matrix.getIdentityMatrix();
    private Matrix projectionMatrix = Matrix.getIdentityMatrix();
    private Dimension viewPortSize = new Dimension();

    private Matrix cameraRotationMatrix = Matrix.getIdentityMatrix();

    public Camera(WorldPresenter worldPresenter) {
        this.worldPresenter = worldPresenter;
    }

    public void setWorldView(BufferedImage image, int selectedIndex) {
        Graphics2D g = image.createGraphics();
        g.setColor(Configuration.getInstance().getBackgroundColor());
        g.fillRect(0, 0, image.getWidth(), image.getHeight());
//
        for (int ind = 0; ind < worldPresenter.wireframesCount(); ind++) {
            java.util.List<java.util.List<Point>> lists1 = worldPresenter.getObjectInWorld(ind);
            List<List<Point>> lists = applyLookAtMatrix(lists1);
            if (selectedIndex == ind) {
                g.setColor(Color.gray);
                //System.out.println(lists.get(0).get(0).getW());
            } else {
                g.setColor(worldPresenter.getWorldWireframes().get(ind).getColor());
            }
            Configuration c = Configuration.getInstance();
            for (int j = 0; j < lists.size(); j++) {
                if (j % Configuration.getInstance().getK() != 0) continue;
                List<Point> points = lists.get(j);
                int i;
                for (i = 0; i < points.size() - 1; i++) {
                    Point p = points.get(i);
                    Point p1 = points.get(i + 1);
                    if (shouldClip(p) || shouldClip(p1)) {
                        continue;
                    }
                    g.drawLine(
                            (int) Math.round((p.getX() + 1) * (viewPortSize.width / 2.)),
                            (int) Math.round((-p.getY() + 1) * (viewPortSize.height / 2.)),
                            (int) Math.round((p1.getX() + 1) * (viewPortSize.width / 2.)),
                            (int) Math.round((-p1.getY() + 1) * (viewPortSize.height / 2.)));
                }
            }

            for (int j = 0; j < lists.get(0).size(); j++) {
                if (j % Configuration.getInstance().getK() != 0) continue;
                for (int i = 0; i < lists.size() - 1; i++) {
                    Point p = lists.get(i).get(j);
                    Point p1 = lists.get(i + 1).get(j);
                    if (shouldClip(p) || shouldClip(p1)) {
                        continue;
                    }
                    g.drawLine(
                            (int) Math.round((p.getX() + 1) * (viewPortSize.width / 2.)),
                            (int) Math.round((-p.getY() + 1) * (viewPortSize.height / 2.)),
                            (int) Math.round((p1.getX() + 1) * (viewPortSize.width / 2.)),
                            (int) Math.round((-p1.getY() + 1) * (viewPortSize.height / 2.)));
                }
            }
        }
        //
        // draw axises
        drawWorldAxis(g);
        drawWorldBounds(g);
        drawObjectAxis(g);
    }

    private boolean shouldClip(Point p) {
        Configuration c = Configuration.getInstance();
        return (-p.getW() - c.getZn()) / (c.getZf() - c.getZn()) > 1 ||
                (-p.getW() - c.getZn()) / (c.getZf() - c.getZn()) < -1 ||
                p.getX() > 1 ||
                p.getX() < -1 ||
                p.getY() > 1 ||
                p.getY() < -1;
    }

    private void drawWorldBounds(Graphics2D g) {
        Point luu = new Point(1, 1, 1, 1);
        Point ruu = new Point(-1, 1, 1, 1);
        Point ldu = new Point(1, 1, -1, 1);
        Point rdu = new Point(-1, 1, -1, 1);
        Point lud = new Point(1, -1, 1, 1);
        Point rud = new Point(-1, -1, 1, 1);
        Point ldd = new Point(1, -1, -1, 1);
        Point rdd = new Point(-1, -1, -1, 1);
//        Matrix i =Matrix.getIdentityMatrix();
        Matrix div = new Matrix(new double[][]{
                {1, 0, 0, 0},
                {0, 1, 0, 0},
                {0, 0, 1, 1. / Configuration.getInstance().getZn()},
                {0, 0, 0, 0}
        });
        applyAllMatrices(luu, lookAtMatrix, div, projectionMatrix);
        applyAllMatrices(ruu, lookAtMatrix, div, projectionMatrix);
        applyAllMatrices(ldu, lookAtMatrix, div, projectionMatrix);
        applyAllMatrices(rdu, lookAtMatrix, div, projectionMatrix);
        applyAllMatrices(lud, lookAtMatrix, div, projectionMatrix);
        applyAllMatrices(rud, lookAtMatrix, div, projectionMatrix);
        applyAllMatrices(ldd, lookAtMatrix, div, projectionMatrix);
        applyAllMatrices(rdd, lookAtMatrix, div, projectionMatrix);
        g.setStroke(new BasicStroke(2));
        g.setColor(Color.BLACK);
        // top
        if (!shouldClip(luu) && !shouldClip(ruu)) {
            g.drawLine(
                    (int) Math.round((luu.getX() + 1) * (viewPortSize.width / 2.)),
                    (int) Math.round((-luu.getY() + 1) * (viewPortSize.height / 2.)),
                    (int) Math.round((ruu.getX() + 1) * (viewPortSize.width / 2.)),
                    (int) Math.round((-ruu.getY() + 1) * (viewPortSize.height / 2.)));
        }
        if (!shouldClip(ruu) && !shouldClip(rdu)) {
            g.drawLine(
                    (int) Math.round((ruu.getX() + 1) * (viewPortSize.width / 2.)),
                    (int) Math.round((-ruu.getY() + 1) * (viewPortSize.height / 2.)),
                    (int) Math.round((rdu.getX() + 1) * (viewPortSize.width / 2.)),
                    (int) Math.round((-rdu.getY() + 1) * (viewPortSize.height / 2.)));
        }
        if (!shouldClip(rdu) && !shouldClip(ldu)) {
            g.drawLine(
                    (int) Math.round((rdu.getX() + 1) * (viewPortSize.width / 2.)),
                    (int) Math.round((-rdu.getY() + 1) * (viewPortSize.height / 2.)),
                    (int) Math.round((ldu.getX() + 1) * (viewPortSize.width / 2.)),
                    (int) Math.round((-ldu.getY() + 1) * (viewPortSize.height / 2.)));
        }
        if (!shouldClip(ldu) && !shouldClip(luu)) {
            g.drawLine(
                    (int) Math.round((ldu.getX() + 1) * (viewPortSize.width / 2.)),
                    (int) Math.round((-ldu.getY() + 1) * (viewPortSize.height / 2.)),
                    (int) Math.round((luu.getX() + 1) * (viewPortSize.width / 2.)),
                    (int) Math.round((-luu.getY() + 1) * (viewPortSize.height / 2.)));
        }
        // bottom
        if (!shouldClip(lud) && !shouldClip(rud)) {
            g.drawLine(
                    (int) Math.round((lud.getX() + 1) * (viewPortSize.width / 2.)),
                    (int) Math.round((-lud.getY() + 1) * (viewPortSize.height / 2.)),
                    (int) Math.round((rud.getX() + 1) * (viewPortSize.width / 2.)),
                    (int) Math.round((-rud.getY() + 1) * (viewPortSize.height / 2.)));
        }
        if (!shouldClip(rud) && !shouldClip(rdd)) {
            g.drawLine(
                    (int) Math.round((rud.getX() + 1) * (viewPortSize.width / 2.)),
                    (int) Math.round((-rud.getY() + 1) * (viewPortSize.height / 2.)),
                    (int) Math.round((rdd.getX() + 1) * (viewPortSize.width / 2.)),
                    (int) Math.round((-rdd.getY() + 1) * (viewPortSize.height / 2.)));

        }
        if (!shouldClip(rdd) && !shouldClip(ldd)) {
            g.drawLine(
                    (int) Math.round((rdd.getX() + 1) * (viewPortSize.width / 2.)),
                    (int) Math.round((-rdd.getY() + 1) * (viewPortSize.height / 2.)),
                    (int) Math.round((ldd.getX() + 1) * (viewPortSize.width / 2.)),
                    (int) Math.round((-ldd.getY() + 1) * (viewPortSize.height / 2.)));
        }
        if (!shouldClip(ldd) && !shouldClip(lud)) {
            g.drawLine(
                    (int) Math.round((ldd.getX() + 1) * (viewPortSize.width / 2.)),
                    (int) Math.round((-ldd.getY() + 1) * (viewPortSize.height / 2.)),
                    (int) Math.round((lud.getX() + 1) * (viewPortSize.width / 2.)),
                    (int) Math.round((-lud.getY() + 1) * (viewPortSize.height / 2.)));
        }
        // sides
        if (!shouldClip(luu) && !shouldClip(lud)) {
            g.drawLine(
                    (int) Math.round((luu.getX() + 1) * (viewPortSize.width / 2.)),
                    (int) Math.round((-luu.getY() + 1) * (viewPortSize.height / 2.)),
                    (int) Math.round((lud.getX() + 1) * (viewPortSize.width / 2.)),
                    (int) Math.round((-lud.getY() + 1) * (viewPortSize.height / 2.)));
        }
        if (!shouldClip(ruu) && !shouldClip(rud)) {
            g.drawLine(
                    (int) Math.round((ruu.getX() + 1) * (viewPortSize.width / 2.)),
                    (int) Math.round((-ruu.getY() + 1) * (viewPortSize.height / 2.)),
                    (int) Math.round((rud.getX() + 1) * (viewPortSize.width / 2.)),
                    (int) Math.round((-rud.getY() + 1) * (viewPortSize.height / 2.)));
        }
        if (!shouldClip(ldu) && !shouldClip(ldd)) {
            g.drawLine(
                    (int) Math.round((ldu.getX() + 1) * (viewPortSize.width / 2.)),
                    (int) Math.round((-ldu.getY() + 1) * (viewPortSize.height / 2.)),
                    (int) Math.round((ldd.getX() + 1) * (viewPortSize.width / 2.)),
                    (int) Math.round((-ldd.getY() + 1) * (viewPortSize.height / 2.)));
        }
        if (!shouldClip(rdu) && !shouldClip(rdd)) {
            g.drawLine(
                    (int) Math.round((rdu.getX() + 1) * (viewPortSize.width / 2.)),
                    (int) Math.round((-rdu.getY() + 1) * (viewPortSize.height / 2.)),
                    (int) Math.round((rdd.getX() + 1) * (viewPortSize.width / 2.)),
                    (int) Math.round((-rdd.getY() + 1) * (viewPortSize.height / 2.)));
        }
    }

    private void drawWorldAxis(Graphics2D g) {
        Point center = new Point(0, 0, 0, 1);
        Point pointX = new Point(0.5, 0, 0, 1);
        Point pointY = new Point(0, -0.5, 0, 1);
        Point pointZ = new Point(0, 0, 0.5, 1);
        Matrix div = new Matrix(new double[][]{
                {1, 0, 0, 0},
                {0, 1, 0, 0},
                {0, 0, 1, 1. / Configuration.getInstance().getZn()},
                {0, 0, 0, 0}
        });
        //Matrix i =Matrix.getIdentityMatrix();
        applyAllMatrices(center, lookAtMatrix, div, projectionMatrix);
        applyAllMatrices(pointX, lookAtMatrix, div, projectionMatrix);
        applyAllMatrices(pointY, lookAtMatrix, div, projectionMatrix);
        applyAllMatrices(pointZ, lookAtMatrix, div, projectionMatrix);
        g.setStroke(new BasicStroke(2));
        g.setColor(Color.RED);
        g.drawLine(
                (int) Math.round((center.getX() + 1) * (viewPortSize.width / 2.)),
                (int) Math.round((-center.getY() + 1) * (viewPortSize.height / 2.)),
                (int) Math.round((pointX.getX() + 1) * (viewPortSize.width / 2.)),
                (int) Math.round((-pointX.getY() + 1) * (viewPortSize.height / 2.)));
        g.setColor(Color.GREEN);
        g.drawLine(
                (int) Math.round((center.getX() + 1) * (viewPortSize.width / 2.)),
                (int) Math.round((-center.getY() + 1) * (viewPortSize.height / 2.)),
                (int) Math.round((pointY.getX() + 1) * (viewPortSize.width / 2.)),
                (int) Math.round((-pointY.getY() + 1) * (viewPortSize.height / 2.)));
        g.setColor(Color.BLUE);
        g.drawLine(
                (int) Math.round((center.getX() + 1) * (viewPortSize.width / 2.)),
                (int) Math.round((-center.getY() + 1) * (viewPortSize.height / 2.)),
                (int) Math.round((pointZ.getX() + 1) * (viewPortSize.width / 2.)),
                (int) Math.round((-pointZ.getY() + 1) * (viewPortSize.height / 2.)));
    }

    private void drawObjectAxis(Graphics2D g) {
        Matrix div = new Matrix(new double[][]{
                {1, 0, 0, 0},
                {0, 1, 0, 0},
                {0, 0, 1, 1. / Configuration.getInstance().getZn()},
                {0, 0, 0, 0}
        });
        List<Wireframe> worldWireframes = worldPresenter.getWorldWireframes();
//        worldPresenter.getObjectAxis(0).get(0).print("ref 0");
        for (int i = 0; i < worldWireframes.size(); i++) {
            List<Point> points = worldPresenter.getObjectAxis(i);
            applyAllMatrices(points.get(0), lookAtMatrix, div, projectionMatrix);
            applyAllMatrices(points.get(1), lookAtMatrix, div, projectionMatrix);
            applyAllMatrices(points.get(2), lookAtMatrix, div, projectionMatrix);
            applyAllMatrices(points.get(3), lookAtMatrix, div, projectionMatrix);
            g.setColor(Color.RED);
            if (i == 0)
                points.get(0).print("ref 0");
            g.drawLine(
                    (int) Math.round((points.get(0).getX() + 1) * (viewPortSize.width / 2.)),
                    (int) Math.round((-points.get(0).getY() + 1) * (viewPortSize.height / 2.)),
                    (int) Math.round((points.get(1).getX() + 1) * (viewPortSize.width / 2.)),
                    (int) Math.round((-points.get(1).getY() + 1) * (viewPortSize.height / 2.)));
            g.setColor(Color.GREEN);
            g.drawLine(
                    (int) Math.round((points.get(0).getX() + 1) * (viewPortSize.width / 2.)),
                    (int) Math.round((-points.get(0).getY() + 1) * (viewPortSize.height / 2.)),
                    (int) Math.round((points.get(2).getX() + 1) * (viewPortSize.width / 2.)),
                    (int) Math.round((-points.get(2).getY() + 1) * (viewPortSize.height / 2.)));
            g.setColor(Color.BLUE);
            g.drawLine(
                    (int) Math.round((points.get(0).getX() + 1) * (viewPortSize.width / 2.)),
                    (int) Math.round((-points.get(0).getY() + 1) * (viewPortSize.height / 2.)),
                    (int) Math.round((points.get(3).getX() + 1) * (viewPortSize.width / 2.)),
                    (int) Math.round((-points.get(3).getY() + 1) * (viewPortSize.height / 2.)));
        }
    }

    public Dimension getViewPortSize(int viewWidth, int viewHeight) {
        Configuration c = Configuration.getInstance();
        double scale = Math.min(viewWidth / c.getSw(), viewHeight / c.getSh());
        viewPortSize.width = (int) (c.getSw() * scale);
        viewPortSize.height = (int) (c.getSh() * scale);
        return viewPortSize;
    }

    private void calculateLookAtMatrix() {
        Point cameraDirection = new Point(position.getX() - viewPoint.getX(), position.getY() - viewPoint.getY(), position.getZ() - viewPoint.getZ(), 1);
        cameraDirection.normalize();
        //cameraDirection.print("Camera direction");
        Point cameraRight = upVector.cross(cameraDirection);
        cameraRight.normalize();
//        cameraRight.print("Camera right");
        Point cameraUp = cameraDirection.cross(cameraRight);
//        cameraUp.print("Camera up");
        Matrix.applyMatrixToPoint(cameraDirection, cameraRotationMatrix);
        Matrix.applyMatrixToPoint(cameraUp, cameraRotationMatrix);
        Matrix.applyMatrixToPoint(cameraRight, cameraRotationMatrix);
        Matrix m = new Matrix(new double[][]{
                {cameraRight.getX(), cameraUp.getX(), cameraDirection.getX(), 0},
                {cameraRight.getY(), cameraUp.getY(), cameraDirection.getY(), 0},
                {cameraRight.getZ(), cameraUp.getZ(), cameraDirection.getZ(), 0},
                {0, 0, 0, 1}
        });
        Matrix m1 = new Matrix(new double[][]{
                {1, 0, 0, 0},
                {0, 1, 0, 0},
                {0, 0, 1, 0},
                {-position.getX(), -position.getY(), -position.getZ(), 1}});
        lookAtMatrix = m1.multiply(m);
    }

    private void calculateProjectionMatrix() {
        Configuration c = Configuration.getInstance();
        projectionMatrix = new Matrix(new double[][]{
                {2. * c.getZn() / c.getSw() / zoom, 0, 0, 0},
                {0, 2. * c.getZn() / c.getSh() / zoom, 0, 0},
                {0, 0, c.getZf() / (c.getZf() - c.getZn()), 1},
                {0, 0, -c.getZn() * c.getZf() / (c.getZf() - c.getZn()), 0}
        });
    }

    private List<List<Point>> applyLookAtMatrix(List<List<Point>> lists) {
        List<List<Point>> newLists = new ArrayList<>();
        calculateLookAtMatrix();
        calculateProjectionMatrix();
        Matrix div = new Matrix(new double[][]{
                {1, 0, 0, 0},
                {0, 1, 0, 0},
                {0, 0, 1, 1. / Configuration.getInstance().getZn()},
                {0, 0, 0, 0}
        });
        for (int i = 0; i < lists.size(); i++) {
            List<Point> list = lists.get(i);
            List<Point> newList = new ArrayList<>();
            for (int i1 = 0; i1 < list.size(); i1++) {
                Point p = list.get(i1).getCopy();
//                Point p = point.getCopy();
                applyAllMatrices(p, lookAtMatrix, div, projectionMatrix);
                newList.add(p);
            }
            newLists.add(newList);
        }
        return newLists;
    }

    // from scaled world to the end
    public void applyAllMatrices(Point p, Matrix lookAt, Matrix div, Matrix proj) {
        Matrix.applyMatrixToPoint(p, lookAt);
        Matrix.applyMatrixToPoint(p, div);
//        p.setZ(p.getZ() / p.getW());
//        p.setW(1);
        p.setX(p.getX() / p.getW());
        p.setY(p.getY() / p.getW());
        Matrix.applyMatrixToPoint(p, proj);
    }

    public Point getViewPoint() {
        return viewPoint;
    }

    public void setViewPoint(Point p) {
        viewPoint = p;
    }

    public Point getUpVector() {
        return upVector;
    }

    public void move(double offset, Wireframe.Axis axis) {
        switch (axis) {
            case X_AXIS:
                position.setX(position.getX() + offset);
                break;
            case Y_AXIS:
                position.setY(position.getY() + offset);
                break;
            case Z_AXIS:
                position.setZ(position.getZ() + offset);
                break;
        }
    }

    public void rotate(double angle, Wireframe.Axis axis) {
        switch (axis) {
            case X_AXIS:
                cameraRotationMatrix = cameraRotationMatrix.multiply(Matrix.getXRotationMatrix(Math.toRadians(angle)));
                break;
            case Y_AXIS:
                cameraRotationMatrix = cameraRotationMatrix.multiply(Matrix.getYRotationMatrix(Math.toRadians(angle)));
                break;
            case Z_AXIS:
                cameraRotationMatrix = cameraRotationMatrix.multiply(Matrix.getZRotationMatrix(Math.toRadians(angle)));
                break;
        }
    }


    public void setPosition(Point pos) {
        position = pos;
    }

    public void setRotationMatrix(Matrix matrix) {
        cameraRotationMatrix = matrix;
    }

    public void zoomOut(double v) {
        zoom /= v;
        System.out.println("zoom out " + zoom);
    }

    public void zoomIn(double v) {
        zoom *= v;
        System.out.println("zoom in " + zoom);
    }
}
