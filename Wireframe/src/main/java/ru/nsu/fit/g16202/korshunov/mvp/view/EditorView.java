package ru.nsu.fit.g16202.korshunov.mvp.view;

import ru.nsu.fit.g16202.korshunov.mvp.model.Configuration;
import ru.nsu.fit.g16202.korshunov.mvp.model.Point;
import ru.nsu.fit.g16202.korshunov.mvp.model.Spline;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;

public class EditorView extends JPanel {

    private Spline spline;
    private List<ReferencePoint> points = new ArrayList<>();
    private double scaleCoefficient;
    private boolean deleteMode;
    private boolean addMode;
    private int centerOffsetX;

    public EditorView() {
        setPreferredSize(new Dimension(450, 450));
        setLayout(null);
        addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                super.mouseClicked(e);
                if (!addMode) return;
                Point point = new Point((e.getX() - getWidth() / 2. - centerOffsetX) / scaleCoefficient,
                        (e.getY() - getHeight() / 2.) / scaleCoefficient * -1, 0,1);
                ReferencePoint p = new ReferencePoint(point, scaleCoefficient);
                p.setCenterOffsetX(centerOffsetX);
                points.add(p);
                p.setSize(new Dimension(11, 11));
                p.setLocation(e.getX(), e.getY());
                spline.getReferencePoints().add(point);
                p.addMouseListener(new MouseAdapter() {
                    @Override
                    public void mouseClicked(MouseEvent e) {
                        super.mouseClicked(e);
                        if (deleteMode && spline.getReferencePoints().size() > 4) {
                            spline.getReferencePoints().remove(point);
                            points.remove(p);
                            remove(p);
                        }
                        repaint();
                    }
                });
                add(p);
                repaint();
            }
        });

    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        g.setColor(Color.BLACK);
        g.fillRect(0, 0, getWidth(), getHeight());
        g.setColor(Color.WHITE);
        int offsetX = getWidth() / 2;
        int offsetY = getHeight() / 2;
        g.drawLine(0, offsetY, getWidth() - 1, offsetY);
        drawShortLines(g);
        if (spline != null) {
            g.setColor(Color.red);
            g.drawOval(points.get(0).getX(),
                    points.get(0).getY(),
                    11, 11);
            for (int i = 0; i < points.size() - 1; i++) {
                g.drawLine((int) (spline.getReferencePoints().get(i).getX() * scaleCoefficient) + offsetX + 6 + centerOffsetX,
                        (int) (-1 * spline.getReferencePoints().get(i).getY() * scaleCoefficient) + offsetY + 6,
                        (int) (spline.getReferencePoints().get(i + 1).getX() * scaleCoefficient) + offsetX + 6 + centerOffsetX,
                        (int) (-1 * spline.getReferencePoints().get(i + 1).getY() * scaleCoefficient) + offsetY + 6);
                g.drawOval(points.get(i + 1).getX(),
                        points.get(i + 1).getY(),
                        11, 11);
            }
            paintSplineLines(g);
        }
    }

    private void paintSplineLines(Graphics g) {
        int offsetX = getWidth() / 2;
        int offsetY = getHeight() / 2;
        g.setColor(Color.gray);
        ((Graphics2D) g).setStroke(new BasicStroke(2));
        List<Point> points = spline.getSplineCurve();
        Point point;
        Point pointNext;
        int firstGray = (int) (points.size() * Configuration.getInstance().getA());
        int secondGray = (int) (points.size() * (1 - Configuration.getInstance().getB()));
        for (int i = 0; i < firstGray - 1; i++) {
            point = points.get(i);
            pointNext = points.get(i + 1);
            g.drawLine((int) (point.getX() * scaleCoefficient) + offsetX + centerOffsetX,
                    (int) (-1 * point.getY() * scaleCoefficient) + offsetY,
                    (int) (pointNext.getX() * scaleCoefficient) + offsetX + centerOffsetX,
                    (int) (-1 * pointNext.getY() * scaleCoefficient) + offsetY);
        }
        g.setColor(Color.cyan);
        for (int i = firstGray - 1 < 0 ? 0 : firstGray - 1; i < points.size() - secondGray - 1; i++) {
            point = points.get(i);
            pointNext = points.get(i + 1);
            g.drawLine((int) (point.getX() * scaleCoefficient) + offsetX + centerOffsetX,
                    (int) (-1 * point.getY() * scaleCoefficient) + offsetY,
                    (int) (pointNext.getX() * scaleCoefficient) + offsetX + centerOffsetX,
                    (int) (-1 * pointNext.getY() * scaleCoefficient) + offsetY);
        }
        g.setColor(Color.gray);
        for (int i = points.size() - secondGray - 1; i < points.size() - 1; i++) {
            point = points.get(i);
            pointNext = points.get(i + 1);
            g.drawLine((int) (point.getX() * scaleCoefficient) + offsetX + centerOffsetX,
                    (int) (-1 * point.getY() * scaleCoefficient) + offsetY,
                    (int) (pointNext.getX() * scaleCoefficient) + offsetX + centerOffsetX,
                    (int) (-1 * pointNext.getY() * scaleCoefficient) + offsetY);
        }
    }

    private void drawShortLines(Graphics g) {
        double oneInPixels = 1 * scaleCoefficient;
        int dashes = (int) (getWidth() / scaleCoefficient);
        int y1 = getHeight() / 2 - 5;
        int y2 = getHeight() / 2 + 5;
        double dashOffset = (-centerOffsetX - getWidth() / 2.) / scaleCoefficient;
        dashOffset = dashOffset - (int) dashOffset;
        System.out.println(dashOffset);
        double x = (1 - Math.abs(dashOffset)) * scaleCoefficient;
        for (int i = 0; i < dashes + 1; i++) {
            g.drawLine((int) Math.round(x), y1, (int) Math.round(x), y2);
            x += oneInPixels;
        }
    }

    public void drawSpline(Spline spline) {
        removeAll();
        this.spline = spline;
        autoScale();
        points.clear();
        int offsetX = getWidth() / 2;
        int offsetY = getHeight() / 2;
        for (int i = 0; i < spline.getReferencePoints().size(); i++) {
            Point point = spline.getReferencePoints().get(i);
            ReferencePoint p = new ReferencePoint(point, scaleCoefficient);
            points.add(p);
            p.setCenterOffsetX(centerOffsetX);
            p.setSize(new Dimension(11, 11));
            p.setLocation((int) (point.getX() * scaleCoefficient) + offsetX + centerOffsetX,
                    (int) (-1 * point.getY() * scaleCoefficient) + offsetY);
            p.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                    super.mouseClicked(e);
                    if (deleteMode && spline.getReferencePoints().size() > 4) {
                        spline.getReferencePoints().remove(point);
                        points.remove(p);
                        remove(p);
                    }
                    repaint();
                }
            });
            add(p);
        }
    }

    public void autoScale() {
        double minX = 0.;
        double maxX = 0.;
        double maxY = 0.;
        double minY = 0.;

        for (Point point :
                spline.getReferencePoints()) {
            minX = Math.min(minX, point.getX());
            maxX = Math.max(maxX, point.getX());
            minY = Math.min(minY, point.getY());
            maxY = Math.max(maxY, point.getY());
        }
        double dif = Math.max(maxX - minX, maxY - minY);
        scaleCoefficient = (getWidth() / 2.) / dif;
        centerOffsetX = (int) (((minX + maxX) / 2) * scaleCoefficient) * -1;
        //centerOffsetY = (int) (((minY + maxY) / 2) * scaleCoefficient) * -1;
    }

    public void setDeleteMode(boolean deleteMode) {
        this.deleteMode = deleteMode;
    }

    public void setAddMode(boolean addMode) {
        this.addMode = addMode;
    }

    public void zoomIn() {
        scaleCoefficient *= 1.1;
        scaleReferencePoints();
        repaint();
    }

    public void zoomOut() {
        scaleCoefficient /= 1.1;
        scaleReferencePoints();
        repaint();
    }

    public void autoZoom() {
        autoScale();
        scaleReferencePoints();
        centerReferencePoints();
        repaint();
    }

    private void scaleReferencePoints() {
        for (int i = 0; i < points.size(); i++) {
            ReferencePoint p = points.get(i);
            p.setScale(scaleCoefficient);
            Point point = spline.getReferencePoints().get(i);
            p.setLocation((int) (point.getX() * scaleCoefficient) + getWidth() / 2 + centerOffsetX,
                    (int) (-1 * point.getY() * scaleCoefficient) + getHeight() / 2);
        }
    }

    private void centerReferencePoints() {
        for (int i = 0; i < points.size(); i++) {
            ReferencePoint p = points.get(i);
            p.setCenterOffsetX(centerOffsetX);
//            p.setCenterOffsetY(centerOffsetY);
            Point point = spline.getReferencePoints().get(i);
            p.setLocation((int) (point.getX() * scaleCoefficient) + getWidth() / 2 + centerOffsetX,
                    (int) (-1 * point.getY() * scaleCoefficient) + getHeight() / 2);
        }
    }
}
