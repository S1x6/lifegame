package ru.nsu.fit.g16202.korshunov.mvp.presenter;

import ru.nsu.fit.g16202.korshunov.mvp.model.Matrix;
import ru.nsu.fit.g16202.korshunov.mvp.model.Point;
import ru.nsu.fit.g16202.korshunov.mvp.model.Wireframe;

import java.util.ArrayList;
import java.util.List;

public class WorldPresenter {

    private Matrix scaleMatrix = Matrix.getIdentityMatrix();
    private List<Wireframe> wireframes; // in model coordinates

    public WorldPresenter(List<Wireframe> wireframes) {
        this.wireframes = wireframes;
        //revalidate();
    }

    public void scaleMatrix(double coefficient) {
        scaleMatrix.setElement(coefficient, 0, 0);
        scaleMatrix.setElement(coefficient, 1, 1);
        scaleMatrix.setElement(coefficient, 2, 2);
        scaleMatrix.setElement(1, 3, 3);
    }

    public void revalidate() {
        double maxX = 0.;
        double maxY = 0.;
        double minX = 0.;
        double minY = 0.;
        double minZ = 0.;
        double maxZ = 0.;
        for (Wireframe wireframe :
                wireframes) {
            Point ref = wireframe.getReferencePoint();
            for (List<Point> list :
                    wireframe.get3DPoints()) {
                for (Point p :
                        list) {
                    double x = p.getX() + ref.getX();
                    double y = p.getY() + ref.getY();
                    double z = p.getZ() + ref.getZ();
                    maxX = x > maxX ? x : maxX;
                    maxY = y > maxY ? y : maxY;
                    maxZ = z > maxZ ? z : maxZ;
                    minX = x < minX ? x : minX;
                    minY = y < minY ? y : minY;
                    minZ = z < minZ ? z : minZ;
                }
            }
        }
        double maxDif = Math.max(Math.max(maxX - minX, maxY - minY), maxZ - minZ);
        double scaleCoefficient = (1 / maxDif);
        scaleMatrix(scaleCoefficient);
    }

    public int wireframesCount() {
        return wireframes.size();
    }

    public List<List<Point>> getObjectInWorld(int index) {
        List<List<Point>> newLists = new ArrayList<>();
        Point refPoint = wireframes.get(index).getReferencePoint().getCopy();
        Matrix.applyMatrixToPoint(refPoint, scaleMatrix);
        List<List<Point>> dPoints = wireframes.get(index).get3DPoints();
        for (int i = 0; i < dPoints.size(); i++) {
            List<Point> list = dPoints.get(i);
            List<Point> newList = new ArrayList<>();
            for (int i1 = 0; i1 < list.size(); i1++) {
                Point point = list.get(i1);
                if (index == 0 && i == 0 && i1 == 0) {
                   // point.print("Same point in world coordinates");
                }
                Point p = point.getCopy();
                Matrix.applyMatrixToPoint(p, scaleMatrix);
                //p.print("lol");
                p.setX(p.getX() + refPoint.getX());
                p.setY(p.getY() + refPoint.getY());
                p.setZ(p.getZ() + refPoint.getZ());
                if (index == 0 && i == 0 && i1 == 0) {
                    //p.print("Point in scaled world coordinates");
                }
                newList.add(p);
            }
            newLists.add(newList);
        }

        return newLists;
    }

    public List<Point> getObjectAxis(int i) {
        Wireframe w = wireframes.get(i);
        Point refPoint = w.getReferencePoint();
        List<Point> list = w.getAxisPoints();
        for (Point p :
                list) {
            p.add(refPoint);
            Matrix.applyMatrixToPoint(p, scaleMatrix);
        }
        return list;
    }


    public void setWireframes(List<Wireframe> wireframes) {
        this.wireframes = wireframes;
        revalidate();
    }

    public List<Wireframe> getWorldWireframes() {
        return wireframes;
    }
}
