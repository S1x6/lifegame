package ru.nsu.fit.g16202.korshunov.mvp.model;

import javax.swing.*;
import javax.swing.filechooser.FileFilter;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Scanner;

public class Configuration {

    private static Configuration instance;

    private int n;
    private int m;
    private int k;
    private double a;
    private double b;
    private double c;
    private double d;
    private double zn;
    private double zf;
    private double sw;
    private double sh;
    private Color backgroundColor;

    private Configuration() {
    }

    public static Configuration getInstance() {
        if (instance == null) {
            instance = new Configuration();
        }
        return instance;
    }

    public static List<Wireframe> loadConfigurationFromFile(Component parent) {
        final JFileChooser fc = new JFileChooser();
        FileFilter filter = new FileNameExtensionFilter("TXT", "txt");
        fc.setFileFilter(filter);
        File dataDir = new File(".\\Wireframe_NSU_g16202_Korshunov_Data");
        if (!Files.exists(dataDir.toPath())) {
            try {
                Files.createDirectory(dataDir.toPath());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        fc.setCurrentDirectory(dataDir);
        int ret = fc.showDialog(parent, "Открыть конфигурацию");

        if (ret == JFileChooser.APPROVE_OPTION) {
            return loadConfiguration(fc.getSelectedFile());
        } else {
            return null;
        }
    }

    public static List<Wireframe> loadConfiguration(File file) {
        if (file.canRead()) {
            try {
                Scanner scanner = new Scanner(file);
                String[] arr;
                String line = getNextLine(scanner);
                arr = line.split(" ");
                int n = Integer.valueOf(arr[0]);
                int m = Integer.valueOf(arr[1]);
                int k = Integer.valueOf(arr[2]);
                double a = Double.valueOf(arr[3]);
                double b = Double.valueOf(arr[4]);
                double c = Double.valueOf(arr[5]);
                double d = Double.valueOf(arr[6]);
                line = getNextLine(scanner);
                arr = line.split(" ");
                double zn = Double.valueOf(arr[0]);
                double zf = Double.valueOf(arr[1]);
                double sw = Double.valueOf(arr[2]);
                double sh = Double.valueOf(arr[3]);
                getNextLine(scanner); //
                getNextLine(scanner); // skip matrix
                getNextLine(scanner); //
                line = getNextLine(scanner);
                arr = line.split(" ");
                Color bgColor = new Color(Integer.valueOf(arr[0]), Integer.valueOf(arr[1]), Integer.valueOf(arr[2]));
                line = getNextLine(scanner);
                int num = Integer.valueOf(line);
                List<Wireframe> wireframes = new ArrayList<>();
                for (int i = 0; i < num; i++) {
                    line = getNextLine(scanner);
                    arr = line.split(" ");
                    Color color = new Color(Integer.valueOf(arr[0]), Integer.valueOf(arr[1]), Integer.valueOf(arr[2]));
                    line = getNextLine(scanner);
                    arr = line.split(" ");
                    Point point = new Point(Double.valueOf(arr[0]), Double.valueOf(arr[1]), Double.valueOf(arr[2]),1);
                    line = getNextLine(scanner);
                    String[] arr1 = line.split(" ");
                    line = getNextLine(scanner);
                    String[] arr2 = line.split(" ");
                    line = getNextLine(scanner);
                    String[] arr3 = line.split(" ");
                    Matrix matrix = new Matrix(new double[][]{
                            {Double.valueOf(arr1[0]), Double.valueOf(arr1[1]), Double.valueOf(arr1[2]), 0},
                            {Double.valueOf(arr2[0]), Double.valueOf(arr2[1]), Double.valueOf(arr2[2]), 0},
                            {Double.valueOf(arr3[0]), Double.valueOf(arr3[1]), Double.valueOf(arr3[2]), 0},
                            {0, 0, 0, 1}
                    });
                    line = getNextLine(scanner);
                    int pointsNum = Integer.valueOf(line);
                    Spline spline = new Spline();
                    for (int j = 0; j < pointsNum; j++) {
                        line = getNextLine(scanner);
                        arr = line.split(" ");
                        spline.addReferencePoint(new Point(Double.valueOf(arr[0]), Double.valueOf(arr[1]), 0.,1));
                    }
                    spline.setColor(color);
                    Wireframe wireframe = new Wireframe(spline, point, matrix);
                    wireframes.add(wireframe);
                }
                getInstance().setN(n);
                instance.setM(m);
                instance.setK(k);
                instance.setA(a);
                instance.setB(b);
                instance.setC(c);
                instance.setD(d);
                instance.setZn(zn);
                instance.setZf(zf);
                instance.setSw(sw);
                instance.setSh(sh);
                instance.setBackgroundColor(bgColor);
                return wireframes;
            } catch (IOException | RuntimeException e) {
                return null;
            }
        }
        return null;
    }

    private static String getNextLine(Scanner scanner) throws NoSuchElementException {
        String line = "";
        int commentIndex;
        while (line.equals("")) {
            line = scanner.nextLine();
            commentIndex = line.indexOf("//");
            if (commentIndex != -1) {
                line = line.substring(0, commentIndex);
            }
            line = line.trim();
        }
        return line;
    }

    public int getN() {
        return n;
    }

    public void setN(int n) {
        this.n = n;
    }

    public int getM() {
        return m;
    }

    public void setM(int m) {
        this.m = m;
    }

    public int getK() {
        return k;
    }

    public void setK(int k) {
        this.k = k;
    }

    public double getA() {
        return a;
    }

    public void setA(double a) {
        this.a = a;
    }

    public double getB() {
        return b;
    }

    public void setB(double b) {
        this.b = b;
    }

    public double getC() {
        return c;
    }

    public void setC(double c) {
        this.c = c;
    }

    public double getD() {
        return d;
    }

    public void setD(double d) {
        this.d = d;
    }

    public double getZn() {
        return zn;
    }

    public void setZn(double zn) {
        this.zn = zn;
    }

    public double getZf() {
        return zf;
    }

    public void setZf(double zf) {
        this.zf = zf;
    }

    public double getSw() {
        return sw;
    }

    public void setSw(double sw) {
        this.sw = sw;
    }

    public double getSh() {
        return sh;
    }

    public void setSh(double sh) {
        this.sh = sh;
    }

    public Color getBackgroundColor() {
        return backgroundColor;
    }

    public void setBackgroundColor(Color backgroundColor) {
        this.backgroundColor = backgroundColor;
    }
}
