package ru.nsu.fit.g16202.korshunov.ui.dialog;

import ru.nsu.fit.g16202.korshunov.frame.DefaultToolBarToggleButton;
import ru.nsu.fit.g16202.korshunov.mvp.model.Configuration;
import ru.nsu.fit.g16202.korshunov.mvp.model.Spline;
import ru.nsu.fit.g16202.korshunov.mvp.view.EditorView;
import ru.nsu.fit.g16202.korshunov.ui.DoubleTextField;
import ru.nsu.fit.g16202.korshunov.ui.IntegerTextField;

import javax.swing.*;
import java.awt.*;
import java.util.List;

public class EditorDialog extends JDialog {

    private EditorView view;
    private List<Spline> splines;
    private Spline currentSplineCopy;

    private IntegerTextField nField = new IntegerTextField("n: ", 0, null, 0);
    private IntegerTextField mField = new IntegerTextField("m: ", 0, null, 0);
    private IntegerTextField kField = new IntegerTextField("k: ", 0, null, 0);
    private IntegerTextField numField = new IntegerTextField("№: ", 0, null, 0);
    private IntegerTextField redField = new IntegerTextField("R: ", 0, 255, 0);
    private DoubleTextField aField = new DoubleTextField("a: ", 0., 1., 0., 0.1);
    private DoubleTextField bField = new DoubleTextField("b: ", 0., 1., 0., 0.1);
    private DoubleTextField cField = new DoubleTextField("c: ", 0., 360., 0., 0.1);
    private DoubleTextField dField = new DoubleTextField("d: ", 0., 360., 0., 0.1);
    private IntegerTextField greenField = new IntegerTextField("G: ", 0, 255, 0);
    private DoubleTextField znField = new DoubleTextField("zn: ", 0., null, 0., 0.1);
    private DoubleTextField zfField = new DoubleTextField("zf: ", 0., null, 0., 0.1);
    private DoubleTextField swField = new DoubleTextField("sw: ", 0., null, 0., 0.1);
    private DoubleTextField shField = new DoubleTextField("sh: ", 0., null, 0., 0.1);
    private IntegerTextField blueField = new IntegerTextField("B: ", 0, 255, 0);

    public EditorDialog(Frame owner) {
        super(owner, true);
        view = new EditorView();
        initLayout();
        setResizable(false);
    }

    private void initLayout() {
        setTitle("Редактирование настроек");
        setLayout(new BorderLayout());
        add(view, BorderLayout.CENTER);
        JPanel bottomPanel = new JPanel();
        add(bottomPanel, BorderLayout.SOUTH);
        bottomPanel.setLayout(new BoxLayout(bottomPanel, BoxLayout.Y_AXIS));
        JPanel fieldsPanel = new JPanel();
        fieldsPanel.setLayout(new GridLayout(3, 5, 6, 2));
        fieldsPanel.add(nField);
        fieldsPanel.add(mField);
        fieldsPanel.add(kField);
        fieldsPanel.add(numField);
        fieldsPanel.add(redField);
        fieldsPanel.add(aField);
        fieldsPanel.add(bField);
        fieldsPanel.add(cField);
        fieldsPanel.add(dField);
        fieldsPanel.add(greenField);
        fieldsPanel.add(znField);
        fieldsPanel.add(zfField);
        fieldsPanel.add(swField);
        fieldsPanel.add(shField);
        fieldsPanel.add(blueField);
        numField.setEnabled(false);
        numField.setOnArrowsClickedListener(e -> {
            currentSplineCopy.setPointsFrom(splines.get(numField.getValue()));
            currentSplineCopy.setColor(splines.get(numField.getValue()).getColor());
            redField.setValue(currentSplineCopy.getColor().getRed());
            greenField.setValue(currentSplineCopy.getColor().getGreen());
            blueField.setValue(currentSplineCopy.getColor().getBlue());
            view.drawSpline(currentSplineCopy);
            view.autoScale();
            view.repaint();
        });
        bottomPanel.add(fieldsPanel);
        JPanel buttonsPanel = new JPanel();
        JButton cancelButton = new JButton("Отмена");
        JButton okButton = new JButton("Ок");
        JButton applyButton = new JButton("Применить");
        JToggleButton addButton = new JToggleButton(new ImageIcon(ClassLoader.getSystemResource("plus.png")));
        addButton.addActionListener(e -> view.setAddMode(addButton.isSelected()));
        JToggleButton deleteButton = new JToggleButton(new ImageIcon(ClassLoader.getSystemResource("close.png")));
        deleteButton.addActionListener(e -> view.setDeleteMode(deleteButton.isSelected()));
        JButton zoomInButton = new JButton(new ImageIcon(ClassLoader.getSystemResource("zoom-in.png")));
        zoomInButton.addActionListener(e->view.zoomIn());
        JButton zoomOutButton = new JButton(new ImageIcon(ClassLoader.getSystemResource("zoom-out.png")));
        zoomOutButton.addActionListener(e->view.zoomOut());
        JButton zoomAutoButton = new JButton(new ImageIcon(ClassLoader.getSystemResource("auto-zoom.png")));
        zoomAutoButton.addActionListener(e->view.autoZoom());
        okButton.addActionListener(e -> {
            if (applySettings())
                dispose();
        });
        applyButton.addActionListener(e -> applySettings());
        cancelButton.addActionListener(e -> dispose());
        buttonsPanel.add(okButton);
        buttonsPanel.add(applyButton);
        buttonsPanel.add(cancelButton);
        buttonsPanel.add(addButton);
        buttonsPanel.add(deleteButton);
        buttonsPanel.add(zoomInButton);
        buttonsPanel.add(zoomOutButton);
        buttonsPanel.add(zoomAutoButton);
        bottomPanel.add(buttonsPanel);
        setCurrentValues();
        pack();
    }

    private boolean applySettings() {
        if (!isDataValid()) {
            JOptionPane.showMessageDialog(this, "Введены некорректные значения");
            return false;
        }
        Configuration configuration = Configuration.getInstance();
        configuration.setN(nField.getValue());
        configuration.setM(mField.getValue());
        configuration.setK(kField.getValue());
        configuration.setA(aField.getValue());
        configuration.setB(bField.getValue());
        configuration.setC(cField.getValue());
        configuration.setD(dField.getValue());
        configuration.setZn(znField.getValue());
        configuration.setZf(zfField.getValue());
        configuration.setSw(swField.getValue());
        configuration.setSh(shField.getValue());
        Spline spline = splines.get(numField.getValue());
        spline.setPointsFrom(currentSplineCopy);
        spline.setColor(new Color(redField.getValue(), greenField.getValue(), blueField.getValue()));
        spline.getSplineCurve();
        view.repaint();
        return true;
    }

    public void open(List<Spline> splines, Integer splineIndex) {
        this.splines = splines;
        numField.setValue(splineIndex);
        numField.setMax(splines.size() - 1);
        setCurrentValues();
        currentSplineCopy = splines.get(splineIndex).getCopy();
        redField.setValue(currentSplineCopy.getColor().getRed());
        greenField.setValue(currentSplineCopy.getColor().getGreen());
        blueField.setValue(currentSplineCopy.getColor().getBlue());
        view.drawSpline(currentSplineCopy);
        view.repaint();
        setLocation(-1 * (getWidth() / 2), -1 * (getHeight() / 2));
        setLocationRelativeTo(getParent());
        this.setVisible(true);
    }

    private void setCurrentValues() {
        Configuration conf = Configuration.getInstance();
        nField.setValue(conf.getN());
        mField.setValue(conf.getM());
        kField.setValue(conf.getK());
        if (currentSplineCopy != null) redField.setValue(currentSplineCopy.getColor().getRed());
        aField.setValue(conf.getA());
        bField.setValue(conf.getB());
        cField.setValue(conf.getC());
        dField.setValue(conf.getD());
        if (currentSplineCopy != null) greenField.setValue(currentSplineCopy.getColor().getGreen());
        zfField.setValue(conf.getZf());
        znField.setValue(conf.getZn());
        swField.setValue(conf.getSw());
        shField.setValue(conf.getSh());
        if (currentSplineCopy != null) blueField.setValue(currentSplineCopy.getColor().getBlue());
    }

    private boolean isDataValid() {
        return nField.validateData() &&
                mField.validateData() &&
                kField.validateData() &&
                redField.validateData() &&
                aField.validateData() &&
                bField.validateData() &&
                cField.validateData() &&
                dField.validateData() &&
                greenField.validateData() &&
                zfField.validateData() &&
                znField.validateData() &&
                swField.validateData() &&
                shField.validateData() &&
                blueField.validateData() &&
                aField.getValue() < bField.getValue() &&
                cField.getValue() < dField.getValue();

    }
}