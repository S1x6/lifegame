package ru.nsu.fit.g16202.korshunov.mvp.view;

import ru.nsu.fit.g16202.korshunov.mvp.model.Configuration;
import ru.nsu.fit.g16202.korshunov.mvp.model.Matrix;
import ru.nsu.fit.g16202.korshunov.mvp.model.Point;
import ru.nsu.fit.g16202.korshunov.mvp.model.Wireframe;
import ru.nsu.fit.g16202.korshunov.mvp.presenter.Camera;
import ru.nsu.fit.g16202.korshunov.mvp.presenter.KeyListener;
import ru.nsu.fit.g16202.korshunov.mvp.presenter.WorldPresenter;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;
import java.awt.event.MouseWheelEvent;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.List;

public class View extends JPanel {

    private static final int HORIZONTAL_OFFSET = 10;
    private static final int VERTICAL_OFFSET = 10;

    private WorldPresenter worldPresenter = new WorldPresenter(null);
    private List<Wireframe> wireframes = new ArrayList<>();
    private Camera camera = new Camera(worldPresenter);
    private BufferedImage image;
    private int lastWidth;
    private int lastHeight;
    private double lastSw;
    private double lastSh;
    private int selectedIndex;
    private int lastX;

    private KeyListener k = new KeyListener();

    public View() {
        addMouseMotionListener(new MouseMotionAdapter() {
            @Override
            public void mouseDragged(MouseEvent e) {
                super.mouseDragged(e);
                int sign = lastX - e.getX() < 0 ? -1 : 1;
                lastX = e.getX();
                if (k.isVPressed()) {
                    camera.rotate(sign, Wireframe.Axis.X_AXIS);
//                    camera.move(sign * 0.1, Wireframe.Axis.X_AXIS);
                }
                if (k.isBPressed()) {
                    camera.rotate(sign, Wireframe.Axis.Y_AXIS);
//                    camera.move(sign * 0.1, Wireframe.Axis.Y_AXIS);
                }
                if (k.isNPressed()) {
                    camera.rotate(sign, Wireframe.Axis.Z_AXIS);
//                    camera.move(sign * 0.1, Wireframe.Axis.Z_AXIS);
                }
                if (k.isFPressed()) {
//                    camera.rotate(sign, Wireframe.Axis.Z_AXIS);
                    camera.move(sign * 0.1, Wireframe.Axis.X_AXIS);
                }
                if (k.isGPressed()) {
//                    camera.rotate(sign, Wireframe.Axis.Z_AXIS);
                    camera.move(sign * 0.1, Wireframe.Axis.Y_AXIS);
                }
                if (k.isHPressed()) {
//                    camera.rotate(sign, Wireframe.Axis.Z_AXIS);
                    camera.move(sign * 0.1, Wireframe.Axis.Z_AXIS);
                }
            }
        });
        addMouseWheelListener(new MouseAdapter() {
            @Override
            public void mouseWheelMoved(MouseWheelEvent e) {
                super.mouseWheelMoved(e);
//                Configuration c = Configuration.getInstance();;
                if (e.getWheelRotation() > 0) {
                    camera.zoomIn(1.05);
                    repaint();
                } else if (e.getWheelRotation() < 0) {
                    camera.zoomOut(1.05);
                    repaint();
                }
            }
        });
        //k.setActionForFPressed(o -> camera.move(1, Wireframe.Axis.X_AXIS));
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        Configuration c = Configuration.getInstance();
        Dimension d = camera.getViewPortSize(getWidth() - 2 * HORIZONTAL_OFFSET, getHeight() - 2 * VERTICAL_OFFSET);
        if (getWidth() != lastWidth || getHeight() != lastHeight || c.getSh() != lastSh || c.getSw() != lastSw) {
            lastSw = c.getSw();
            lastSh = c.getSh();
            lastHeight = getHeight();
            lastWidth = getWidth();
            image = new BufferedImage(d.width, d.height, BufferedImage.TYPE_INT_RGB);
            // camera.calculateProjectionMatrix();
        }
        g.drawRect(HORIZONTAL_OFFSET - 1, VERTICAL_OFFSET - 1, d.width + 1, d.height + 1);
        camera.setWorldView(image, selectedIndex);
        g.drawImage(image, HORIZONTAL_OFFSET, VERTICAL_OFFSET, this);
    }

    public void setSelectedIndex(int selectedIndex) {
        this.selectedIndex = selectedIndex;
    }

    public void setWireframes(List<Wireframe> wireframe) {
        this.wireframes = wireframe;
        worldPresenter.setWireframes(wireframes);
        repaint();
    }

    public void resetCamera() {
        camera.setViewPoint(new Point(0, 0, 0, 0));
        camera.setPosition(new Point(-10, 0, 0, 0));
        camera.setRotationMatrix(Matrix.getIdentityMatrix());
        repaint();
    }
}
