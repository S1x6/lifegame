package ru.nsu.fit.g16202.korshunov.mvp.model;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Spline {

    private static Matrix M = new Matrix(new double[][]{
            {-1. / 6, 3. / 6, -3. / 6, 1. / 6},
            {3. / 6, -6. / 6, 3. / 6, 0},
            {-3. / 6, 0, 3. / 6, 0},
            {1. / 6, 4. / 6, 1. / 6, 0}
    });

    private List<Point> referencePoints;
    private Color color;
    private List<Point> points = new ArrayList<>();

    public Spline() {
        referencePoints = new ArrayList<>();
        Random r = new Random();
        color = new Color(r.nextInt(256), r.nextInt(256), r.nextInt(256));
    }

    public void addReferencePoint(Point point) {
        referencePoints.add(point);
    }

    public List<Point> getReferencePoints() {
        return referencePoints;
    }

    public Spline getCopy() {
        Spline spline = new Spline();
        for (Point point :
                referencePoints) {
            Point p = point.getCopy();
            spline.referencePoints.add(p);
        }
        spline.setColor(new Color(color.getRGB()));
        return spline;
    }

    public void setPointsFrom(Spline spline) {
        this.referencePoints.clear();
        for (Point point :
                spline.referencePoints) {
            Point p = point.getCopy();
            this.referencePoints.add(p);
        }
    }

    public List<Point> getSplineCurve() {
        points.clear();
        int accuracy = 100 / (referencePoints.size() - 3);
        double[][] elementsT = new double[][]{{0, 0, 0, 0}, {0, 0, 0, 0}, {0, 0, 0, 0}, {0, 0, 0, 0}};
        Matrix T = new Matrix(elementsT);
        double t;
        for (int i = 1; i < referencePoints.size() - 2; i++) {
            t = 0;
            double stepT = 1. / accuracy;
            Matrix Gx = new Matrix(new double[][]{
                    {referencePoints.get(i - 1).getX(), 0, 0, 0},
                    {referencePoints.get(i).getX(), 0, 0, 0},
                    {referencePoints.get(i + 1).getX(), 0, 0, 0},
                    {referencePoints.get(i + 2).getX(), 0, 0, 0}
            });
            Matrix Gy = new Matrix(new double[][]{
                    {referencePoints.get(i - 1).getY(), 0, 0, 0},
                    {referencePoints.get(i).getY(), 0, 0, 0},
                    {referencePoints.get(i + 1).getY(), 0, 0, 0},
                    {referencePoints.get(i + 2).getY(), 0, 0, 0}
            });
            for (int j = 0; j < accuracy; j++) {
                t += stepT;
                elementsT[0][0] = t * t * t;
                elementsT[0][1] = t * t;
                elementsT[0][2] = t;
                elementsT[0][3] = 1;
                T.setElements(elementsT);
                double x = T.multiply(M).multiply(Gx).getElement(0, 0);
                double y = T.multiply(M).multiply(Gy).getElement(0, 0);
                points.add(new Point(x, y, 0, 1));
            }
        }
        System.out.println("spline points " + points.size() + " accuracy " + accuracy);
        return points;
    }

    public Point getPointAt(double part) {
        int first = (int) ((points.size()-1) * part);
        int second = first == points.size() - 1 ? first : first + 1;
        return new Point((points.get(first).getX() + points.get(second).getX()) / 2,
                (points.get(first).getY() + points.get(second).getY()) / 2,
                0, 1);
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }
}
