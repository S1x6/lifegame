package ru.nsu.fit.g16202.korshunov.mvp.model;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.List;

public abstract class Wireframe {

    private OpticalConfiguration opticalConfiguration;
    protected List<Point> points = new ArrayList<>();
    protected List<Point> initialPoints = new ArrayList<>();

    public Wireframe(OpticalConfiguration opticalConfiguration) {
        this.opticalConfiguration = opticalConfiguration;
    }

    public final void applyMatrix(Matrix m) {
        for (Point p :
                points) {
            Matrix.applyMatrixToPoint(p, m);
        }
    }

    public void initPoints() {
        points.clear();
        initialPoints.forEach(p -> points.add(p.getCopy()));
    }

    public abstract void print(Graphics2D g, Dimension d);

    public abstract double getMaxX();
    public abstract double getMaxY();
    public abstract double getMaxZ();
    public abstract double getMinX();
    public abstract double getMinY();
    public abstract double getMinZ();

    public abstract Point getNormal(Point p);

    public abstract Double detectCollision(Point start, Point ray);

    public void applyPerspective() {
        points.forEach(p -> {
            p.setX(p.getX() / p.getW());
            p.setY(p.getY() / p.getW());
        });
    }

    public final OpticalConfiguration getOpticalOptions() {
        return opticalConfiguration;
    }

    public enum Axis {
        X_AXIS, Y_AXIS, Z_AXIS
    }
}
