package ru.nsu.fit.g16202.korshunov.mvp.presenter;

import ru.nsu.fit.g16202.korshunov.mvp.model.*;

import java.util.ArrayList;
import java.util.List;

public class WorldPresenter {

    private Matrix scaleMatrix = Matrix.getIdentityMatrix();
    private List<Wireframe> wireframes = new ArrayList<>();
    private List<Light> lights = new ArrayList<>();

    public void scaleMatrix(double coefficient) {
        scaleMatrix.setElement(coefficient, 0, 0);
        scaleMatrix.setElement(coefficient, 1, 1);
        scaleMatrix.setElement(coefficient, 2, 2);
        scaleMatrix.setElement(1, 3, 3);
    }

    public void revalidate() {
        double maxX = -Double.MAX_VALUE;
        double maxY = -Double.MAX_VALUE;
        double maxZ = -Double.MAX_VALUE;
        double minX = Double.MAX_VALUE;
        double minY = Double.MAX_VALUE;
        double minZ = Double.MAX_VALUE;
        for (Wireframe w :
                wireframes) {
            w.initPoints();
            maxX = w.getMaxX() > maxX ? w.getMaxX() : maxX;
            maxY = w.getMaxY() > maxY ? w.getMaxY() : maxY;
            maxZ = w.getMaxZ() > maxZ ? w.getMaxZ() : maxZ;
            minX = w.getMinX() < minX ? w.getMinX() : minX;
            minY = w.getMinY() < minY ? w.getMinY() : minY;
            minZ = w.getMinZ() < minZ ? w.getMinZ() : minZ;
        }
        Point newCenter = new Point(maxX - (maxX - minX) / 2, maxY - (maxY - minY) / 2, maxZ - (maxZ - minZ) / 2, 1).inverse();
        double maxDif = Math.max(Math.max(maxX - minX, maxY - minY), maxZ - minZ);
        double scaleCoefficient = (2 / maxDif / 1.05);
        scaleMatrix(scaleCoefficient);
        Matrix translationMatrix = new Matrix(new double[][]{
                {1, 0, 0, 0},
                {0, 1, 0, 0},
                {0, 0, 1, 0},
                {newCenter.getX() * scaleCoefficient, newCenter.getY() * scaleCoefficient, newCenter.getZ() * scaleCoefficient, 1}});
        lights.forEach(l-> {
            l.initPosition();
            Matrix.applyMatrixToPoint(l.getPosition(),translationMatrix);
        });
        wireframes.forEach(w -> {
            w.applyMatrix(scaleMatrix);
            w.applyMatrix(translationMatrix);
            if (w instanceof Sphere) {
                ((Sphere) w).setRadius(((Sphere) w).getRadius() * scaleCoefficient);
            }
        });
    }

    public int wireframesCount() {
        return wireframes.size();
    }

    public void setWireframes(List<Wireframe> wireframes) {
        this.wireframes = wireframes;
        revalidate();
    }

    public List<Wireframe> getWorldWireframes(boolean revalidate) {
        if (revalidate) {
            revalidate();
        }
        return wireframes;
    }

    public List<Light> getLights() {
        List<Light> light = new ArrayList<>();
        lights.forEach(l -> light.add(new Light(l.getPosition().getCopy(), l.getColor())));
        return light;
    }

    public void setLights(List<Light> lights) {
        this.lights = lights;
    }
}
