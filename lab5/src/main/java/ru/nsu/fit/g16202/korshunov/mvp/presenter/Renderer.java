package ru.nsu.fit.g16202.korshunov.mvp.presenter;


import ru.nsu.fit.g16202.korshunov.mvp.model.Light;
import ru.nsu.fit.g16202.korshunov.mvp.model.Point;
import ru.nsu.fit.g16202.korshunov.mvp.model.Wireframe;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.List;

public class Renderer {

    private int backgroundColor = Color.decode("#222222").getRGB();

    private Camera camera;
    private int traceDepth = 2;
    private Color ambientColor = Color.decode("#000000");
    private List<Wireframe> objects;
    private List<Light> lights;

    public void render(Camera camera, BufferedImage image) {
        this.camera = camera;
        this.objects = camera.getObjectsInCamera();
        this.lights = camera.getLights();
        Dimension imageSize = new Dimension(image.getWidth(), image.getHeight());
        Dimension plainSize = new Dimension(1, 1);
//        Point invertedCameraPosition = camera.getPosition().getCopy();
//        invertedCameraPosition.setX(-invertedCameraPosition.getX());
//        invertedCameraPosition.setY(-invertedCameraPosition.getY());
//        invertedCameraPosition.setZ(-invertedCameraPosition.getZ());
        Point ray = new Point(1, 1, -Camera.ZN * Camera.ZN, 1);
        for (int h = 0; h < image.getHeight(); h++) {
            for (int w = 0; w < image.getWidth(); w++) {
                setPixelPosition(ray, h, w, imageSize, plainSize);
//                ray.add(invertedCameraPosition);
//                ray.print("");
                ray.setZ(-Camera.ZN * Camera.ZN);
                ray.normalize();
                image.setRGB(w, h, processRay(ray));
            }
        }
    }

    private void setPixelPosition(Point p, int h, int w, Dimension imageSize, Dimension plainSize) {
        p.setX(plainSize.width / 2. - (double) (plainSize.width) / imageSize.width * w);
        p.setY(-plainSize.height / 2. + (double) (plainSize.height) / imageSize.height * h);
    }

    private int processRay(Point ray) {

        Point cameraPos = new Point(0, 0, 0, 1);
        int color = backgroundColor;
        double lastZ = -Double.MAX_VALUE;
        Wireframe nearestWireframe = null;
        for (Wireframe w : objects) {
            Double t = w.detectCollision(cameraPos, ray);
            if (t == null) {
                continue;
            }
            Point collision = new Point(ray.getX() * t, ray.getY() * t, ray.getZ() * t, 1);
            if (lastZ < collision.getZ()) {
                lastZ = collision.getZ();
                color = findLights(collision, ray, w, traceDepth);
                nearestWireframe = w;
            }
        }
        if (nearestWireframe != null) {
            color = addColorComponent(color, (int) (ambientColor.getRed() * nearestWireframe.getOpticalOptions().getKDr()), ColorComponent.RED);
            color = addColorComponent(color, (int) (ambientColor.getGreen() * nearestWireframe.getOpticalOptions().getKDg()), ColorComponent.GREEN);
            color = addColorComponent(color, (int) (ambientColor.getBlue() * nearestWireframe.getOpticalOptions().getKDb()), ColorComponent.BLUE);
        }
        return color;
    }

    private int findLights(Point collision, Point specularRay, Wireframe object, int traceNum) {
        int lightColorR = 0;
        int lightColorG = 0;
        int lightColorB = 0;
        Wireframe nearestWireframe;
        Point ray;
        boolean noCollision;
        int reflectedColor;
        Double t, t1;
        double dist, dist1, dist2, diffCoef, specCoef, lastZ;
        Point normal, h, look, reflected, newCollision;
         for (Light l : lights) {
                ray = l.getPosition().getCopy();
                ray.add(collision.getCopy().inverse());
                ray.normalize();
                noCollision = true;
                for (Wireframe w : objects) {
                    t = w.detectCollision(collision, ray);
                    if (t != null) {
                        noCollision = false;
                    }
                }
                if (noCollision) {
                    dist = l.getPosition().distanceFrom(collision);
                    diffCoef = 1;
                    specCoef = 1;

                    // diffuse
                    normal = object.getNormal(collision);
                    diffCoef = normal.scalar(ray) / ray.getLength();
                    if (diffCoef < 0) {
                        diffCoef = 0;
                    }

                    // specular
                    h = specularRay.getCopy().inverse();
                    h.add(ray);
                    h.normalize();
                    specCoef = Math.pow(normal.scalar(h), object.getOpticalOptions().getPower());

                    // apply diffuse and specular
                    lightColorR += l.getColor().getRed() / dist / dist *
                            (object.getOpticalOptions().getKDr() * diffCoef + specCoef * object.getOpticalOptions().getKSr());
                    lightColorG += l.getColor().getGreen() / dist / dist *
                            (object.getOpticalOptions().getKDg() * diffCoef + specCoef * object.getOpticalOptions().getKSg());
                    lightColorB += l.getColor().getBlue() / dist / dist *
                            (object.getOpticalOptions().getKDb() * diffCoef + specCoef * object.getOpticalOptions().getKSb());

                    //reflected

                    if (traceNum > 1) {
                        look = collision.getCopy().inverse();
                        look.normalize();
                        reflected = normal.getCopy().multiply(2 * normal.scalar(look));
                        reflected.add(look.inverse());
                        reflected.normalize();
                        nearestWireframe = null;
                        newCollision = null;
                        lastZ = Double.MAX_VALUE;
                        for (Wireframe w : objects) {
                            t1 = w.detectCollision(collision, reflected);
                            if (t1 == null) {
                                continue;
                            }
                            newCollision = new Point(reflected.getX() * t1, reflected.getY() * t1, reflected.getZ() * t1, 1);
                            if (lastZ > newCollision.getZ()) {
                                lastZ = newCollision.getZ();
                                nearestWireframe = w;
                            }
                        }
                        if (nearestWireframe != null) {
                            dist2 = newCollision.getLength();
                            dist1 = Math.max(dist2 * dist2, 1);
                            newCollision.add(collision);
                            reflectedColor = findLights(newCollision, reflected, nearestWireframe, traceDepth - 1);
//                        System.out.println("reflected: " + reflectedColor);
                            lightColorR += ((reflectedColor >> 16) & 0x0000ff) / dist1 * object.getOpticalOptions().getKSr();
                            lightColorG += ((reflectedColor >> 8) & 0x0000ff) / dist1 * object.getOpticalOptions().getKSg();
                            lightColorB += (reflectedColor & 0x0000ff) / dist1 * object.getOpticalOptions().getKSb();
                        }
                    }

                }
            }

            lightColorR = Math.min(lightColorR, 255);
            lightColorG = Math.min(lightColorG, 255);
            lightColorB = Math.min(lightColorB, 255);
        return (lightColorR << 16) | (lightColorG << 8) | lightColorB;

    }

    private int addColorComponent(int color, int comp, ColorComponent c) {
        int newColor;
        switch (c) {
            case RED:
                newColor = Math.min(((color >> 16) & 0x0000ff) + comp, 255);
                color &= 0x00ffff;
                color |= newColor << 16;
                break;
            case GREEN:
                newColor = Math.min(((color >> 8) & 0x0000ff) + comp, 255);
                color &= 0xff00ff;
                color |= newColor << 8;
                break;
            case BLUE:
                newColor = Math.min((color & 0x0000ff) + comp, 255);
                color &= 0xffff00;
                color |= newColor;
                break;
        }
        return color;
    }

    private enum ColorComponent {RED, GREEN, BLUE}

}
