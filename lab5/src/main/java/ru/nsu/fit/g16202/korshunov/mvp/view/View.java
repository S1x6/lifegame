package ru.nsu.fit.g16202.korshunov.mvp.view;

import ru.nsu.fit.g16202.korshunov.mvp.model.Matrix;
import ru.nsu.fit.g16202.korshunov.mvp.model.Point;
import ru.nsu.fit.g16202.korshunov.mvp.model.Wireframe;
import ru.nsu.fit.g16202.korshunov.mvp.presenter.Camera;
import ru.nsu.fit.g16202.korshunov.mvp.presenter.Renderer;
import ru.nsu.fit.g16202.korshunov.mvp.presenter.WorldPresenter;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;
import java.awt.event.MouseWheelEvent;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.List;

public class View extends JPanel {

    private static final int HORIZONTAL_OFFSET = 10;
    private static final int VERTICAL_OFFSET = 10;

    private List<Wireframe> wireframes = new ArrayList<>();
    private BufferedImage image;
    private Dimension viewPortSize;
    private Camera camera;
    private int lastWidth;
    private int lastHeight;

    public View() {
    }


    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        Dimension d = camera.getViewPortSize(getWidth() - 2 * HORIZONTAL_OFFSET, getHeight() - 2 * VERTICAL_OFFSET);
        if (getWidth() != lastWidth || getHeight() != lastHeight) {
            lastHeight = getHeight();
            lastWidth = getWidth();
            image = new BufferedImage(d.width, d.height, BufferedImage.TYPE_INT_RGB);
            // camera.calculateProjectionMatrix();
        }
        g.drawRect(HORIZONTAL_OFFSET - 1, VERTICAL_OFFSET - 1, d.width + 1, d.height + 1);
        g.drawImage(image, HORIZONTAL_OFFSET, VERTICAL_OFFSET, this);
    }

    public void setWireframes(List<Wireframe> wireframe) {
        this.wireframes = wireframe;
        repaint();
    }

    public BufferedImage getImage() {
        return image;
    }


    public void setViewPortSize(Dimension viewPortSize) {
        this.viewPortSize = viewPortSize;
    }

    public void setCamera(Camera camera) {
        this.camera = camera;
    }
}
