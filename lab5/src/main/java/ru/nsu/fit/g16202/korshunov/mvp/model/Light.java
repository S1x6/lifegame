package ru.nsu.fit.g16202.korshunov.mvp.model;

import java.awt.*;

public class Light {

    private Point position;
    private Point initialPosition;
    private Color color;

    public Light(Point position, Color color) {
        this.initialPosition = position;
        this.position = position.getCopy();
        this.color = color;
    }

    public void initPosition() {
        this.position = initialPosition.getCopy();
    }

    public Point getPosition() {
        return position;
    }

    public void setPosition(Point position) {
        this.position = position;
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }
}
