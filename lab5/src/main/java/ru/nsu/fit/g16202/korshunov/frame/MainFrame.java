package ru.nsu.fit.g16202.korshunov.frame;

import ru.nsu.fit.g16202.korshunov.mvp.presenter.Presenter;
import ru.nsu.fit.g16202.korshunov.ui.StatusBar;

import javax.swing.*;
import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

public class MainFrame extends JFrame {

    private Presenter presenter;
    private StatusBar statusBar;
    private JToggleButton selectViewButton;
    private JToggleButton renderButton;

    public static void main(String[] args) {
        try {
            UIManager.setLookAndFeel(
                    UIManager.getSystemLookAndFeelClassName());
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | UnsupportedLookAndFeelException e) {
            e.printStackTrace();
        }
        new MainFrame();
    }

    private MainFrame() {
        super();
        Dimension minDim = new Dimension(1100, 700);
        setSize(minDim);
        setMinimumSize(minDim);
        setTitle("RayTracing NSU_g16202_Korshunov");
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
        this.setLocation(dim.width / 2 - this.getSize().width / 2, dim.height / 2 - this.getSize().height / 2);
        this.setLayout(new BorderLayout());
        initStatusBar();
        initToolbar();
        presenter = new Presenter(this);
        add(presenter.getViewPort(), BorderLayout.CENTER);
        pack();
        setVisible(true);
    }

    private void initToolbar() {
        JToolBar toolBar = new JToolBar();
        toolBar.setFloatable(false);
//        JButton open = new DefaultToolBarButton("folder.png", "Открыть файл сцены", statusBar, e -> presenter.openNewScene());
//        toolBar.add(open);
//        JButton save = new DefaultToolBarButton("save.png", "Сохранить сцену в файл", statusBar, e -> presenter.saveSceneToFile());
//        toolBar.add(save);
//        JButton init = new DefaultToolBarButton("home-button.png", "Сброс сцены", statusBar, e -> presenter.init());
//        toolBar.add(init);
//        JButton settings = new DefaultToolBarButton("settings.png", "Настройки сцены", statusBar, e -> presenter.openSettings());
//        toolBar.add(settings);
//        JButton add = new DefaultToolBarButton("add.png", "Добавить объект", statusBar, e -> presenter.createNewSpline());
//        toolBar.add(add);
//        JButton delete = new DefaultToolBarButton("delete.png", "Редактировать объект", statusBar, e -> presenter.deleteSpline());
//        toolBar.add(delete);
//        JButton left = new DefaultToolBarButton("left-arrow.png", "Предыдущий объект", statusBar, e -> presenter.selectPrevious());
//        toolBar.add(left);
//        JButton right = new DefaultToolBarButton("right-arrow.png", "Следующий объект", statusBar, e -> presenter.selectNext());
//        toolBar.add(right);
        selectViewButton = new DefaultToolBarToggleButton("settings.png", "Настройка ракурса", statusBar,
                (e, s) -> {
                    if (s) {
                        presenter.setSelectionMode();
                        renderButton.setEnabled(true);
                        renderButton.setSelected(false);
                        selectViewButton.setEnabled(false);
                    }
                });
        renderButton = new DefaultToolBarToggleButton("right-arrow.png", "Рендер", statusBar,
                (e, s) -> {
                    if (s) {
                        presenter.startRender();
                        selectViewButton.setSelected(false);
                        selectViewButton.setEnabled(true);
                        renderButton.setEnabled(false);
                    }
                });
        toolBar.add(selectViewButton);
        toolBar.add(renderButton);
        JButton about = new DefaultToolBarButton("info.png", "О программе", statusBar, e -> {
            JOptionPane.showMessageDialog(this,
                    "Приложение \"RayTracing\" \nАвтор: Коршунов Вадим\nГруппа: 16202\nВерсия 0.1",
                    "О программе", JOptionPane.INFORMATION_MESSAGE, new ImageIcon(ClassLoader.getSystemResource("me.jpg")));
        });
        toolBar.add(about);
        add(toolBar, BorderLayout.NORTH);
    }

    private void initStatusBar() {
        statusBar = new StatusBar(this);
        add(statusBar, BorderLayout.SOUTH);
    }

}
