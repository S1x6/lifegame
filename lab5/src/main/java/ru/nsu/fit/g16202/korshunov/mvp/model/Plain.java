package ru.nsu.fit.g16202.korshunov.mvp.model;

import java.awt.*;

public class Plain extends Wireframe {

    public Plain(OpticalConfiguration opticalConfiguration, Point one, Point two, Point three, Point four) {
        super(opticalConfiguration);
        initialPoints.add(one);
        initialPoints.add(two);
        initialPoints.add(three);
        initialPoints.add(four);
    }

    @Override
    public void print(Graphics2D g, Dimension d) {
        g.setColor(Color.BLACK);
        for (int i = 0; i < 4; i++) {
            g.drawLine((int) Math.round((points.get(i).getX() + 1) * (d.width / 2.)),
                    (int) Math.round((-points.get(i).getY() + 1) * (d.height / 2.)),
                    (int) Math.round((points.get((i + 1) % 4).getX() + 1) * (d.width / 2.)),
                    (int) Math.round((-points.get((i + 1) % 4).getY() + 1) * (d.height / 2.)));
        }
        g.drawLine((int) Math.round((points.get(0).getX() + 1) * (d.width / 2.)),
                (int) Math.round((-points.get(0).getY() + 1) * (d.height / 2.)),
                (int) Math.round((points.get(2).getX() + 1) * (d.width / 2.)),
                (int) Math.round((-points.get(2).getY() + 1) * (d.height / 2.)));
        g.drawLine((int) Math.round((points.get(1).getX() + 1) * (d.width / 2.)),
                (int) Math.round((-points.get(1).getY() + 1) * (d.height / 2.)),
                (int) Math.round((points.get(3).getX() + 1) * (d.width / 2.)),
                (int) Math.round((-points.get(3).getY() + 1) * (d.height / 2.)));
    }

    @Override
    public double getMaxX() {
        return Math.max(points.get(0).getX(), Math.max(points.get(1).getX(), Math.max(points.get(2).getX(), points.get(3).getX())));
    }

    @Override
    public double getMaxY() {
        return Math.max(points.get(0).getY(), Math.max(points.get(1).getY(), Math.max(points.get(2).getY(), points.get(3).getY())));
    }

    @Override
    public double getMaxZ() {
        return Math.max(points.get(0).getZ(), Math.max(points.get(1).getZ(), Math.max(points.get(2).getZ(), points.get(3).getZ())));
    }

    @Override
    public double getMinX() {
        return Math.min(points.get(0).getX(), Math.min(points.get(1).getX(), Math.min(points.get(2).getX(), points.get(3).getX())));
    }

    @Override
    public double getMinY() {
        return Math.min(points.get(0).getY(), Math.min(points.get(1).getY(), Math.min(points.get(2).getY(), points.get(3).getY())));
    }

    @Override
    public double getMinZ() {
        return Math.min(points.get(0).getZ(), Math.min(points.get(1).getZ(), Math.min(points.get(2).getZ(), points.get(3).getZ())));
    }

    @Override
    public Point getNormal(Point p) {
        Point ray1 = points.get(0).getCopy();
        ray1.add(points.get(1).getCopy().inverse());
        Point ray2 = points.get(1).getCopy();
        ray2.add(points.get(2).getCopy().inverse());
        return ray1.cross(ray2).normalize();
    }

    @Override
    public Double detectCollision(Point start, Point ray) {
        Point normal = getNormal(null);
        double vd = normal.scalar(ray);
        if (vd == 0 || vd > 0) return null;
        Point p1 = points.get(0);
        Point p2 = points.get(1);
        Point p3 = points.get(2);
        double d = p1.getX()*p2.getY()*p3.getZ() + p1.getY()*p2.getZ()*p3.getX() + p1.getZ()*p2.getX()*p3.getY()
                - p1.getZ()*p2.getY()*p3.getX() - p1.getX()*p2.getZ()*p3.getY() - p1.getY()*p2.getX()*p3.getZ();
        double v0 = -normal.scalar(start) + d;
        Double t = v0/vd;
        if (t < 0) return null;
        return t;
    }
}
