package ru.nsu.fit.g16202.korshunov.mvp.presenter;

import com.sun.javafx.sg.prism.NGSubScene;
import ru.nsu.fit.g16202.korshunov.mvp.model.Light;
import ru.nsu.fit.g16202.korshunov.mvp.model.Matrix;
import ru.nsu.fit.g16202.korshunov.mvp.model.Point;
import ru.nsu.fit.g16202.korshunov.mvp.model.Wireframe;

import javax.security.auth.login.Configuration;
import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.List;

@SuppressWarnings("Duplicates")
public class Camera {

    public static double ZN = 2;
    private Point position = new Point(-10, 0, 0, 1);
    private Point viewPoint = new Point(0, 0, 0, 1);
    private Point upVector = new Point(0, 1, 0, 0);
    private WorldPresenter worldPresenter;
    private double zoom = 1;
    private Matrix lookAtMatrix = Matrix.getIdentityMatrix();
    private Matrix projectionMatrix = Matrix.getIdentityMatrix();
    private Dimension viewPortSize = new Dimension();
    private Matrix cameraRotationMatrix = Matrix.getIdentityMatrix();

    public Camera(WorldPresenter worldPresenter) {
        this.worldPresenter = worldPresenter;
    }

    public void setWorldView(BufferedImage image) {
        if (image == null) return;
        Graphics2D g = image.createGraphics();
        g.setColor(Color.WHITE);
        g.fillRect(0, 0, image.getWidth(), image.getHeight());
//

        calculateLookAtMatrix();
        calculateProjectionMatrix();
        Matrix div = new Matrix(new double[][]{
                {1, 0, 0, 0},
                {0, 1, 0, 0},
                {0, 0, 1, 1. / ZN},
                {0, 0, 0, 0}
        });
        g.setColor(Color.BLACK);
        worldPresenter.getWorldWireframes(true).forEach(w -> {
            applyAllMatrices(w, lookAtMatrix, div, projectionMatrix);
            w.print(g, viewPortSize);
        });

        //

        //drawWorldAxis(g);
        drawWorldBounds(g);
        //drawObjectAxis(g);
    }

    private boolean shouldClip(Point p) {
        //Configuration c = Configuration.getInstance();
        return false;
//        return (-p.getW() - c.getZn()) / (c.getZf() - c.getZn()) > 1 ||
//                (-p.getW() - c.getZn()) / (c.getZf() - c.getZn()) < -1 ||
//                p.getX() > 1 ||
//                p.getX() < -1 ||
//                p.getY() > 1 ||
//                p.getY() < -1;
    }

    private void drawWorldBounds(Graphics2D g) {
        Point luu = new Point(1, 1, 1, 1);
        Point ruu = new Point(-1, 1, 1, 1);
        Point ldu = new Point(1, 1, -1, 1);
        Point rdu = new Point(-1, 1, -1, 1);
        Point lud = new Point(1, -1, 1, 1);
        Point rud = new Point(-1, -1, 1, 1);
        Point ldd = new Point(1, -1, -1, 1);
        Point rdd = new Point(-1, -1, -1, 1);
//        Matrix i =Matrix.getIdentityMatrix();
        Matrix div = new Matrix(new double[][]{
                {1, 0, 0, 0},
                {0, 1, 0, 0},
                {0, 0, 1, 1. / ZN},
                {0, 0, 0, 0}
        });
        applyAllMatrices(luu, lookAtMatrix, div, projectionMatrix);
        applyAllMatrices(ruu, lookAtMatrix, div, projectionMatrix);
        applyAllMatrices(ldu, lookAtMatrix, div, projectionMatrix);
        applyAllMatrices(rdu, lookAtMatrix, div, projectionMatrix);
        applyAllMatrices(lud, lookAtMatrix, div, projectionMatrix);
        applyAllMatrices(rud, lookAtMatrix, div, projectionMatrix);
        applyAllMatrices(ldd, lookAtMatrix, div, projectionMatrix);
        applyAllMatrices(rdd, lookAtMatrix, div, projectionMatrix);
        g.setStroke(new BasicStroke(2));
        g.setColor(Color.BLACK);
        // top
        if (!shouldClip(luu) && !shouldClip(ruu)) {
            g.drawLine(
                    (int) Math.round((luu.getX() + 1) * (viewPortSize.width / 2.)),
                    (int) Math.round((-luu.getY() + 1) * (viewPortSize.height / 2.)),
                    (int) Math.round((ruu.getX() + 1) * (viewPortSize.width / 2.)),
                    (int) Math.round((-ruu.getY() + 1) * (viewPortSize.height / 2.)));
        }
        if (!shouldClip(ruu) && !shouldClip(rdu)) {
            g.drawLine(
                    (int) Math.round((ruu.getX() + 1) * (viewPortSize.width / 2.)),
                    (int) Math.round((-ruu.getY() + 1) * (viewPortSize.height / 2.)),
                    (int) Math.round((rdu.getX() + 1) * (viewPortSize.width / 2.)),
                    (int) Math.round((-rdu.getY() + 1) * (viewPortSize.height / 2.)));
        }
        if (!shouldClip(rdu) && !shouldClip(ldu)) {
            g.drawLine(
                    (int) Math.round((rdu.getX() + 1) * (viewPortSize.width / 2.)),
                    (int) Math.round((-rdu.getY() + 1) * (viewPortSize.height / 2.)),
                    (int) Math.round((ldu.getX() + 1) * (viewPortSize.width / 2.)),
                    (int) Math.round((-ldu.getY() + 1) * (viewPortSize.height / 2.)));
        }
        if (!shouldClip(ldu) && !shouldClip(luu)) {
            g.drawLine(
                    (int) Math.round((ldu.getX() + 1) * (viewPortSize.width / 2.)),
                    (int) Math.round((-ldu.getY() + 1) * (viewPortSize.height / 2.)),
                    (int) Math.round((luu.getX() + 1) * (viewPortSize.width / 2.)),
                    (int) Math.round((-luu.getY() + 1) * (viewPortSize.height / 2.)));
        }
        // bottom
        if (!shouldClip(lud) && !shouldClip(rud)) {
            g.drawLine(
                    (int) Math.round((lud.getX() + 1) * (viewPortSize.width / 2.)),
                    (int) Math.round((-lud.getY() + 1) * (viewPortSize.height / 2.)),
                    (int) Math.round((rud.getX() + 1) * (viewPortSize.width / 2.)),
                    (int) Math.round((-rud.getY() + 1) * (viewPortSize.height / 2.)));
        }
        if (!shouldClip(rud) && !shouldClip(rdd)) {
            g.drawLine(
                    (int) Math.round((rud.getX() + 1) * (viewPortSize.width / 2.)),
                    (int) Math.round((-rud.getY() + 1) * (viewPortSize.height / 2.)),
                    (int) Math.round((rdd.getX() + 1) * (viewPortSize.width / 2.)),
                    (int) Math.round((-rdd.getY() + 1) * (viewPortSize.height / 2.)));

        }
        if (!shouldClip(rdd) && !shouldClip(ldd)) {
            g.drawLine(
                    (int) Math.round((rdd.getX() + 1) * (viewPortSize.width / 2.)),
                    (int) Math.round((-rdd.getY() + 1) * (viewPortSize.height / 2.)),
                    (int) Math.round((ldd.getX() + 1) * (viewPortSize.width / 2.)),
                    (int) Math.round((-ldd.getY() + 1) * (viewPortSize.height / 2.)));
        }
        if (!shouldClip(ldd) && !shouldClip(lud)) {
            g.drawLine(
                    (int) Math.round((ldd.getX() + 1) * (viewPortSize.width / 2.)),
                    (int) Math.round((-ldd.getY() + 1) * (viewPortSize.height / 2.)),
                    (int) Math.round((lud.getX() + 1) * (viewPortSize.width / 2.)),
                    (int) Math.round((-lud.getY() + 1) * (viewPortSize.height / 2.)));
        }
        // sides
        if (!shouldClip(luu) && !shouldClip(lud)) {
            g.drawLine(
                    (int) Math.round((luu.getX() + 1) * (viewPortSize.width / 2.)),
                    (int) Math.round((-luu.getY() + 1) * (viewPortSize.height / 2.)),
                    (int) Math.round((lud.getX() + 1) * (viewPortSize.width / 2.)),
                    (int) Math.round((-lud.getY() + 1) * (viewPortSize.height / 2.)));
        }
        if (!shouldClip(ruu) && !shouldClip(rud)) {
            g.drawLine(
                    (int) Math.round((ruu.getX() + 1) * (viewPortSize.width / 2.)),
                    (int) Math.round((-ruu.getY() + 1) * (viewPortSize.height / 2.)),
                    (int) Math.round((rud.getX() + 1) * (viewPortSize.width / 2.)),
                    (int) Math.round((-rud.getY() + 1) * (viewPortSize.height / 2.)));
        }
        if (!shouldClip(ldu) && !shouldClip(ldd)) {
            g.drawLine(
                    (int) Math.round((ldu.getX() + 1) * (viewPortSize.width / 2.)),
                    (int) Math.round((-ldu.getY() + 1) * (viewPortSize.height / 2.)),
                    (int) Math.round((ldd.getX() + 1) * (viewPortSize.width / 2.)),
                    (int) Math.round((-ldd.getY() + 1) * (viewPortSize.height / 2.)));
        }
        if (!shouldClip(rdu) && !shouldClip(rdd)) {
            g.drawLine(
                    (int) Math.round((rdu.getX() + 1) * (viewPortSize.width / 2.)),
                    (int) Math.round((-rdu.getY() + 1) * (viewPortSize.height / 2.)),
                    (int) Math.round((rdd.getX() + 1) * (viewPortSize.width / 2.)),
                    (int) Math.round((-rdd.getY() + 1) * (viewPortSize.height / 2.)));
        }
    }


    public Dimension getViewPortSize(int viewWidth, int viewHeight) {
        double scale = Math.min(viewWidth, viewHeight);
        viewPortSize.width = (int) (scale);
        viewPortSize.height = (int) (scale);
        return viewPortSize;
    }

    private void calculateLookAtMatrix() {
//        Matrix.applyMatrixToPoint(position, cameraRotationMatrix);
        Point cameraDirection = new Point(position.getX() - viewPoint.getX(), position.getY() - viewPoint.getY(), position.getZ() - viewPoint.getZ(), 1);
        cameraDirection.normalize();
        Point cameraRight = upVector.cross(cameraDirection);
        cameraRight.normalize();
        Point cameraUp = cameraDirection.cross(cameraRight);
//        Matrix.applyMatrixToPoint(cameraDirection, cameraRotationMatrix);
//        Matrix.applyMatrixToPoint(cameraUp, cameraRotationMatrix);
//        Matrix.applyMatrixToPoint(cameraRight, cameraRotationMatrix);
        Matrix m = new Matrix(new double[][]{
                {cameraRight.getX(), cameraUp.getX(), cameraDirection.getX(), 0},
                {cameraRight.getY(), cameraUp.getY(), cameraDirection.getY(), 0},
                {cameraRight.getZ(), cameraUp.getZ(), cameraDirection.getZ(), 0},
                {0, 0, 0, 1}
        });
        Matrix m1 = new Matrix(new double[][]{
                {1, 0, 0, 0},
                {0, 1, 0, 0},
                {0, 0, 1, 0},
                {-position.getX(), -position.getY(), -position.getZ(), 1}});
        lookAtMatrix = m1.multiply(m);
    }

    private void calculateProjectionMatrix() {
        projectionMatrix = new Matrix(new double[][]{
                {2. * ZN / 1 / zoom, 0, 0, 0},
                {0, 2. * ZN / 1 / zoom, 0, 0},
                {0, 0, 15. / (15 - ZN), 1},
                {0, 0, -ZN * 15 / (15 - ZN), 0}
        });
    }

    // from scaled world to the end
    public void applyAllMatrices(Wireframe w, Matrix lookAt, Matrix div, Matrix proj) {
        w.applyMatrix(lookAt);
        w.applyMatrix(div);
        w.applyPerspective();
        w.applyMatrix(proj);
    }

    public void applyAllMatrices(Point p, Matrix lookAt, Matrix div, Matrix proj) {
        Matrix.applyMatrixToPoint(p, lookAt);
        Matrix.applyMatrixToPoint(p, div);
//        p.setZ(p.getZ() / p.getW());
//        p.setW(1);
        p.setX(p.getX() / p.getW());
        p.setY(p.getY() / p.getW());
        Matrix.applyMatrixToPoint(p, proj);
    }

    public Point getViewPoint() {
        return viewPoint;
    }

    public void setViewPoint(Point p) {
        viewPoint = p;
    }

    public Point getUpVector() {
        return upVector;
    }

    public void move(double offset, Wireframe.Axis axis) {
        switch (axis) {
            case X_AXIS:
                position.setX(position.getX() + offset);
                break;
            case Y_AXIS:
                position.setY(position.getY() + offset);
//                viewPoint.setY(viewPoint.getY() + offset);
                break;
            case Z_AXIS:
                position.setZ(position.getZ() + offset);
//                viewPoint.setZ(viewPoint.getZ() + offset);
                break;
        }
    }

    public void rotate(double angle, Wireframe.Axis axis) {
        switch (axis) {
            case X_AXIS:
//                cameraRotationMatrix = cameraRotationMatrix.multiply(Matrix.getXRotationMatrix(Math.toRadians(angle)));
                Matrix.applyMatrixToPoint(position, Matrix.getXRotationMatrix(Math.toRadians(angle)));
                break;
            case Y_AXIS:
//                cameraRotationMatrix = cameraRotationMatrix.multiply(Matrix.getYRotationMatrix(Math.toRadians(angle)));
                Matrix.applyMatrixToPoint(position, Matrix.getYRotationMatrix(Math.toRadians(angle)));
                break;
            case Z_AXIS:
//                cameraRotationMatrix = cameraRotationMatrix.multiply(Matrix.getZRotationMatrix(Math.toRadians(angle)));
                Matrix.applyMatrixToPoint(position, Matrix.getZRotationMatrix(Math.toRadians(angle)));
                break;
        }
    }


    public void setPosition(Point pos) {
        position = pos;
    }

    public void setRotationMatrix(Matrix matrix) {
        cameraRotationMatrix = matrix;
    }

    public void zoomOut(double v) {
        zoom /= v;
        System.out.println("zoom out " + zoom);
    }

    public void zoomIn(double v) {
        zoom *= v;
        System.out.println("zoom in " + zoom);
    }

    public Point getPosition() {
        return position;
    }

    public List<Wireframe> getObjectsInCamera() {
        worldPresenter.revalidate();
        calculateLookAtMatrix();
        worldPresenter.getWorldWireframes(true).forEach(w -> w.applyMatrix(lookAtMatrix));
//        calculateLookAtMatrix();
        calculateProjectionMatrix();
        Matrix div = new Matrix(new double[][]{
                {1, 0, 0, 0},
                {0, 1, 0, 0},
                {0, 0, 1, 1. / ZN},
                {0, 0, 0, 0}
        });
//        worldPresenter.getWorldWireframes().forEach(w -> {
//            applyAllMatrices(w, lookAtMatrix, div, projectionMatrix);
//        });
        List<Wireframe> list = worldPresenter.getWorldWireframes(false);
        list.forEach(w-> {
            //w.applyMatrix(div);
            //w.applyPerspective();
            //w.applyMatrix(projectionMatrix);
        });
        return list;
    }

    public List<Light> getLights() {
        List<Light> lights = worldPresenter.getLights();

        lights.forEach(l -> Matrix.applyMatrixToPoint(l.getPosition(), lookAtMatrix));
        return lights;
    }

    public WorldPresenter getWorldPresenter() {
        return worldPresenter;
    }
}
