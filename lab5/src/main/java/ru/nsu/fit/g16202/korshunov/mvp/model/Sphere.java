package ru.nsu.fit.g16202.korshunov.mvp.model;

import java.awt.*;

public class Sphere extends Wireframe {

    private double initRadius;
    private double radius;

    public Sphere(OpticalConfiguration opticalConfiguration, Point center, double radius) {
        super(opticalConfiguration);
        initialPoints.add(center);
        this.initRadius = radius;
        createPoints();
    }

    private void createPoints() {
        Point center = initialPoints.get(0);
        initialPoints.add(new Point(center.getX() + initRadius, center.getY() + initRadius , center.getZ() + initRadius , 1));
        initialPoints.add(new Point(center.getX() + initRadius, center.getY() - initRadius , center.getZ() + initRadius , 1));
        initialPoints.add(new Point(center.getX() + initRadius, center.getY() - initRadius , center.getZ() - initRadius , 1));
        initialPoints.add(new Point(center.getX() + initRadius, center.getY() + initRadius , center.getZ() - initRadius , 1));
        initialPoints.add(new Point(center.getX() - initRadius, center.getY() + initRadius , center.getZ() + initRadius , 1));
        initialPoints.add(new Point(center.getX() - initRadius, center.getY() - initRadius , center.getZ() + initRadius , 1));
        initialPoints.add(new Point(center.getX() - initRadius, center.getY() - initRadius , center.getZ() - initRadius , 1));
        initialPoints.add(new Point(center.getX() - initRadius, center.getY() + initRadius , center.getZ() - initRadius , 1));
    }

    @Override
    public void initPoints() {
        super.initPoints();
        radius = initRadius;
    }

    @Override
    public void print(Graphics2D g, Dimension d) {
        for (int i = 0; i < 4; i++) {
            g.drawLine(
                    (int) Math.round((points.get(i+1).getX() + 1) * (d.width / 2.)),
                    (int) Math.round((-points.get(i+1).getY() + 1) * (d.height / 2.)),
                    (int) Math.round((points.get((i + 1) % 4 +1).getX() + 1) * (d.width / 2.)),
                    (int) Math.round((-points.get((i + 1) % 4+ 1).getY() + 1) * (d.height / 2.)));
        }
        for (int i = 4; i < 8; i++) {
            g.drawLine(
                    (int) Math.round((points.get(i+1).getX() + 1) * (d.width / 2.)),
                    (int) Math.round((-points.get(i+1).getY() + 1) * (d.height / 2.)),
                    (int) Math.round((points.get((i + 1) % 4 + 5).getX() + 1) * (d.width / 2.)),
                    (int) Math.round((-points.get((i + 1) % 4 + 5).getY() + 1) * (d.height / 2.)));
        }
        for (int i = 1; i < 5; i++) {
            g.drawLine(
                    (int) Math.round((points.get(i).getX() + 1) * (d.width / 2.)),
                    (int) Math.round((-points.get(i).getY() + 1) * (d.height / 2.)),
                    (int) Math.round((points.get((i + 4)).getX() + 1) * (d.width / 2.)),
                    (int) Math.round((-points.get((i + 4)).getY() + 1) * (d.height / 2.)));
        }
    }

    @Override
    public double getMaxX() {
        return points.get(0).getX() + initRadius;
    }

    @Override
    public double getMaxY() {
        return points.get(0).getY() + initRadius;
    }

    @Override
    public double getMaxZ() {
        return points.get(0).getZ() + initRadius;
    }

    @Override
    public double getMinX() {
        return points.get(0).getX() - initRadius;
    }

    @Override
    public double getMinY() {
        return points.get(0).getY() - initRadius;
    }

    @Override
    public double getMinZ() {
        return points.get(0).getZ() - initRadius;
    }

    @Override
    public Point getNormal(Point p) {
        Point normal = points.get(0).getCopy().inverse();
        normal.add(p);
        normal.normalize();
        return normal;
    }

    @Override
    public Double detectCollision(Point start, Point ray) {
        Point oc = start.getCopy().inverse();
        oc.add(points.get(0));
        double ocLength = Math.sqrt(oc.scalar(oc));
        if (ocLength < radius) return null;
        double tca = oc.scalar(ray);
        double thcSquare = radius * radius - ocLength * ocLength + tca * tca;
        if (thcSquare < 0) return null;
        double t = tca - Math.sqrt(thcSquare);
        if (t < 0) return null;
        return t;
    }

    public double getRadius() {
        return radius;
    }

    public void setRadius(double v) {
        this.radius = v;
    }
}
