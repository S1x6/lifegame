package ru.nsu.fit.g16202.korshunov.mvp.model;

import java.awt.*;

public class Box extends Wireframe {

    private int n;
    private Point center;

    public Box(OpticalConfiguration opticalConfiguration, Point minPoint, Point maxPoint, int n) {
        super(opticalConfiguration);
        this.n = n;
        Point one = minPoint.getCopy();
        Point two = minPoint.getCopy();
        Point three = minPoint.getCopy();
        Point four;
        Point five;
        Point six;

        initialPoints.add(minPoint);

        one.setX(maxPoint.getX());
        initialPoints.add(one);

        four = one.getCopy();
        four.setY(maxPoint.getY());
        initialPoints.add(four);

        two.setY(maxPoint.getY());
        initialPoints.add(two);

        five = one.getCopy();
        five.setZ(maxPoint.getZ());
        initialPoints.add(five);

        three.setZ(maxPoint.getZ());
        initialPoints.add(three);

        six = two.getCopy();
        six.setZ(maxPoint.getZ());
        initialPoints.add(six);

        initialPoints.add(maxPoint);
        center = new Point((maxPoint.getX() + minPoint.getX()) / 2,
                (maxPoint.getY() + minPoint.getY()) / 2,
                (maxPoint.getZ() + minPoint.getZ()) / 2,
                1
        );
    }

    @Override
    public void print(Graphics2D g, Dimension d) {
        g.setColor(n == 1 ? Color.RED : n == 2 ? Color.green : Color.BLUE);
        for (int i = 0; i < 4; i++) {
            g.drawLine(
                    (int) Math.round((points.get(i).getX() + 1) * (d.width / 2.)),
                    (int) Math.round((-points.get(i).getY() + 1) * (d.height / 2.)),
                    (int) Math.round((points.get((i + 1) % 4).getX() + 1) * (d.width / 2.)),
                    (int) Math.round((-points.get((i + 1) % 4).getY() + 1) * (d.height / 2.)));
        }
        for (int i = 4; i < 8; i++) {
            g.drawLine(
                    (int) Math.round((points.get(i).getX() + 1) * (d.width / 2.)),
                    (int) Math.round((-points.get(i).getY() + 1) * (d.height / 2.)),
                    (int) Math.round((points.get((i + 1) % 4 + 4).getX() + 1) * (d.width / 2.)),
                    (int) Math.round((-points.get((i + 1) % 4 + 4).getY() + 1) * (d.height / 2.)));
        }
        g.drawLine(
                (int) Math.round((points.get(0).getX() + 1) * (d.width / 2.)),
                (int) Math.round((-points.get(0).getY() + 1) * (d.height / 2.)),
                (int) Math.round((points.get(5).getX() + 1) * (d.width / 2.)),
                (int) Math.round((-points.get(5).getY() + 1) * (d.height / 2.)));
        g.drawLine(
                (int) Math.round((points.get(1).getX() + 1) * (d.width / 2.)),
                (int) Math.round((-points.get(1).getY() + 1) * (d.height / 2.)),
                (int) Math.round((points.get(4).getX() + 1) * (d.width / 2.)),
                (int) Math.round((-points.get(4).getY() + 1) * (d.height / 2.)));
        g.drawLine(
                (int) Math.round((points.get(2).getX() + 1) * (d.width / 2.)),
                (int) Math.round((-points.get(2).getY() + 1) * (d.height / 2.)),
                (int) Math.round((points.get(7).getX() + 1) * (d.width / 2.)),
                (int) Math.round((-points.get(7).getY() + 1) * (d.height / 2.)));
        g.drawLine(
                (int) Math.round((points.get(3).getX() + 1) * (d.width / 2.)),
                (int) Math.round((-points.get(3).getY() + 1) * (d.height / 2.)),
                (int) Math.round((points.get(6).getX() + 1) * (d.width / 2.)),
                (int) Math.round((-points.get(6).getY() + 1) * (d.height / 2.)));

    }

    @Override
    public Point getNormal(Point p) {
        Point normal = p.getCopy();
        normal.add(center.getCopy().inverse());
        return normal;
    }

    @Override
    public Double detectCollision(Point start, Point ray) {

        Double tn;
        Double tf;
        tn = -Double.MAX_VALUE;
        tf = Double.MAX_VALUE;

        if (ray.getX() == 0) {
            if (start.getX() < points.get(0).getX() || start.getX() > points.get(7).getX()) {
                return null;
            }
        } else {
            double t1 = (points.get(0).getX() - start.getX()) / ray.getX();
            double t2 = (points.get(7).getX() - start.getX()) / ray.getX();
            if (t2 < t1) {
                double a = t2;
                t2 = t1;
                t1 = a;
            }
            if (t1 > tn) { tn = t1;}
            if (t2 < tf) { tf = t2;}
            if (tn > tf) return null;
            if (tf < 0) return null;
        }
        if (ray.getY() == 0) {
            if (start.getY() < points.get(0).getY() || start.getY() > points.get(7).getY()) {
                return null;
            }
        } else {
            double t1 = (points.get(0).getY() - start.getY()) / ray.getY();
            double t2 = (points.get(7).getY() - start.getY()) / ray.getY();
            if (t2 < t1) {
                double a = t2;
                t2 = t1;
                t1 = a;
            }
            if (t1 > tn) { tn = t1;}
            if (t2 < tf) { tf = t2;}
            if (tn > tf) return null;
            if (tf < 0) return null;
        }

        if (ray.getZ() == 0) {
            if (start.getZ() < points.get(0).getZ() || start.getZ() > points.get(7).getZ()) { // 1
                return null;
            }
        } else {
            double t1 = (points.get(0).getZ() - start.getZ()) / ray.getZ();
            double t2 = (points.get(7).getZ() - start.getZ()) / ray.getZ();
            if (t2 < t1) {
                double a = t2;
                t2 = t1;
                t1 = a;
            }
            if (t1 > tn) { tn = t1;}
            if (t2 < tf) { tf = t2;}
            if (tn > tf) return null;
            if (tf < 0) return null;
        }
//        System.out.print("x: " + tnx + " " + tf);
//        System.out.print(" y: " + tny + " " + tfy);
//        System.out.println(" z: " + tnz + " " + tfz);
//        return new Point(ray.getX()*tn,ray.getY()*tn,ray.getZ()*tn,n);
//        System.out.println("found");
        if (tn < 0) return null;
        return tn;
    }

    @Override
    public double getMaxX() {
        return points.get(7).getX();
    }

    @Override
    public double getMaxY() {
        return points.get(7).getY();
    }

    @Override
    public double getMaxZ() {
        return points.get(7).getZ();
    }

    @Override
    public double getMinX() {
        return points.get(0).getX();
    }

    @Override
    public double getMinY() {
        return points.get(0).getY();
    }

    @Override
    public double getMinZ() {
        return points.get(0).getZ();
    }

}
