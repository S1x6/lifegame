package ru.nsu.fit.g16202.korshunov.mvp.presenter;

import ru.nsu.fit.g16202.korshunov.mvp.model.*;
import ru.nsu.fit.g16202.korshunov.mvp.model.Box;
import ru.nsu.fit.g16202.korshunov.mvp.model.Point;
import ru.nsu.fit.g16202.korshunov.mvp.view.View;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;
import java.awt.event.MouseWheelEvent;
import java.util.ArrayList;
import java.util.List;

public class Presenter {

    private WorldPresenter worldPresenter;
    private View view;
    private List<Wireframe> wireframes = new ArrayList<>();
    private JFrame frame;
    private Camera camera;
    private Renderer renderer;
    private int lastX = -1;
    private int lastY = -1;

    public Presenter(JFrame frame) {
        view = new View();
        this.frame = frame;
        worldPresenter = new WorldPresenter();
        camera = new Camera(worldPresenter);
        renderer = new Renderer();
        view.setCamera(camera);
        loadConfiguration();
        view.addMouseMotionListener(new MouseMotionAdapter() {
            @Override
            public void mouseDragged(MouseEvent e) {
                super.mouseDragged(e);
                int signX = lastX == -1 ? 0 : lastX - e.getX();
                int signY = lastY == -1 ? 0 : lastY - e.getY();
                lastX = e.getX();
                lastY = e.getY();
                System.out.println(signX);
                camera.rotate(signX , Wireframe.Axis.X_AXIS);
                camera.rotate(signY , Wireframe.Axis.Z_AXIS);
                setViewSize();
                camera.setWorldView(view.getImage());
                view.repaint();
            }
        });
        view.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseReleased(MouseEvent e) {
                super.mouseReleased(e);
                lastX = -1;
                lastY = -1;
            }
        });
        view.addMouseWheelListener(new MouseAdapter() {
            @Override
            public void mouseWheelMoved(MouseWheelEvent e) {
                super.mouseWheelMoved(e);
//                Configuration c = Configuration.getInstance();;
                if (e.getWheelRotation() > 0) {
                    camera.zoomIn(1.05);
                    view.repaint();
                    camera.setWorldView(view.getImage());
                } else if (e.getWheelRotation() < 0) {
                    camera.zoomOut(1.05);
                    view.repaint();
                    camera.setWorldView(view.getImage());
                }
            }
        });
        setViewSize();
        camera.setWorldView(view.getImage());
        view.repaint();
    }

    public View getViewPort() {
        return view;
    }

    private void loadConfiguration() {
        this.wireframes.clear();
        OpticalConfiguration o1 = new OpticalConfiguration(
                128. / 255, 128. / 255, 128. / 255,
                128. / 255, 128. / 255, 128. / 255,
                100
        );
        OpticalConfiguration o2 = new OpticalConfiguration(
                128. / 255, 128. / 255, 0,
                128. / 255, 128. / 255, 1,
                100
        );
        OpticalConfiguration mirrorOc = new OpticalConfiguration(
                0.4,0.4,0.4,
                1,1,1,
                200
        );
//        this.wireframes.add(new Box(o, new Point(1, 1, 1, 1), new Point(2, 2, 2, 1), 1));
//        this.wireframes.add(new Box(o, new Point(2, -2, -2, 1), new Point(1, -1, -1, 1), 2));
//        this.wireframes.add(new Box(o, new Point(-2, -2, 2, 1), new Point(-1, -1, 1, 1), 3));
        this.wireframes.add(new Sphere(o1, new Point(1,0,2,1), 0.5));
        this.wireframes.add(new Sphere(o2, new Point(0,1,2,1), 0.5));
        this.wireframes.add(new Sphere(o1, new Point(0,0,0,1), 0.5));
        this.wireframes.add(new Sphere(o2, new Point(0,2,2,1), 0.5));
        this.wireframes.add(new Sphere(o1, new Point(3,1,2,1), 0.5));
        this.wireframes.add(new Sphere(o2, new Point(1,2,1,1), 0.5));
        this.wireframes.add(new Sphere(o2, new Point(2,2,0,1), 0.5));
        this.wireframes.add(new Sphere(mirrorOc, new Point(0,-3,0,1), 1));
//        this.wireframes.add(new Plain(o, new Point(2.2, 2, 2, 1), new Point(2.2, 2, -2, 1),
//                new Point(2.2, -2, 2, 1), new Point(2.2, -2, -2, 1)));
        List<Light> lights = new ArrayList<>();
        lights.add(new Light(new Point(1, 0, 0, 1), Color.RED));
        lights.add(new Light(new Point(-1,0,0,1), Color.WHITE));
        worldPresenter.setLights(lights);
        worldPresenter.setWireframes(this.wireframes);
        view.repaint();
    }

    public void setSelectionMode() {

    }

    public void startRender() {
        renderer.render(camera, view.getImage());
        view.repaint();
    }

    public void resetCamera() {
        camera.setViewPoint(new Point(0, 0, 0, 0));
        camera.setPosition(new Point(-10, 0, 0, 0));
        camera.setRotationMatrix(Matrix.getIdentityMatrix());
        view.repaint();
    }

    private void setViewSize() {
        Dimension d = camera.getViewPortSize(view.getWidth() - 2 * 10, view.getHeight() - 2 * 10);
        view.setViewPortSize(d);
    }
}
