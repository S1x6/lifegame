package ru.nsu.fit.g16202.korshunov.mvp.model;

public class OpticalConfiguration {
    private double KDr;
    private double KDg;
    private double KDb;
    private double KSr;
    private double KSg;
    private double KSb;
    private double Power;

    public OpticalConfiguration(double kDr, double kDg, double kDb, double kSr, double kSg, double kSb, double power) {
        KDr = kDr;
        KDg = kDg;
        KDb = kDb;
        KSr = kSr;
        KSg = kSg;
        KSb = kSb;
        Power = power;
    }

    public double getKDr() {
        return KDr;
    }

    public void setKDr(double KDr) {
        this.KDr = KDr;
    }

    public double getKDg() {
        return KDg;
    }

    public void setKDg(double KDg) {
        this.KDg = KDg;
    }

    public double getKDb() {
        return KDb;
    }

    public void setKDb(double KDb) {
        this.KDb = KDb;
    }

    public double getKSr() {
        return KSr;
    }

    public void setKSr(double KSr) {
        this.KSr = KSr;
    }

    public double getKSg() {
        return KSg;
    }

    public void setKSg(double KSg) {
        this.KSg = KSg;
    }

    public double getKSb() {
        return KSb;
    }

    public void setKSb(double KSb) {
        this.KSb = KSb;
    }

    public double getPower() {
        return Power;
    }

    public void setPower(double power) {
        Power = power;
    }
}
